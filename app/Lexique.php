<?php

namespace App;
use App\Lexique;
use Illuminate\Database\Eloquent\Model;

class Lexique extends Model
{
    //
	protected $fillable = [	
	'matiere_id', 
	'title', 
	'content', 
	'vue',
	'comment',		
	'category_id'		
	];
}

