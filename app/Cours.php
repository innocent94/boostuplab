<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Cours;

class Cours extends Model
{
    //
	protected $fillable = [	
	'matiere_id', 
	'objectifs', 
	'prerequis', 
	'illustration_img_id', 
	'title', 
	'content', 
	'user_id', 
	'status_id',
	'file_id',
	'level',
	'time',
	'vue',
	'comment',		
	'category_id'		
	];
	/*
	public function categorie(){
	  return $this->belongsTo('App\Cours');
	}
	
	//scope time frame for creation of the claime
	public function scopeDateRange($query, $from,$to)
	{
	 return $query->whereBetween('actualites.created_at', [$from, $to]);
	}
	
	
	
	
*/
}

