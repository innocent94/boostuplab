<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Exercise;

class Exercise extends Model
{
    //
	protected $fillable = [	
	'matiere_id', 
	'user_id', 
	'title', 
	'body', 
	'topic', 
	'category_id', 
	'correction', 
	'status_id',
	'correction_id',
	'vue',
	'comment'		
	];
}

