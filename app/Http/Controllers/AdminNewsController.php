<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Files;
use App\News;

use DB;

class AdminNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$news =  News::orderBy('created_at', 'desc')->get();	
	    return view('admin.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //		
	    return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$input = $request->all();
        $user = Auth::user();
        if($file = $request->file('file_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/news', $name);
            $Files = Files::create(['name'=>$name]);
            $input['file_id'] = $name;
        }
	
		$news = News::create($input);
		$news->user_id = $user->name;
		$news->save();
		
		return redirect('/admin/news')->with('success', 'Actualite cree avec succes');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$news =  News::findOrFail($id);		
        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		
		$input = $request->all();
        $user = Auth::user();
        if($file = $request->file('file_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/news', $name);
            $Files = Files::create(['name'=>$name]);
            $input['file_id'] = $name;
        }
		
		$article =  News::findOrFail($id);
	
		$article->update($input);
		$article->user_id = $user->name;
		$article->save();
		
		return redirect()->route('admin.news.edit', $article->id)->with('success', 'Mise(s) à jour effectuée(s)');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$article =  News::findOrFail($id);
        unlink(public_path() . '/images/news/'. $article->file_id);
		
    
		$article->delete();
        return redirect('/admin/news')->withErrors('Article supprimé');
    }
	
	
	public function read($id){
	  $read = Actualite::findOrFail($id);
	  $categories = DB::table('actu_categories')->where('title', $read->category_id)->get();
	  $article = DB::table('actualites')->where('id', $read->id)->get();
	  return view('show.read', compact('article', 'categories'));
	}
}
