<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Matiere;
use App\Student;
use App\Quiz\Topic;
use App\Quiz\Question;
use App\Quiz\QuestionsOption;

class PremiumQuizController extends Controller
{
    //
	
	protected function initializeDate()
    {
	  $filter_matiere="";
	  $filter_status="";
    }
	
	
	
	private $filter_matiere, $filter_status;
		
	public function index(Request $request, $filter_matiere="", $filter_status="")
	{
		
		//Get filter variable
		$filter_matiere = $request->input('filter_matiere');
		$filter_status = $request->input('filter_status');
		
		// 4 conditions query
		
		// !empty values
		if(!empty($filter_matiere) and !empty($filter_status)){
			$topics = Topic::join('matieres', 'topics.matiere_id', '=', 'matieres.id')
			->select('topics.*', 'topics.created_at', 'topics.updated_at', 'matieres.name')
			->where('matiere_id', $filter_matiere)
			->where('category_id', $filter_status)
			->orderBy('topics.created_at', 'desc')->paginate(2);
		}
		// matiere empty
		else if(empty($filter_matiere) and !empty($filter_status)){
			$topics = Topic::join('matieres', 'topics.matiere_id', '=', 'matieres.id')
			->select('topics.*', 'topics.created_at', 'topics.updated_at', 'matieres.name')
			->where('category_id', $filter_status)
			->orderBy('topics.created_at', 'desc')->paginate(2);
		}
		//status empty
		else if(!empty($filter_matiere) and empty($filter_status)){
			$topics = Topic::join('matieres', 'topics.matiere_id', '=', 'matieres.id')
			->select('topics.*', 'topics.created_at', 'topics.updated_at', 'matieres.name')
			->where('matiere_id', $filter_matiere)
			->orderBy('topics.created_at', 'desc')->paginate(2);
		}
		// empty twice
		else{
			$topics = Topic::join('matieres', 'topics.matiere_id', '=', 'matieres.id')
			->select('topics.*', 'topics.created_at', 'topics.updated_at', 'matieres.name')
			->orderBy('topics.created_at', 'desc')->paginate(2);
		}
		
			
		$matieres = Matiere::join('topics', 'topics.matiere_id', '=', 'matieres.id')
			->groupBy('matieres.id')
			->get([
			  'matieres.id',
			  'matieres.name',
			  DB::raw('count(topics.matiere_id) as count')
			]);
			
			
		return view('quiz.index', compact('topics', 'matieres', 'filter_matiere', 'filter_status'));
	}
	
	
	
	
	public function test($id)
	{	
		$topics = Topic::findOrFail($id);
		$vews = $topics->vews;		
		$countvews = DB::table('topics')->where('id', $topics->id)->update(array('vews' => $vews + 1));		
		if($topics->category_id == 1){
			if(Auth::guard('student')->user()->active_subscription != 0){
				
			}else{				
				return redirect()->route('errors.premium.index');
			}
		}
		
		$questions = Question::inRandomOrder()->where('topic_id', $topics->id)->limit(10)->get();
        foreach ($questions as &$question) {
            $question->options = QuestionsOption::where('question_id', $question->id)->inRandomOrder()->get();
        }
		
		return view('quiz.test.index', compact('topics', 'questions'));
	}
	
	
	
	
	/**
     * Store a newly solved Test in storage with results.
     *
     * @param  \App\Http\Requests\StoreResultsRequest  $request
     * @return \Illuminate\Http\Response
     */
	 
    public function store_quiz_test(Request $request, $id)
    {
		$topics = Topic::findOrFail($id);
		$matiere = DB::table('matieres')->where('id', $topics->matiere_id)->select('name')->first();		
        $result = 0;
        $ok = 0;
        $notok = 0;
		
		$QuestionId = array();
		$Answers = array();
		$OptionSelected = array();
			
        foreach ($request->input('questions', []) as $key => $question) {
            $status = 0;			
			$i++;
            if ($request->input('answers.'.$question) != null
                && QuestionsOption::find($request->input('answers.'.$question))->correct
            ) {
                $status = 1;
                $result += 2;
                $ok++;			
				$OptionSelected[] = $request->input('answers.'.$question);
				$Answers[] = $status;
            }else{
				$status = 0;
				$notok++;
				$Answers[] = $status;
				$OptionSelected[] = $request->input('answers.'.$question);
			}			
			$QuestionId[] = $question;	
        }
		
        return view('quiz.test.result.index', compact('topics', 'matiere', 'result', 'ok', 'notok', 'Answers', 'QuestionId', 'OptionSelected'));
    }
	
	
	
	
	
	
	public function show_result($id)
	{	
		$topic = Topic::findOrFail($id);
		
		return view('quiz.test.result.index');
	}
	
	
	
}
