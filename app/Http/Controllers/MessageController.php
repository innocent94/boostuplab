<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

use App\Message;
use App\Photo;
use DB;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
		
		$m_not_read = $request->input('not_read');
		$m_importants = $request->input('is_placed');
		if(empty($m_importants) and empty($m_not_read)){
			$filter = Message::orderby('created_at','desc')->get()->all();
		}else{
			if($m_not_read ==0){
				$filter = Message::orderby('created_at','desc')->where('is_read', 0)->get()->all();
			}else{}
		
			if(!empty($m_importants)){
				$filter = Message::orderby('created_at','desc')->where('is_placed', '=', 'important')->get()->all();
			}else{}
		}	
		
		
		$messages = $filter;
		
		$all_received = count($messages);
		$not_read = Message::where('is_read', 0)->get()->count();	
		$important = DB::table('messages')->where('is_placed', '=', 'important')->get()->count();
		
		
	    return view('admin.messages.index',compact('messages', 'not_read', 'important', 'all_received'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$messages = Message::findOrFail($id);
		
		//Read
		$affected = DB::table('messages')->where('id', $id)->update(array('is_read' => 1));
		
		$read = DB::table('messages')->where('id', $id)->get();
		$important = DB::table('messages')->where('is_placed', '=', 'important')->get()->count();
		$not_read = DB::table('messages')->where('is_read', 0)->get()->count();
		
		
        return view('admin.messages.edit', compact('messages', 'read', 'important', 'not_read'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$input = $request->all();
        
		$message =  Message::findOrFail($id);
	
		$message->update($input);
		$message->save();
		
		return redirect()->route('admin.messages.edit', $message->id)->with('success', 'Ajouté a la liste des messages importants');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$article =  Message::findOrFail($id);
		$article->delete();
        return redirect('/admin/messages')->withErrors('Message Supprimé');
    }
	
	
	public function composemail($id){
	  $messages = Message::findOrFail($id);
	  $email = $messages->email;
	  $id = $messages->id;
	  return view('admin.messages.mail', compact('messages'));
	}
	
	public function sendmail(Request $request){
	
	  $email = $request->input('email');
	  $title = $request->input('object');
	  
	  $data = array(
		'title' => $request->title,
	    'content' => $request->message
	  );

	  Mail::to($email)->send(new SendMail($data));
	   	  
	  return redirect()->route('admin.messages.index')->with('success', 'Email envoyé avec succès');
	}
}
