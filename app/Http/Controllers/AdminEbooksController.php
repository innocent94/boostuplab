<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Ebook;
use App\Matiere;
use App\Files;
use App\Cours;


use DB;

class AdminEbooksController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();	
		if(Auth::user()->roles->pluck('name')->first() =='Administrateur (Professeur)'){				
			$ebooks =  Ebook::join('matieres', 'matieres.id', '=', 'ebooks.matiere_id')
			->select('ebooks.*', 'matieres.name')
			->where('user_id', Auth::user()->id)
			->orderBy('created_at', 'desc')
			->get(); 
		}else{
			$ebooks =  Ebook::join('matieres', 'matieres.id', '=', 'ebooks.matiere_id')
			->select('ebooks.*', 'matieres.name')
			->orderBy('created_at', 'desc')
			->get(); 
		}
	    return view('admin.ebooks.index',compact('ebooks', 'matieres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();		
	    return view('admin.ebooks.create',compact('matieres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$input = $request->all();
		if($file_illustration = $request->file('illustration_img_id')){
            $name = time() . $file_illustration->getClientOriginalName();
            $file_illustration->move('images/illustation', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['illustration_img_id'] = $name;
        }
	
		$ebooks = Ebook::create($input);
		$ebooks->save();
				
		return redirect()->route('admin.ebooks.index')->with('success', 'Ebook ajoutee avec succes');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$ebooks =  Ebook::findOrFail($id);
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();
		
        return view('admin.ebooks.edit', compact('ebooks', 'matieres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		
		$input = $request->all();
        $user = Auth::user();
        
		if($file_illustration = $request->file('illustration_img_id')){
            $name = time() . $file_illustration->getClientOriginalName();
            $file_illustration->move('images/illustation', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['illustration_img_id'] = $name;
        }
		
		$ebooks =  Ebook::findOrFail($id);
	
		$ebooks->update($input);
		$ebooks->user_id = $user->id;
		$ebooks->save();
		
		return redirect()->route('admin.ebooks.edit', $ebooks->id)->with('success', 'Modifications(s) effectuee(s)');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$ebooks =  Ebook::findOrFail($id);
        unlink(public_path() . '/images/illustration/'. $ebooks->illustration_img_id);
		
		$ebooks->delete();
        return redirect('/admin/ebooks/')->withErrors('L\'element a ete supprime');
    }
	

}
