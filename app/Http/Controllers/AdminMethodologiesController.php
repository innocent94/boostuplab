<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Methodologie;
use App\Matiere;
use App\Files;
use App\Cours;


use DB;

class AdminMethodologiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();	
		if(Auth::user()->roles->pluck('name')->first() =='Administrateur (Professeur)'){				
			$methodologies =  Methodologie::join('matieres', 'matieres.id', '=', 'methodologies.matiere_id')
			->select('methodologies.*', 'matieres.name')
			->where('user_id', Auth::user()->id)
			->orderBy('created_at', 'desc')
			->get(); 
		}else{
			$methodologies =  Methodologie::join('matieres', 'matieres.id', '=', 'methodologies.matiere_id')
			->select('methodologies.*', 'matieres.name')
			->orderBy('created_at', 'desc')
			->get(); 
		}
	    return view('admin.methodologies.index',compact('methodologies', 'matieres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();		
	    return view('admin.methodologies.create',compact('matieres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$input = $request->all();
        if($file = $request->file('file_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/methodologies', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['file_id'] = $name;
        }
		
		if($file_illustration = $request->file('illustration_img_id')){
            $name = time() . $file_illustration->getClientOriginalName();
            $file_illustration->move('images/illustation', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['illustration_img_id'] = $name;
        }
	
		$methodologies = Methodologie::create($input);
		$methodologies->save();
				
		return redirect()->route('admin.methodologies.index')->with('success', 'methodologies cree avec succes');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$methodologies =  Methodologie::findOrFail($id);
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();
		
        return view('admin.methodologies.edit', compact('methodologies', 'matieres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		
		$input = $request->all();
        $user = Auth::user();
        if($file = $request->file('file_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/methodologies', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['file_id'] = $name;
        }
		
		if($file_illustration = $request->file('illustration_img_id')){
            $name = time() . $file_illustration->getClientOriginalName();
            $file_illustration->move('images/illustation', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['illustration_img_id'] = $name;
        }
		
		$methodologies =  Methodologie::findOrFail($id);
	
		$methodologies->update($input);
		$methodologies->user_id = $user->id;
		$methodologies->save();
		
		return redirect()->route('admin.methodologies.edit', $methodologies->id)->with('success', 'Modifications(s) effectuee(s)');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$methodologies =  Methodologie::findOrFail($id);
        unlink(public_path() . '/images/methodologies/'. $methodologies->file_id);
        unlink(public_path() . '/images/illustration/'. $methodologies->illustration_img_id);
		
		$methodologies->delete();
        return redirect('/admin/methodologies/')->withErrors('La methodologies a ete supprimee');
    }
	

}
