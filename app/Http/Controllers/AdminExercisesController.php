<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Exercise;
use App\Matiere;
use App\Files;
use App\Cours;


use DB;

class AdminExercisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();	
		if(Auth::user()->roles->pluck('name')->first() =='Administrateur (Professeur)'){				
			$exercises =  Exercise::join('matieres', 'matieres.id', '=', 'exercises.matiere_id')
			->select('exercises.*', 'matieres.name')
			->where('user_id', Auth::user()->id)
			->orderBy('created_at', 'desc')
			->get(); 
		}else{
			$exercises =  Exercise::join('matieres', 'matieres.id', '=', 'exercises.matiere_id')
			->select('exercises.*', 'matieres.name')
			->orderBy('created_at', 'desc')
			->get(); 
		}
	    return view('admin.exercises.index',compact('exercises', 'matieres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();		
	    return view('admin.exercises.create',compact('matieres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$input = $request->all();
        if($file = $request->file('topic')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/exercises', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['topic'] = $name;
        }
		
		if($file_illustration = $request->file('correction')){
            $name = time() . $file_illustration->getClientOriginalName();
            $file_illustration->move('images/exercises', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['correction'] = $name;
        }
	
		$exercises = Exercise::create($input);
		$exercises->save();
				
		return redirect()->route('admin.exercises.index')->with('success', 'Cours cree avec succes');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$exercises =  Exercise::findOrFail($id);
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();
		
        return view('admin.exercises.edit', compact('exercises', 'matieres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		
		$input = $request->all();
        $user = Auth::user();
        if($file = $request->file('topic')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/exercises', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['topic'] = $name;
        }
		
		if($file_illustration = $request->file('correction')){
            $name = time() . $file_illustration->getClientOriginalName();
            $file_illustration->move('images/exercises', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['correction'] = $name;
        }
		
		$exercises =  Exercise::findOrFail($id);
	
		$exercises->update($input);
		$exercises->user_id = $user->id;
		$exercises->save();
		
		return redirect()->route('admin.exercises.edit', $exercises->id)->with('success', 'Modifications(s) effectuee(s)');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$exercises =  Exercise::findOrFail($id);
        unlink(public_path() . '/images/cours/'. $exercises->topic);
        unlink(public_path() . '/images/illustration/'. $exercises->correction);
		
		$exercises->delete();
        return redirect('/admin/exercises/')->withErrors('L\'exercie a ete supprime');
    }
	
	
	public function view($id){
	  $read = Exercise::findOrFail($id);
	  $cours = DB::table('exercises')->where('id', $read->id)->get();
	  return view('admin.exercises.view', compact('exercises'));
	}
}
