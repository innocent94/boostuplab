<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersEditRequest;
use App\Http\Requests\UsersRequest;
use App\Files;
use App\User;
use Illuminate\Http\Request;


use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

//For spatie
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Hash;



class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware(['auth', 'isAdmin']); //middleware 
    }

    public function index()
    {
        //
        $users = User::with('roles.permissions')->get();

       // $roles = $users->roles->pluck('name','id')->all();

        return view('admin.users.index', compact('users','roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::pluck('name','id')->all();
        $permissions = Permission::wherenotin('name', ['reclam_view'])->pluck('name','id')->all();
        return view('admin.users.create', compact('roles','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'roles' => 'required'
        ]);

        //
        if(trim($request->password) == ''){
            $input = $request->except('password');
        } else{
            $input = $request->all();
            $input['password'] = bcrypt($request->password);
        }

        if($file = $request->file('file_id')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images/admin', $name);
            $photo = Files::create(['name'=>$name]);
            $input['file_id'] = $photo->id;
        }

        $user = User::create($input);
        //spatie roles assigment
        $user->assignRole($request->input('roles'));

        //Spatie permissions 
        $user->givePermissionTo($request->input('permissions'));

      
        $user->save();

        return redirect('/admin/users');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.uses.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);
  
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','id')->all();

        $permissions = Permission::wherenotin('name', ['reclam_view'])->get();
        $userPermissions = $user->getAllPermissions()->pluck('name','id')->all();

       // ->all();
        return view('admin.users.edit', compact('user','roles','userRole','permissions','userPermissions'));
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function editprofile($id)
     {
        //
        $user = User::findOrFail($id);
  
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','id')->all();
        $permissions = Permission::wherenotin('name', ['reclam_view'])->get();
        $userPermissions = $user->getAllPermissions()->pluck('name','id')->all();
  
     return view('admin.users.editprofile', compact('user','roles','userRole','permissions','userPermissions'));

    }

   /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(UsersEditRequest $request, $id)
   {
        //

    $user = User::findOrFail($id);
    if(trim($request->password) == ''){
        $input = $request->except('password');
    } else{
        $input = $request->all();
        $input['password'] = bcrypt($request->password);
    }

    if($file = $request->file('file_id')){
        $name = time() . $file->getClientOriginalName();
        $file->move('images/admin', $name);
        $photo = Files::create(['name'=>$name]);
        $input['file_id'] = $photo->id;
    }


    //update all values assigned (any new value should be in the mass-assignment!)
    $user->update($input);

        //spatie roles assigment 
    if($role = $request->input('roles')){
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));
    }

        //spatie permission assignment 
    DB::table('model_has_permissions')->where('model_id',$id)->delete();
    $user->givePermissionTo($request->input('permissions'));



return redirect()->route('admin.users.edit', $user->id)->with('success', 'Mise(s) à jour effectuée(s)');


}
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update_profile(UsersEditRequest $request, $id)
     {
        //

        $user = User::findOrFail($id);
        $newpassword = strlen(trim($request->newpassword));
        $password = $request->password;
        $file = $request->file('file_id');

        if(empty($newpassword) and empty($password) and empty($file)){
            return redirect()->route('editprofile', $user->id)->with('success', 'Aucune mise à jour effectuée');
        }

        if(!empty($newpassword) and empty($password)){
         return redirect()->route('editprofile', $user->id)->withErrors("Ancien mot de passe requis");
     }

     if(empty($newpassword) and !empty($password)){
         return redirect()->route('editprofile', $user->id)->withErrors("Nouveau mot de passe requis");
     }


     if(!empty($newpassword) and !empty($password)){
        if ($newpassword < 8) {
            return redirect()->route('editprofile', $user->id)->withErrors("Le nouveau mot de passe est trop court il doit comporter au moins (8) caractères");
        }

        if(trim($request->password) == '' or trim($request->newpassword) == ''){
            $input = $request->except('password');
        } else{
            if(Hash::check($request->password,$user->password)){
                $input = $request->all();
                $input['password'] = bcrypt($request->newpassword);
            }else{
                return redirect()->route('editprofile', $user->id)->withErrors("L'ancien mot de passe n'est pas correct");
            }
        }
    }

    if($file = $request->file('file_id')){
        $name = time() . $file->getClientOriginalName();
        $file->move('images/admin', $name);
        $photo = Files::create(['name'=>$name]);
        $input['file_id'] = $photo->id;
    }

        //update all values assigned (any new value should be in the mass-assignment!)
    $user->update($input);

        //spatie roles assigment 
    if($role = $request->input('roles')){
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));
    }

        //spatie permission assignment 
    DB::table('model_has_permissions')->where('model_id',$id)->delete();
    $user->givePermissionTo($request->input('permissions'));


    return redirect()->route('editprofile', $user->id)->with('success', 'Mise(s) à jour effectuée(s)');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
       // return $user->photo->file;
        if($file = $user->file_id)
        {
           // unlink($user->photo->file);
        }
        $user->delete();
        Session::flash('deleted_user','Agent effacer avec success');
         //notify l agent, le chef de departement, le superadmin de la creation 
        return redirect('/admin/users');
    }
}
