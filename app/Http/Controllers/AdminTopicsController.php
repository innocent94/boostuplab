<?php

namespace App\Http\Controllers;

use App\Quiz\Topic;
use App\Quiz\Question;
use App\Quiz\QuestionsOption;
use App\User;
use App\Matiere;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTopicsRequest;
use App\Http\Requests\UpdateTopicsRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreQuestionsRequest; 

use DB;

class AdminTopicsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	    $this->middleware(['isAdmin'])->except('index', 'show');
	    $this->middleware('permission:admin_access');
    }

    /**
     * Display a listing of Topic.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::all();
        return view('admin.quiz.topics.index', compact('topics'));
    }

    /**
     * Show the form for creating new Topic.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();	
        return view('admin.quiz.topics.create', compact('matieres'));
    }

    /**
     * Store a newly created Topic in storage.
     *
     * @param  \App\Http\Requests\StoreTopicsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTopicsRequest $request)
    {
		$user = Auth::user();		
        $topic = Topic::create($request->all());		
		$topic->user_id = $user->name;
		$topic->save();

        return redirect()->route('admin.quiz.topics.index')->with('success', 'Quiz cree avec succes');;
    }


    /**
     * Show the form for editing Topic.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topic = Topic::findOrFail($id);
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();	
		
        return view('admin.quiz.topics.edit', compact('topic', 'matieres'));
    }

    /**
     * Update Topic in storage.
     *
     * @param  \App\Http\Requests\UpdateTopicsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTopicsRequest $request, $id)
    {
        $topic = Topic::findOrFail($id);
        $topic->update($request->all());
		
		return redirect()->route('admin.quiz.topics.edit', $topic->id)->with('success', 'Modification effectuée');
    }


    /**
     * Display Topic.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topic = Topic::findOrFail($id);

        return view('admin.quiz.topics.show', compact('topic'));
    }


    /**
     * Remove Topic from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::findOrFail($id);
        $topic->delete();

        return redirect()->route('admin.quiz.topics.index')->with('success', 'Quiz supprimé');
    }

    /**
     * Delete all selected Topic at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = Topic::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
	
	
	
	 public function addquestion($id)
    {
        $topic = Topic::findOrFail($id);
		$questions = Question::orderby('id','asc')->where('topic_id', $topic->id)->get();

		$relations = [
            'topics' => \App\Quiz\Topic::get()->pluck('title', 'id')->prepend('Please select', ''),
        ];

        $correct_options = [
            'option1' => 'Option #1',
            'option2' => 'Option #2',
            'option3' => 'Option #3',
            'option4' => 'Option #4'
        ];
			
        return view('admin.quiz.topics.add_questions.index', compact('topic', 'correct_options', 'questions') + $relations);
    }
	
	
	
	
	 /**
     * Display Question.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add(StoreQuestionsRequest $request, $id)
    {
        $topic = Topic::findOrFail($id);
		$questions = Question::orderby('id','asc')->where('topic_id', $topic->id)->get();
		
		
		$question = Question::create($request->all());

        foreach ($request->input() as $key => $value) {
            if(strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('correct') == $key ? 1 : 0;
                QuestionsOption::create([
                    'question_id' => $question->id,
                    'option'      => $value,
                    'correct'     => $status
                ]);
            }
        }
			
        return view('admin.quiz.topics.questions.index', compact('topic', 'questions'));
		return redirect()->route('admin.quiz.topics.questions.index', $topic->id)->with('success', 'Quiz supprimé');
    }
	
	
	 /**
     * Display Question.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_questions($id)
    {
        $topic = Topic::findOrFail($id);
		$questions = Question::orderby('id','asc')->where('topic_id', $topic->id)->get();
		

			
        return view('admin.quiz.topics.questions.index', compact('topic', 'questions'));
    }
}
