<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Matiere;
use App\Student;
use App\Cours;
use Illuminate\Support\Facades\Auth;

class PremiumCourseController extends Controller
{
    //
	
	protected function initializeDate()
    {
	  $filter_matiere="";
	  $filter_status="";
    }
	
	
	
	private $filter_matiere, $filter_status;
		
	public function index(Request $request, $filter_matiere="", $filter_status="")
	{
		
		//Get filter variable
		$filter_matiere = $request->input('filter_matiere');
		$filter_status = $request->input('filter_status');
		
		// 4 conditions query
		
		// !empty values
		if(!empty($filter_matiere) and !empty($filter_status)){
			$cours = Cours::join('matieres', 'cours.matiere_id', '=', 'matieres.id')
			->select('cours.*', 'cours.created_at', 'cours.updated_at', 'matieres.name')
			->where('matiere_id', $filter_matiere)
			->where('category_id', $filter_status)
			->orderBy('cours.created_at', 'desc')->paginate(2);
		}
		// matiere empty
		else if(empty($filter_matiere) and !empty($filter_status)){
			$cours = Cours::join('matieres', 'cours.matiere_id', '=', 'matieres.id')
			->select('cours.*', 'cours.created_at', 'cours.updated_at', 'matieres.name')
			->where('category_id', $filter_status)
			->orderBy('cours.created_at', 'desc')->paginate(2);
		}
		//status empty
		else if(!empty($filter_matiere) and empty($filter_status)){
			$cours = Cours::join('matieres', 'cours.matiere_id', '=', 'matieres.id')
			->select('cours.*', 'cours.created_at', 'cours.updated_at', 'matieres.name')
			->where('matiere_id', $filter_matiere)
			->orderBy('cours.created_at', 'desc')->paginate(2);
		}
		// empty twice
		else{
			$cours = Cours::join('matieres', 'cours.matiere_id', '=', 'matieres.id')
			->select('cours.*', 'cours.created_at', 'cours.updated_at', 'matieres.name')
			->orderBy('cours.created_at', 'desc')->paginate(2);
		}
		
			
		$matieres = Matiere::join('cours', 'cours.matiere_id', '=', 'matieres.id')
			->groupBy('matieres.id')
			->get([
			  'matieres.id',
			  'matieres.name',
			  DB::raw('count(cours.matiere_id) as count')
			]);
			
			
		return view('courses.index', compact('cours', 'matieres', 'filter_matiere', 'filter_status'));
	}
	
	
	
	
	public function show($id)
	{	
		$cours = Cours::findOrFail($id);
		$user = DB::table('users')->where('id', $id)->select('name', 'file_id', 'email')->first();
		$user_picture = DB::table('files')->where('id', $user->file_id)->select('name')->first();
		$matiere = DB::table('matieres')->where('id', $cours->matiere_id)->select('name')->first();	
		$vews = $cours->vue;		
		$countvews = DB::table('cours')->where('id', $id)->update(array('vue' => $vews + 1));		
		if($cours->category_id == 1){
			if(Auth::guard('student')->user()->active_subscription != 0){
				return view('courses.show', compact('cours', 'user', 'user_picture', 'matiere'));
			}else{				
				return redirect()->route('errors.premium.index');
			}
		}
		
		
		
		return view('courses.show', compact('cours', 'user', 'user_picture', 'matiere'));
	}
	
	
	
	
}
