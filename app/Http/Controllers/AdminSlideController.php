<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Slide;
use App\Files;
use DB;

class AdminSlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$slide = Slide::orderby('order','asc')->get()->all();
	    return view('admin.slider.index',compact('slide'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$LastOrder = DB::table('slides')->latest()->first();
		$input = $request->all();
        if($file = $request->file('photo_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/slides', $name);
            $photo = Files::create(['name'=>$name]);
            $input['image_id'] = $name;
        }
	
		$slide = Slide::create($input);
		$slide->order = $LastOrder->order + 1;
		$slide->save();
		
		return redirect('/admin/slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$LastOrder = Slide::orderBy('order', 'desc')->first();
		$max = $LastOrder->order;
		
		$slide = Slide::findOrFail($id);
        return view('admin.slider.edit', compact('slide', 'max'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$slide =  Slide::findOrFail($id);
		
		$input = $request->all();
        if($file = $request->file('photo_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/slides', $name);
            $photo = Files::create(['name'=>$name]);
            $input['image_id'] = $name;
        }
		
		
		
		//PastOrder
		$past_order = $slide->order;	
		//NewOrder
		$new_order = $request->input('order');
		if($past_order != $new_order){
			if($past_order > $new_order){
				$rows = DB::table('slides')->whereBetween('order', array($new_order, $past_order - 1))->get(['order', 'id']);
				foreach($rows as $row) {			
					$affected = DB::table('slides')->where('id', $row->id)->update(array('order' => $row->order + 1));
				}
			}elseif($past_order < $new_order){
				$rows = DB::table('slides')->whereBetween('order', array($past_order + 1, $new_order))->get(['order', 'id']);
				foreach($rows as $row) {			
					$affected = DB::table('slides')->where('id', $row->id)->update(array('order' => $row->order - 1));
				}
			}else{
				
			}
		}
		
		$slide->update($input);
		$slide->save();
		
		return redirect()->route('admin.slider.edit', $slide->id)->with('success', 'Modification effectuée');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$slide =  Slide::findOrFail($id);
       
	
		
		
		unlink(public_path() . '/images/slides/'. $slide->image_id);
				
		$slide->delete();
		
		$order = $slide->order;	
		
		$rows = DB::table('slides')->where('order', '>', $order)->get(['order', 'id']);
		foreach($rows as $row) {			
			$affected = DB::table('slides')->where('id', $row->id)->update(array('order' => $row->order - 1));
		}
		
        return redirect('/admin/slider')->withErrors('Slide supprimé');
    }
}
