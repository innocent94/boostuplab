<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Student;

class PremiumAccessController extends Controller
{
    //
	
	public function __construct()
    {
      $this->middleware('auth.student');
      $this->middleware('isAdmin');
  
    }
	
	
	public function index()
	{
		
		return view('account.premium.index');
	}
}
