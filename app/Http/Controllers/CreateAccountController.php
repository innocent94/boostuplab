<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\StudentRequest;

use App\Files;
use App\Student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Hash;

class CreateAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		return view('create.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        //
		$this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:students,email',
            'password' => 'required'
        ]);
		
		$input = $request->all();
        $password = strlen(trim($request->password));
		$confirmpassword = strlen(trim($request->confirmpassword));
		
        if($password == $confirmpassword){
            $input['password'] = bcrypt($request->password);
        } else{
           return redirect()->route('create.index')->withErrors("Les mots de passe ne correspondent pas");         
        }
		
		$input['avatar'] = 'user.jpg';

        $student = Student::create($input);
		$student->save();

		return redirect('/create')->with('success', 'Votre Compte a bien ete cree connectez vous maintenant');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
