<?php

namespace App\Http\Controllers;

use App\Quiz\Topic;
use App\Quiz\Question;
use App\Quiz\QuestionsOption;
use Illuminate\Http\Request;
use App\Http\Requests\StoreQuestionsRequest;
use App\Http\Requests\UpdateQuestionsRequest;
use App\Http\Requests\UpdateQuestionsOptionsRequest;

use DB;

class AdminQuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	    $this->middleware(['isAdmin'])->except('index', 'show');
	    $this->middleware('permission:admin_access');
    }

    /**
     * Display a listing of Question.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();

        return view('questions.index', compact('questions'));
    }

    /**
     * Show the form for creating new Question.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relations = [
            'topics' => \App\Topic::get()->pluck('title', 'id')->prepend('Please select', ''),
        ];

        $correct_options = [
            'option1' => 'Option #1',
            'option2' => 'Option #2',
            'option3' => 'Option #3',
            'option4' => 'Option #4'
        ];

        return view('admin.quiz.topics.add_questions.create', compact('correct_options') + $relations);
    }

    /**
     * Store a newly created Question in storage.
     *
     * @param  \App\Http\Requests\StoreQuestionsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQuestionsRequest $request)
    {
		
		
		
        $question = Question::create($request->all());

        foreach ($request->input() as $key => $value) {
            if(strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('correct') == $key ? 1 : 0;
                QuestionsOption::create([
                    'question_id' => $question->id,
                    'option'      => $value,
                    'correct'     => $status
                ]);
            }
        }

        return redirect()->route('admin.topics.questions.index', $question->id)->with('success', 'Question ajoutee avec succes');
    }


    /**
     * Show the form for editing Question.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::findOrFail($id);		
		$questions_option = QuestionsOption::orderby('id','asc')->where('question_id', $question->id)->get();	
        return view('admin.quiz.topics.questions.edit', compact('question', 'questions_option'));
    }

    /**
     * Update Question in storage.
     *
     * @param  \App\Http\Requests\UpdateQuestionsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionsRequest $request, $id)
    {
        $question = Question::findOrFail($id);
        
		$rows = DB::table('questions_options')->where('question_id', $question->id)->get(['option', 'id']);
		$i = 1;
		foreach($rows as $row) {
			if($request->input('correct') == $row->id){
				$affected = DB::table('questions_options')->where('id', $row->id)->update(array('option' => $request->input('option'.$i), 'correct' => 1));
			}else{
				$affected = DB::table('questions_options')->where('id', $row->id)->update(array('option' => $request->input('option'.$i), 'correct' => 0));
			}
			$i++;
		}
		
				
	
		$question->update($request->all());
        return redirect()->route('admin.quiz.topics.questions.edit', $question->id)->with('success', 'Question modifiee avec success..!');
    }


    /**
     * Display Question.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'topics' => \App\Topic::get()->pluck('title', 'id')->prepend('Please select', ''),
        ];

        $question = Question::findOrFail($id);

        return view('questions.show', compact('question') + $relations);
    }


    /**
     * Remove Question from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $question->delete();

        return redirect()->route('questions.index');
    }

    /**
     * Delete all selected Question at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = Question::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
