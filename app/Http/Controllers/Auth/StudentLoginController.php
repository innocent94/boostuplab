<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use\Auth;

class StudentLoginController extends Controller
{
    //

	public function __construct()
    {
        $this->middleware('guest:student');
    }

	public function showLoginForm()
	{
		return view('auth.student_login');
	}

	public function login(Request $request)
	{

		$this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);

		if(Auth::guard('student')->attempt(['email' => $request->email, 'password' => $request->password])){
			return redirect()->intended('/account');
		}

		return redirect()->back()->withErrors("Erruer de connexion");

	}
}
