<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;

use Carbon\Carbon;

use App\GalCategorie;
use App\Evenement;
use App\Slide;
use App\Video;
use App\Photo;
use App\Message;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    public function __construct()
    {
      $this->middleware('auth');
      $this->middleware(['isAdmin', 'clearance'])->except('index', 'show');
      $this->middleware('permission:admin_access');
      self::initializeDate();
    }
    protected function initializeDate()
    {
       $this->start = Carbon::create(2018, 1, 1, 0, 0, 0);
      //$this->start = Carbon::now()->startOfMonth();
      $this->firstDayofPreviousMonnsth = Carbon::now()->startOfMonth()->subMonth()->toDateString();
      $this->end = Carbon::now()->endOfMonth();
      $this->lastDayofPreviousMonth = Carbon::now()->subMonth();

      $this->isclosed=True;
      $this->isNonfonde=False;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     * Le scope pour les utilisateur est active. Ce qui veut dire que toutes les requetes 
     * automatiquement reduit la donnees au context de l utilisateur. 
     *
     */
    //protected $this->start, $this->end;
    private $start, $end;
    protected $totalATraiter, $growTotalATraiter;
    protected $exportExcelData;

    public function updateDashbaordFilter(
      Request $request,$start="",$end="")
    {
       
       

        //update agents
      if ($status != ""){
        $this->status=$status;
      }
      if($this->start == "" ){
        $this->start = Carbon::now()->startOfMonth();
        $firstDayofPreviousMonth = Carbon::now()->startOfMonth()->subMonth()->toDateString();
      }else {
        $this->start = new Carbon($this->start);
        $firstDayofPreviousMonth = (new Carbon($this->start))->startOfMonth()->subMonth()->toDateString();
      }
      if ($this->end == "")
      {
        $this->end = Carbon::now();
        $lastDayofPreviousMonth = Carbon::now()->subMonth();
      }else{
        $this->end = new Carbon($this->end);
        $lastDayofPreviousMonth = (new Carbon($this->end))->subMonth();
      }
      $this->isNonfonde = $isNonfonde;
      self::index();

    }

    public function index(Request $request,$start="",$end="",$processus='',$isclosed=True, $isNonfonde=False,$cellule='',$agence='',$status='')
    {
		// Message
		$not_read = Message::where('is_read', 0)->get()->all();
		$TotalNotRead = count($not_read);
		$messages = Message::get()->all();	
		$TotalMessageReceive = count($messages);	
		$MessagesImportants = Message::orderby('created_at','desc')->limit(5)->where('is_placed', '=', 'important')->get()->all();
		$TotalMessageImportants = count($MessagesImportants);



		return view('admin', compact('not_read', 'MessagesImportants', 'TotalMessageImportants'));
	}
	
	public function MessageManager()
	{
		// Message
		$not_read = Message::where('is_read', 0)->get()->all();
		$TotalNotRead = count($not_read);
		$messages = Message::get()->all();	
		$TotalMessageReceive = count($messages);
		
		$MessagesImportants = Message::orderby('created_at','desc')->limit(5)->where('is_placed', '=', 'important')->get()->all();
		$TotalMessageImportants = count($MessagesImportants);
		
		return view('ha_top_nav_bar_with_notif', compact('TotalNotRead', 'MessagesImportants', 'TotalMessageImportants'));
	}
	
	
	
	

}

