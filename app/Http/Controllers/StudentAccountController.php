<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use DB;
use Hash;
use App\Files;
use App\User;
use App\Student;
use App\Subscription;

use Carbon\Carbon;

class StudentAccountController extends Controller
{
    //
	
	public function __construct(){
    
      $this->middleware('auth.student');
      $this->middleware('isAdmin');
  
    }
	
	
	public function index()
	{	
		$student = Student::where('id', Auth::guard('student')->user()->id)->get();		
		$abonement = DB::table('students')->where('id', Auth::guard('student')->user()->id)->first();	
		return view('account.index', compact('student'));
	}
	
	
	public function subscription(){
		$subscription = Subscription::all();
		return view('account.subscription.index', compact('subscription'));
	}
	
	
	
	public function validate($id){
		
		$subscription = Subscription::findOrFail($id);
		
		if(Auth::guard('student')->user()->active_subscription != 1){
			//Event Date update
			$update = DB::table('students')->where('id',  Auth::guard('student')->user()->id)->update(array('type_subscription'=>$subscription->name, 'amount'=>$subscription->amount));
		}
		
		// Get update values from student
		$student = Student::where('id', Auth::guard('student')->user()->id)->first();	
		return view('account.subscription.validate_subscription', compact('student'));
		
	}
	
	
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function edit($id){	
		$student = Student::findOrFail($id);
		if(Auth::guard('student')->user()->id != $student->id){
			return view('error');	
		}else{
			return view('account.student_profile', compact('student'));	
		}
	}
	
	
	
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update_profile(Request $request, $id)
     {
        //
 
		$student = Student::findOrFail($id);
        $newpassword = strlen(trim($request->newpassword));
        $password = $request->password;
		

       
		if(!empty($newpassword) and empty($password)){
			return redirect()->route('student_profile', $student->id)->withErrors("Ancien mot de passe requis");
		}

		if(empty($newpassword) and !empty($password)){
			return redirect()->route('student_profile', $student->id)->withErrors("Nouveau mot de passe requis");
		}


		if(!empty($newpassword) and !empty($password)){
			if ($newpassword < 8) {
				return redirect()->route('student_profile', $student->id)->withErrors("Le nouveau mot de passe est trop court il doit comporter au moins (8) caractères");
			}
			if(trim($request->password) == '' or trim($request->newpassword) == ''){
				$input = $request->except('password');
			} else{
				if(Hash::check($request->password,$student->password)){
					$input = $request->all();
					$input['password'] = bcrypt($request->newpassword);
				}else{
					return redirect()->route('student_profile', $student->id)->withErrors("Ancien mot de passe incorrect");
				}
			}
		}else{
			$input = $request->except('password');
		}
	
		if($file = $request->file('avatar')){
			$name = time() . $file->getClientOriginalName();
			$file->move('images/students', $name);
			$Get_file = Files::create(['name'=>$name]);
			$input['avatar'] = $name;
		}

		$student->update($input);		
		return redirect()->route('student_profile', $student->id)->with('success', 'Mise(s) à jour effectuée(s)');
}
	
	
}
