<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Lexique;
use App\Matiere;
use App\Files;
use App\Cours;


use DB;

class AdminLexiquesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();	
		if(Auth::user()->roles->pluck('name')->first() =='Administrateur (Professeur)'){				
			$lexiques =  Lexique::join('matieres', 'matieres.id', '=', 'lexiques.matiere_id')
			->select('lexiques.*', 'matieres.name')
			->where('user_id', Auth::user()->id)
			->orderBy('created_at', 'desc')
			->get(); 
		}else{
			$lexiques =  Lexique::join('matieres', 'matieres.id', '=', 'lexiques.matiere_id')
			->select('lexiques.*', 'matieres.name')
			->orderBy('created_at', 'desc')
			->get(); 
		}
	    return view('admin.lexiques.index',compact('lexiques', 'matieres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();		
	    return view('admin.lexiques.create',compact('matieres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$input = $request->all();
        	
		$lexiques = Lexique::create($input);
		$lexiques->save();
				
		return redirect()->route('admin.lexiques.index')->with('success', 'Cours cree avec succes');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$lexiques =  Lexique::findOrFail($id);
		$matieres = Matiere::orderby('name','asc')->pluck('name','name')->all();
		
        return view('admin.lexiques.edit', compact('lexiques', 'matieres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		
		$input = $request->all();
        $user = Auth::user();
        
		
		$lexiques =  Lexique::findOrFail($id);
	
		$lexiques->update($input);
		$lexiques->user_id = $user->id;
		$lexiques->save();
		
		return redirect()->route('admin.lexiques.edit', $lexiques->id)->with('success', 'Modifications(s) effectuee(s)');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$lexiques =  Lexique::findOrFail($id);
        
		$lexiques->delete();
        return redirect('/admin/lexiques/')->withErrors('Le mot a ete supprime');
    }
	
	
}
