<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Matiere;
use App\Files;
use App\Cours;


use DB;

class AdminCoursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();
		if(Auth::user()->roles->pluck('name')->first() =='Administrateur (Professeur)'){
			$cours =  Cours::join('matieres', 'matieres.id', '=', 'cours.matiere_id')
			->select('cours.*', 'matieres.name')
			->where('user_id', Auth::user()->id)
			->orderBy('created_at', 'desc')
			->get();
		}else{
			$cours =  Cours::join('matieres', 'matieres.id', '=', 'cours.matiere_id')
			->select('cours.*', 'matieres.name')
			->orderBy('created_at', 'desc')
			->get();
        }

	    return view('admin.cours.index',compact('cours', 'matieres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$matieres = Matiere::orderby('name','asc')->pluck('name','id')->all();
	    return view('admin.cours.create',compact('matieres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$input = $request->all();
        if($file = $request->file('file_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/cours', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['file_id'] = $name;
        }

		if($file_illustration = $request->file('illustration_img_id')){
            $name = time() . $file_illustration->getClientOriginalName();
            $file_illustration->move('images/illustation', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['illustration_img_id'] = $name;
        }

		$cours = Cours::create($input);
		$cours->save();

		return redirect()->route('admin.cours.index')->with('success', 'Cours cree avec succes');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$cours =  Cours::findOrFail($id);
		$matieres = Matiere::orderby('name','asc')->pluck('name','name')->all();

        return view('admin.cours.edit', compact('cours', 'matieres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

		$input = $request->all();
        $user = Auth::user();
        if($file = $request->file('file_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('images/cours', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['file_id'] = $name;
        }

		if($file_illustration = $request->file('illustration_img_id')){
            $name = time() . $file_illustration->getClientOriginalName();
            $file_illustration->move('images/illustation', $name);
            $Get_file = Files::create(['name'=>$name]);
            $input['illustration_img_id'] = $name;
        }

		$cours =  Cours::findOrFail($id);

		$cours->update($input);
		$cours->user_id = $user->id;
		$cours->save();

		return redirect()->route('admin.cours.edit', $cours->id)->with('success', 'Modifications(s) effectuee(s)');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$cours =  Cours::findOrFail($id);
        unlink(public_path() . '/images/cours/'. $article->file_id);
        unlink(public_path() . '/images/illustration/'. $article->illustration_img_id);

		$cours->delete();
        return redirect('/admin/cours/')->withErrors('Le cours a ete supprime');
    }


	public function view($id){
	  $read = Cours::findOrFail($id);
	  $cours = DB::table('cours')->where('id', $read->id)->get();
	  return view('admin.cours.view', compact('cours'));
	}
}
