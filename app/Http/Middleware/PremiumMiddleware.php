<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class PremiumMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::guard('student')->user();
        if(isset($user)){
            if (Auth::guard('student')->user()->active_subscription != 1) {  
				return redirect()->route('errors.premium.index')->withErrors("Les mots de passe ne correspondent pas");  
            }
            if(Auth::user()->is_active != 1)
            {
                return redirect('406');
            }
        }
        return $next($request);
    }
}