<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(isset($user)){
            if (!Auth::user()->hasPermissionTo('admin_access')) {  
                return redirect('404');
            }
            if(Auth::user()->is_active != 1)
            {
                return redirect('406');
            }
        }
        return $next($request);
    }
}