<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
	protected $fillable = [
	'first_name', 
	'last_name', 
	'email', 
	'email', 
	'telephone', 
	'object', 
	'body', 
	'is_read', 
	'is_placed', 
	'image_id'
	];
}
