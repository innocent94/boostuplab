<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class Student extends Authenticatable
{
		
	protected $guard ='student';
	
	//use spatie
    use HasApiTokens, Notifiable;
    use HasRoles;
	
    protected $fillable = [
	'type_subscription',  
	'amount', 
	'date_subscription', 
	'end_subscription', 
	'active_subscription',
	'name',
	'surname',
	'phone_number',
	'school_name',
	'email',
	'password',
	'avatar'
	];

    protected $hidden = ['password',  'remember_token'];
	
	public function files(){
      return $this->belongsTo('App\Files');
    }

    public function getGravatarAttribute(){
        $hash = md5(strtolower(trim($this->attributes['email']))) . "?d=mm&s=";
        return "http://www.gravatar.com/avatar/$hash";
    }
}



    