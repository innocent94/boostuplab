<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Methodologie;

class Methodologie extends Model
{
    //
	protected $fillable = [	
	'matiere_id', 
	'objectifs', 
	'illustration_img_id', 
	'title', 
	'content', 
	'user_id', 
	'status_id',
	'file_id',
	'time',
	'vue',
	'comment',		
	'category_id'		
	];

}

