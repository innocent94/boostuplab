<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Files extends Model

{
    //


    protected $uploads = '/images/';



    protected $fillable = ['name', 'download'];

    public function getFileAttribute($files){
        return url('/').$this->uploads . $files;
    }
}
