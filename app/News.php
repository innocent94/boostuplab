<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
	protected $fillable = [	
		'title',  
		'content', 
		'file_id', 
		'user_id', 
		'status_id',
		'vue'	
	];
}
