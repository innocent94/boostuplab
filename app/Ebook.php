<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Ebook;

class Ebook extends Model
{
    //
	protected $fillable = [	
	'matiere_id', 
	'illustration_img_id', 
	'title', 
	'body', 
	'user_id', 
	'vue',
	'comment',		
	'category_id'		
	];
	
}

