<!DOCTYPE html>
<html dir="ltr" lang="en" xml:lang="en" class="yui3-js-enabled">


	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>BoostupLab | @yield('title')</title>
		<link rel="shortcut icon" href="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/favicon">
		<meta name="keywords" content="moodle, LearningZone">
		<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('css/site/style.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

        @yield('stylesheets')

        @include('includes.site_js')

    </head>


    <body id="page-site-index" class="format-site course path-site safari dir-ltr lang-en yui-skin-sam yui3-skin-sam learningzone-themescustom-com pagelayout-frontpage course-1 context-2 has-region-side-pre used-region-side-pre has-region-side-post empty-region-side-post side-pre-only layout-option-nonavbar jsenabled">


        <div id="page">


            @yield('menu')

            @yield('body')

			<!-- Start Footer section -->
			@include('includes.site_footer')

        </div>

        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery-3.1.1.min.js')}}"></script>

        @yield('javascripts')
	</body>
</html>

