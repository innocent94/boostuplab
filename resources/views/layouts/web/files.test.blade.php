<!DOCTYPE html>
<html dir="ltr" lang="en" xml:lang="en" class="yui3-js-enabled">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>boostuplab</title>
		<link rel="shortcut icon" href="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/favicon">
		<meta name="keywords" content="moodle, LearningZone">
		<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('css/site/style.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		@include('includes.site_js')
	</head>
	<body id="page-site-index" class="format-site course path-site safari dir-ltr lang-en yui-skin-sam yui3-skin-sam learningzone-themescustom-com pagelayout-frontpage course-1 context-2 has-region-side-pre used-region-side-pre has-region-side-post empty-region-side-post side-pre-only layout-option-nonavbar jsenabled">
		<div id="page">
			<div class="top-header">
			<div class="container-fluid">
				<div class="pull-left logocon">
					<a class="logo" href="http://learningzone.themescustom.com"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572//logo"/></a>
				</div>
				<div class="pull-right navigation-con">
					<header role="banner" class="navbar">
						<nav role="navigation" class="navbar-inner">
							<div class="container-fluid">
								<a class="brand" title="Home" href="{{url('/')}}">Accueil</a>
								<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
								<div class="loginsection pull-right">
									<i class="fa fa-user" aria-hidden="true"></i>
									@if(Auth::guard('student')->user())
									<a class="signup common" href="{{route('account.index')}}">Mon Compte</a>
									@else
									<a class="signup common" href="{{route('student_login')}}">Espace Etudiant</a>
									@endif
								</div>
								<!-- end div .loginsection -->
								<div class="nav-collapse collapse">
									<ul class="nav">
										<li><a  href="{{route('courses.index')}}">Cours</a></li>
										<li><a  href="#">Methodologie</a></li>
										<li><a  href="{{route('quiz.index')}}">Quiz</a></li>
										<li><a  href="{{route('exercises.index')}}">Exercices</a></li>
										<li><a  href="#">News</a></li>
										<li class="dropdown">
										<a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Mon Emploi<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
											<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
										</ul>
										<ul class="nav pull-right">
											<li></li>
										</ul>
								</div>
							</div>
						</nav>
					</header>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="bottom-header" style="positionn:relative">
				<div class="container-fluid">
					<div class="pull-left"></div>
					<div class="pull-right">
						<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
						<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
					</div>
					<div class="clearfix"></div>
				</div>

				@include('includes.message')
			</div>
			<!-- End Bottom Header Section -->
		</div>
		<!-- End Top Header Section -->

			<!-- End Bottom Header Section -->
			<!-- Start Banner -->
			<div class="callbacks_container">
				<ul class="rslides callbacks callbacks1" id="slider3">
					<li id="callbacks1_s0" class="" style="display: block; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;">
						<img src="{{asset('images/slides/banner1image.jpg')}}" alt="First Slider">
						<div class="container-fluid">
							<div class="row-fluid">
								<div class="caption span4">
									<h1>Use your imagination – it is the only thing that will never run out.</h1>
									<p>
										Our mission is to inspire our students not only intellectually but also spiritually, through participation in the sacramental life of the school.
									</p>
									<a class="btn btn-primary" href="http://learningzone.themescustom.com/#">Learn More</a>
								</div>
							</div>
						</div>
					</li>
					<li id="callbacks1_s1" style="float: left; position: relative; opacity: 1; z-index: 2; display: list-item; transition: opacity 500ms ease-in-out 0s;" class="callbacks1_on">
						<img src="{{asset('images/slides/banner2image.jpg')}}" alt="First Slider">
						<div class="container-fluid">
							<div class="row-fluid">
								<div class="caption span4">
									<h1>Use your imagination – it is the only thing that will never run out.</h1>
									<p>
										Our mission is to inspire our students not only intellectually but also spiritually, through participation in the sacramental life of the school.
									</p>
									<a class="btn btn-primary" href="http://learningzone.themescustom.com/#">Learn More</a>
								</div>
							</div>
						</div>
					</li>
					<li id="callbacks1_s2" style="float: none; position: absolute; opacity: 0; z-index: 1; display: list-item; transition: opacity 500ms ease-in-out 0s;" class="">
						<img src="{{asset('images/slides/banner3image.jpg')}}" alt="First Slider">
						<div class="container-fluid">
							<div class="row-fluid">
								<div class="caption span4">
									<h1>Use your imagination – it is the only thing that will never run out.</h1>
									<p>
										Our mission is to inspire our students not only intellectually but also spiritually, through participation in the sacramental life of the school.
									</p>
									<a class="btn btn-primary" href="http://learningzone.themescustom.com/#">Learn More</a>
								</div>
							</div>
						</div>
					</li>
				</ul>
				<a href="http://learningzone.themescustom.com/#" class="callbacks_nav callbacks1_nav prev">Previous</a>
				<a href="http://learningzone.themescustom.com/#" class="callbacks_nav callbacks1_nav next">Next</a>
			</div>
			<!-- End Banner -->
			<!-- Start Count Section -->
			<div class="numbers">
				<div class="container-fluid">
					<h1>Learning Zone By The Numbers</h1>
					<div class="row-fluid numbers-counter">
						<div class="span3">
							<div class="pull-left image-counter">
								<img src="{{ asset('images/icons/icon-1.png') }}" alt="">
							</div>
							<div class="pull-left text-con">
								<span class="counter">100000</span>
								<span class="count-info">Etudiants Inscrits</span>
							</div>
						</div>
						<div class="span3">
							<div class="pull-left image-counter">
								<img src="{{ asset('images/icons/icon-2.png') }}" alt="">
							</div>
							<div class="pull-left text-con">
								<span class="counter">10,397</span>
								<span class="count-info">Awards Winning</span>
							</div>
						</div>
						<div class="span3">
							<div class="pull-left image-counter">
								<img src="{{ asset('images/icons/icon-3.png') }}" alt="">
							</div>
							<div class="pull-left text-con">
								<span class="counter">30,897</span>
								<span class="count-info">Years of History</span>
							</div>
						</div>
						<div class="span3">
							<div class="pull-left image-counter">
								<img src="{{ asset('images/icons/icon-4.png') }}" alt="">
							</div>
							<div class="pull-left text-con">
								<span class="counter">46,034</span>
								<span class="count-info">Library Books</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Count Section -->
			<script>
				jQuery(document).ready(function( $ ) {
					$('.counter').counterUp({
						delay: 10,time: 1000
					});
				});
			</script>




			<!-- Start Available Courses-->
			<div id="courses-block">
				<br/>
				<br/>
				<div class="header">
					<div class="container-fluid">
						<h2 class="courses-block-heading">Présentation</h2>
						<p class="tagline">
							<strong>“ Boost’up “ </strong> est une plateforme fonctionnant en présentiel, en application web et mobile. Elle permet aux jeunes déscolarisés, vulnérables, chômeurs aux étudiants et travailleurs africains de bénéficier de fiches de révision, exercices, cours en plusieurs formats (html, pdf, podcasts et vidéos), conseils et aides.
							Nous faisons apprendre en 3 mois ce qui d’autres font apprendre en 02 ans, grâce à notre nouvelle approche pédagogique, testée et confirmée, avec des formations organisées et orientés entrepreneuriat.

						</p>
					</div>
				</div>
				<div class="container-fluid">
					<div id="frontpage-course-list" class="availablecourses">
						<h2>Page accueil a terminer Design classique &*@ep</h2>
					</div>
				</div>
			</div>
			<!-- End Available Courses -->
			<!-- Start My Courses -->

			<!-- End Faculties Section -->

			<!-- Start Footer section -->
			@include('includes.site_footer')

		</div>




	</body>
</html>

<div>
    @yield('menu')

    @yield('body')

@include('includes.site_footer')



</div>

@yield('javascripts')


font-family: "Montserrat",sans-serif;
