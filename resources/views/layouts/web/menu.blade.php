@extends('layouts.web.base')

@section('menu')


<div class="top-header" style="box-shadow: 0 0 10px 2px rgba(0,0,0,0.2);" id="myHeader">
    <div class="container-fluid">
        <div class="pull-left logocon">
            <a class="logo" href="#">
                <img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572//logo"/>
            </a>
        </div>
        <div class="pull-right navigation-con">
            <header role="banner" class="navbar">
                <nav role="navigation" class="navbar-inner">
                    <div class="container-fluid">
                        <a class="brand" title="Home" href="{{url('/')}}">Accueil</a>
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <div class="loginsection pull-right">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            @if(Auth::guard('student')->user())
                            <a class="signup common" href="{{route('account.index')}}">Mon Compte</a>
                            @else
                            <a class="signup common" href="{{route('student_login')}}">Espace Etudiant</a>
                            @endif
                        </div>
                        <!-- end div .loginsection -->
                        <div class="nav-collapse collapse">
                            <ul class="nav">
                                <li><a  href="{{route('courses.index')}}">Cours</a></li>
                                <li><a  href="#">Methodologie</a></li>
                                <li><a  href="{{route('quiz.index')}}">Quiz</a></li>
                                <li><a  href="{{route('exercises.index')}}">Exercices</a></li>
                                <li><a  href="#">News</a></li>
                                <li class="dropdown">
                                <a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Mon Emploi<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a title="History and Philosophy of Science" href="#">History and Philosophy of Science</a></li>
                                    <li><a title="Methodology of Science – Biology" href="#">Methodology of Science – Biology</a></li>
                                </ul>
                                <ul class="nav pull-right">
                                    <li></li>
                                </ul>
                        </div>
                    </div>
                </nav>
            </header>
        </div>
        <div class="clearfix"></div>
    </div>
<!-- End Bottom Header Section -->
</div>
<!-- End Top Header Section -->

@endsection

