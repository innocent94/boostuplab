<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login</title>

    <!-- CSS landing_page bootstrap -->
    <link href="{{asset('css/css_backend/landing_page/bootstrap.min.css')}}" rel="stylesheet">
  
    <!-- Font Awesome -->
    <link href="{{asset('vendors/vendor_backend/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('build/build_backend/css/custom.css')}}" rel="stylesheet">
    
  </head>

  <body class="login">
    @yield('content')
  </body>


</html>
