<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User create</title>
    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
    <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Créer Utilisateur</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Accueil</a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
					<div class="ibox-title">
                            <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
				</div>
				{!! Form::open(['method'=>'POST', 'action'=> 'AdminUsersController@store', 'class'=>'form-horizontal', 'files'=>true]) !!}
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">                       
                       <div class="ibox-content">
                                <div class="form-group">								
                                    <div class="col-sm-12">
										{!! Form::label('name', 'Nom et Prénoms:') !!}
										{!! Form::text('name', null, ['class'=>'form-control'])!!}
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">								
                                    <div class="col-sm-12">
										{!! Form::label('email', 'Email:') !!}
										{!! Form::text('email', null, ['class'=>'form-control'])!!}
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                
                                <div class="form-group">
                                    <div class="col-sm-6">
									<div class="text-navy"> {!! Form::label('roles', 'Roles:') !!} </div>
										@foreach($roles as $value)
											<label> {{ Form::radio('roles[]', $value, false, array('class' => 'name roles')) }} {{ $value }}</label><br/>
										@endforeach
                                    </div>
                             
								
							
                                    <div class="col-sm-6">
									<div class="text-navy"> {!! Form::label('roles', 'Permissions:') !!} </div>
										@foreach($permissions as $value)
											<label> {{ Form::checkbox('permissions[]', $value, false, array('class' => 'name permissions')) }} {{ $value }}</label><br/>
										@endforeach
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
					<div class="ibox float-e-margins">                       
                       <div class="ibox-content">
							<div class="form-group">								
								<div class="col-sm-12">		
									{!! Form::label('photo_id', 'Photo:') !!}
									
											{!! Form::file('file_id', null)!!}
										 
								</div> 
							</div> 
							<br/>
							<br/>
							<div class="form-group">								
								<div class="col-sm-12">	
									{!! Form::label('password', 'Mot de passe:') !!}
									{!! Form::text('password', null, ['class'=>'form-control'])!!}
								</div>
							</div>
							
							<div class="hr-line-dashed"></div>
							<div class="form-group">								
								<div class="col-sm-12">	
									{!! Form::label('is_active', 'Statut:') !!}
									{!! Form::select('is_active', array(1 => 'Active', 0=> 'Not Active'), 0 , ['class'=>'form-control m-b'])!!}
								</div>
							</div>
						</div> 
					</div> 					
                </div>
				
				<div class="col-lg-12">
                   {!! Form::submit('Enregistrer', ['class'=>'btn btn-primary col-sm-6']) !!}
                </div>
				
				{!! Form::close() !!}
            </div>
        </div><br/><br/>
        @include('includes.ha_footer')

        </div>
        </div>
	
	
	@include('includes.ha_script')

   

    <!-- iCheck -->
    <script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
</body>

</html>
