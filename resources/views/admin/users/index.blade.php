<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User list</title>
    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
    <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Liste des utilisateurs</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Accueil</a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Border Table </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>id</th>
									<th style="width: 5%">Photo</th>
									<th>Nom et Prénoms</th>
									<th style="width: 20%">Rôle </th>
									<th style="width: 20%">Permissions</th>
									<th>Statut</th>
									<th style="width: 10%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($users)
      @foreach($users as $user)
      <tr>
        <td>{{$user->id}}</td>
        <td> <img height="20" src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" alt="" ></td>
        <td><a href="{{route('admin.users.edit', $user->id)}}">{{$user->name}}</a></td>
        <td>
          @if(!empty($user->roles))
          @foreach($user->roles as $v)
          <label class="label label-success">{{ $v->name }}</label>
          @endforeach
          @endif
        </td>

        <td>
         @if(!empty($user->getAllPermissions()))
         @foreach($user->getAllPermissions() as $v)
         <label class="label label-info">{{ $v->name }}</label>
         @endforeach
         @endif       
       </td>


       <td>{{$user->is_active == 1 ? 'Active' : 'Not Active' }}</td>

       <td>
        @can('user_modify')
        <div class="col-xs-4 text-center">
          <a href="{{route('admin.users.edit', $user->id)}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> </a>
        </div>
        @endcan
        @can('user_delete')
        <div class="col-xs-4 text-right">
          <a href="{{route('admin.users.edit', $user->id)}}" class="btn btn-danger btn-xs "><i class="fa fa-trash fa-danger"></i> </a>
        </div>
        @endcan
      </td>
    </tr>
    @endforeach
    @endif
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
        </div>
        </div>
        @include('includes.ha_footer')

        </div>
        </div>
	
	
	@include('includes.ha_script')


    <!-- iCheck -->
    <script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
</body>

</html>
