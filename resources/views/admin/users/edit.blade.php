<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User edit</title>
    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
    <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Modification des informations de l'Utilisateur</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Accueil</a>
                        </li>
						<li>
                            <a href="{{route('admin.users.index')}}">Liste des utilisateurs</a>
                        </li>
						<li>
							<a href="">Modification</a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
					@include('includes.form_error')
				 </div>
            <div class="row">
                <div class="col-lg-12">
					<div class="ibox-title">
                            <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
				</div>
				
				{!! Form::model($user, ['method'=>'PATCH', 'action'=> ['AdminUsersController@update', $user->id], 'class'=>'form-horizontal', 'files'=>true]) !!}
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">                       
                       <div class="ibox-content">
                                <div class="form-group">								
                                    <div class="col-sm-12">
										{!! Form::label('name', 'Nom et Prénoms:') !!}
										{!! Form::text('name', null, ['class'=>'form-control'])!!}
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">								
                                    <div class="col-sm-12">
										{!! Form::label('email', 'Email:') !!}
										{!! Form::text('email', null, ['class'=>'form-control'])!!}
									</div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                
                                <div class="form-group">
                                    <div class="col-sm-6">
									<div class="text-navy"> {!! Form::label('roles', 'Roles:') !!} </div>
										@foreach($roles as $value)
										  <label>{{ Form::checkbox('roles[]', $value, in_array($value, $userRole) ? true : false, array('class' => 'name roles')) }}
										  {{ $value }}</label>
										  <br/>
										 @endforeach 
                                    </div>
                             
								
							
                                    <div class="col-sm-6">
									<div class="text-navy"> {!! Form::label('roles', 'Permissions:') !!} </div>
										@foreach($permissions as $value)
											<label>{{ Form::checkbox('permissions[]', $value->name, in_array($value->name, $userPermissions) ? true : false, array('class' => 'name')) }}
       {{ $value->name }}</label><br/>
										@endforeach
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
					<div class="ibox float-e-margins">                       
                       <div class="ibox-content">
							 <div class="col-sm-2 form-group has-feedback" style="margin-top: 7px">
								<img src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" alt="" class="img-responsive img-rounded">
							 </div>
							<div class="form-group">								
								<div class="col-sm-12">		
									{!! Form::label('photo_id', 'Photo:') !!}
									
											{!! Form::file('photo_id', null)!!}
										 
								</div> 
							</div> 
							<br/>
							<br/>
							<div class="form-group">								
								<div class="col-sm-12">	
									{!! Form::label('password', 'Mot de passe:') !!}
									{!! Form::text('password', '', ['class'=>'form-control'])!!}
								</div>
							</div>
							
							<div class="hr-line-dashed"></div>
							<div class="form-group">								
								<div class="col-sm-12">	
									{!! Form::label('is_active', 'Statut:') !!}
									{!! Form::select('is_active', array(1 => 'Active', 0=> 'Not Active'), null , ['class'=>'form-control'])!!}
								</div>
							</div>
						</div> 
					</div> 					
                </div>
				
				<div class="col-lg-12">
                   {!! Form::submit('Mettre à jour', ['class'=>'btn btn-primary col-sm-6']) !!}
                </div>
				
				{!! Form::close() !!}
				
			
				
				
            </div>
			<br/>
			<div class="row">
			<div class="col-lg-12" onclick="return confirm('Etes vous sûre de vouloir supprimer cet administrateur ?')">
				{!! Form::open(['method'=>'DELETE', 'action'=> ['AdminUsersController@destroy', $user->id]]) !!}
					{!! Form::submit('Supprimer', ['class'=>'btn btn-danger col-sm-6']) !!}
				{!! Form::close() !!} 
				</div>
			</div>
			
        </div><br/><br/>
        @include('includes.ha_footer')

        </div>
        </div>
	
	
	@include('includes.ha_script')

   
    <!-- iCheck -->
    <script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
		<script type="text/javascript">
  // JavaScript code
  $(document).ready(function(){
      $(".roles").change(function(){
        if($(".roles").is(':checked')){
          $("input:checkbox[name='"+$(this).attr("name")+"']").not(this).prop("checked",false);
        }
      });
  });
</script>
        
</body>

</html>
