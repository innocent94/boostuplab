<!DOCTYPE html>
@include('includes.tinyeditor')
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actualite Modification</title>
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
    <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Modification de l'Actualite</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Accueil</a>
                        </li>
						<li>
                            <a href="{{route('admin.news.index')}}">Actualites</a>
                        </li>
						<li>
							<a href="">Modification</a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
					@include('includes.form_error')
				 </div>
            <div class="row">
                <div class="col-lg-12">
					<div class="ibox-title">
                            <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
				</div>

				{!! Form::model($news, ['method'=>'PATCH', 'class'=>'form-horizontal', 'action'=> ['AdminNewsController@update', $news->id], 'files'=>true]) !!}
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">                       
                        <div class="ibox-content">
                            <div class="form-group">								
                                <div class="col-sm-12">
										{!! Form::label('name', 'Titre:', ['class'=>'text-navy']) !!}
										{!! Form::text('title', null, ['class'=>'form-control'])!!}
									</div>
                              								
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">								
									<div class="col-sm-6">		
										{!! Form::label('file_id', 'Image illustrative: ', ['class'=>'text-navy']) !!}										
										{!! Form::file('file_id', null)!!}											 
									</div> 
								</div>
                                <div class="hr-line-dashed"></div>
 
                                <div class="form-group">
									<div class="col-sm-12">
									  {!! Form::label('content', 'Redaction de l\'article', ['class'=>'text-navy']) !!}
									  {!! Form::textarea('content', null, ['class'=>'form-control', 'id'=>'editor'])!!}
									</div>
								</div>
								
								<div class="hr-line-dashed"></div>
								
								<div class="form-group">	
									
									<div class="col-sm-12">	
										{!! Form::label('status_id', 'Statut:', ['class'=>'text-navy']) !!}
										{!! Form::select('status_id', ['1'=>'En redaction', '2'=>'Publié', '3'=>'Suspendu'], null,['class' => 'form-control m-b']) !!}
									</div>
								</div>
                        </div>
                    </div>
                </div>
				
				
				<div class="col-lg-12">
                   {!! Form::submit('Enregistrer', ['class'=>'btn btn-primary col-sm-6']) !!}
                </div>
				
				{!! Form::close() !!}
				
				<div class="col-sm-12">
				{!! Form::open(['method'=>'DELETE', 'action'=> ['AdminNewsController@destroy', $news->id]]) !!}

				  <div class="form-group" onclick="return confirm('Etes vous sûre de vouloir supprimer ce cours ?')">
					{!! Form::submit('Supprimer', ['class'=>'btn btn-danger col-sm-6']) !!}
				  </div>
				  {!! Form::close() !!}
				</div>
				
            </div>
        </div><br/><br/>
        @include('includes.ha_footer')

        </div>
        </div>	
	
	@include('includes.ha_script')
</body>

</html>
