<!DOCTYPE html>
@include('includes.tinyeditor')
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Slider | Modification</title>
    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
    <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Modification Slider </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Accueil</a>
                        </li>
						
						 <li>
                            <a href="{{route('admin.slider.index')}}">Slider</a>
                        </li>
						
						<li>
                            <a href="">Modification</a>
                        </li>
						
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
					@include('includes.form_error')
				 </div>
            <div class="row">
                <div class="col-lg-12">
					<div class="ibox-title">
                            <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
				</div>
				{!! Form::model($slide, ['method'=>'PATCH', 'class'=>'form-horizontal', 'action'=> ['AdminSlideController@update', $slide->id],'files'=>true]) !!}
				              
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">                       
                       <div class="ibox-content">
								<div class="form-group">								
									<div class="col-sm-12">		
										{!! Form::label('photo_id', 'Image:') !!}									
										{!! Form::file('photo_id', null)!!}											 
									</div> 
								</div>

                                <div class="hr-line-dashed"></div>
								<div class="form-group">
									 <div class="col-sm-12">
								  {!! Form::label('description', 'Description') !!}
								  {!! Form::textarea('description', null, ['class'=>'form-control'])!!}
								</div>
								</div>
								<div class="hr-line-dashed"></div>
								
								<div class="form-group">								
                                    <div class="col-sm-12">
										{!! Form::label('Ordre', 'Ordre') !!}
										{!! Form::number('order', null, ['class'=>'form-control', 'max'=>$max, 'min'=>'1', 'id'=>'new_order'])!!}
									</div>
                                </div>
                        </div>
                    </div>
					{!! Form::submit('Valider', ['class'=>'btn btn-primary col-sm-6']) !!}
                </div>                 				
				{!! Form::close() !!}	
				
				<div class="col-sm-12">
				{!! Form::open(['method'=>'DELETE', 'action'=> ['AdminSlideController@destroy', $slide->id]]) !!}

				  <div class="form-group" onclick="return confirm('Etes vous sûre de vouloir supprimer cet element ?')">
					{!! Form::submit('Supprimer', ['class'=>'btn btn-danger col-sm-3']) !!}
				  </div>
				  {!! Form::close() !!}
				</div>
	  
            </div>
        </div><br/><br/>
        @include('includes.ha_footer')

        </div>
        </div>	
	@include('includes.ha_script')
</body>

</html>
