<!DOCTYPE html>
@include('includes.tinyeditor')
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cours Modification</title>
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
    <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Modification du cours</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Accueil</a>
                        </li>
						<li>
                            <a href="{{route('admin.cours.index')}}">Cours</a>
                        </li>
						<li>
							<a href="">Modification</a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
					@include('includes.form_error')
				 </div>
            <div class="row">
                <div class="col-lg-12">
					<div class="ibox-title">
                            <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
				</div>

				{!! Form::model($ebooks, ['method'=>'PATCH', 'class'=>'form-horizontal', 'action'=> ['AdminEbooksController@update', $ebooks->id], 'files'=>true]) !!}
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">                       
                        <div class="ibox-content">
                                <div class="form-group">								
                                    <div class="col-sm-6">
										{!! Form::label('name', 'Titre:', ['class'=>'text-navy']) !!}
										{!! Form::text('title', null, ['class'=>'form-control'])!!}
									</div>
                              								
                                   	
									
									<div class="col-sm-6">	
										{!! Form::label('matiere_id', 'Matiere:', ['class'=>'text-navy']) !!}
										{!! Form::select('matiere_id', [''=>''] + $matieres, null,['class' => 'form-control m-b']) !!}
									</div>
									
									
									
                                </div>
								<div class="hr-line-dashed"></div>
								
								<div class="form-group">
									<div class="col-sm-12">
									  {!! Form::label('objectifs', 'Description & Commentaire a propos du document', ['class'=>'text-navy']) !!}
									  {!! Form::textarea('body', null, ['class'=>'form-control'])!!}
									</div>
								</div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">								
									<div class="col-sm-6">		
										{!! Form::label('illustration_img_id', 'Image illustrative :', ['class'=>'text-navy']) !!}										
										{!! Form::file('illustration_img_id', null)!!}											 
									</div> 
								</div>
                                <div class="hr-line-dashed"></div>
								
									
									<div class="col-sm-6 hide">	
										
										<input name="user_id" value="{{Auth::user()->pluck('id')->first()}}">
									</div>
									
								

                        </div>
                    </div>
                </div>
				
				
				<div class="col-lg-12">
                   {!! Form::submit('Enregistrer', ['class'=>'btn btn-primary col-sm-6']) !!}
                </div>
				
				{!! Form::close() !!}
				
				<div class="col-sm-12">
				{!! Form::open(['method'=>'DELETE', 'action'=> ['AdminEbooksController@destroy', $ebooks->id]]) !!}

				  <div class="form-group" onclick="return confirm('Etes vous sûre de vouloir supprimer ce document ?')">
					{!! Form::submit('Supprimer', ['class'=>'btn btn-danger col-sm-6']) !!}
				  </div>
				  {!! Form::close() !!}
				</div>
				
            </div>
        </div><br/><br/>
        @include('includes.ha_footer')

        </div>
        </div>	
	
	@include('includes.ha_script')
</body>

</html>
