<html>
<head>
  <title>Hpco-ci email</title>
  <style type="text/css">
  @media only screen and (max-width: 600px) {
   table[class="contenttable"] {
     width: 320px !important;
     border-width: 3px!important;
   }
   table[class="tablefull"] {
     width: 100% !important;
   }
   table[class="tablefull"] + table[class="tablefull"] td {
     padding-top: 0px !important;
   }
   table td[class="tablepadding"] {
     padding: 15px !important;
   }
 }
</style>
</head>
<body style="margin:0; border: none; background:#f5f5f5">
  <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tbody><tr>
      <td align="center" valign="top"><table class="contenttable" style="border-width: 8px; border-style: solid; border-collapse: separate; border-color:#ececec; margin-top:40px; font-family:Arial, Helvetica, sans-serif" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="600">
        <tbody><tr>
          <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
              <tr>
                <td height="40" width="100%">&nbsp;</td>
              </tr>
              <tr>
                <td align="center" valign="top"><a href="#"><img alt="" src="{{$message->embed(URL::asset('images/Logo_HPCO-CI_lite.jpg'))}}" style="padding-bottom: 0; display: inline !important;" width="217"></a></td>
              </tr>
              <tr>
                <td height="40" width="100%">&nbsp;</td>
              </tr>
              <tr>
              </tr></tbody>
            </table></td>
          </tr>
          <tr>
            <td></td>
            </tr>
   
            <tr>
            <td class="tablepadding" style="padding:20px; font-size:14px; line-height:20px;"> {!! $data['content'] !!}</td>
            </tr>
            <tr>
              <td class="tablepadding" style="padding:20px 0; border-top-width:1px;border-top-style:solid;border-top-color:#ececec;border-collapse:collapse" bgcolor="#fcfcfc">
                <table style="font-size:11px;color:#999999; padding-bottom: 10px; border-bottom:10px solid #0b1e60;" width="100%">
                  <tbody>
                   
                    <tr>
                      <td style="width: 15%"><img src="{{$message->embed(URL::asset('images/iso9001.jpg'))}}" width="100%"></td>
                      <td style="width: 85%" style="line-height:20px; padding:20px;" align="left"> 
                          NSIA BANQUE CÔTE D’IVOIRE (anciennement BIAO-CI) – SA au capital de FCFA 23.170.000.000 – Siège Social : Abidjan – Plateau, 8-10, Av. Joseph Anoma 01 BP 1274 Abidjan 01 – RCI – Tél. : (225) 20.20.07.20 - Fax. : (225) 20.20.07.00  - Email : nsiabanque.ci@groupensia.com - Site web : www.nsiabanque SWIFT : BIAOCIAB - RCCM : CI-ABJ-1981-B-52039 - LBCI No A 0042 Q - Compte bancaire : CI00 2631100 A00030421 / BCEAO
                      </td>
                    </tr>   
                  </tbody>
                </table>
            </td>
          </tr>
        </tbody></table></td>
      </tr>
    </tbody></table>
  </body>