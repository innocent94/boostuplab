<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Messages</title>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

     <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
		{!! Form::model($messages, ['method'=>'PATCH', 'id'=>'update', 'class'=>'form-horizontal', 'action'=> ['MessageController@update', $messages->id]]) !!}
			<input type="text" name="is_placed" value="important" class="hide">
		{!! Form::close() !!}	
		
		{!! Form::open(['method'=>'DELETE','id'=>'del', 'action'=> ['MessageController@destroy', $messages->id]]) !!}
	
		{!! Form::close() !!}	
		
        <div class="wrapper wrapper-content">
		@include('includes.form_error')
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb" style="padding:15px">
					<li>
						<a href="{{ url('/') }}">Accueil</a>
					</li>
					
					 <li>
						<a href="{{route('admin.messages.index')}}">Messages</a>
					</li>
					
					<li>
						<a href="">Lecture</a>
					</li>
					
				</ol>
			</div>
		</div>
		<br/>
        <div class="row">
            <div class="col-lg-12 animated fadeInRight">
			@foreach($read as $list)
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <a href="{{route('mail', $list->id)}}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Reply"><i class="fa fa-reply"></i> Repondre</a>
                    <a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Print email"><i class="fa fa-print"></i> </a>
                    <a href="mailbox.html" id="delete" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Supprimer"><i class="fa fa-trash-o"></i> </a>
                </div>
                <h2>
                    Lire Message
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">


                    <h3>
                        <span class="font-normal">Object: </span>{{$list->object}}
                    </h3>
                    <h5>
                        <span class="pull-right font-normal">Abidjan le : {{date('d-m-Y', strtotime($list->created_at))}}</span>
                        <span class="font-normal">De: </span>{{$list->first_name}} {{$list->last_name}} 
						<br/>
						<br/>
                        <span class="font-normal">Email: </span>{{$list->email}}
                    </h5>
                </div>
            </div>
                <div class="mail-box">


                <div class="mail-body" style="line-height:25px;">
                    <p>
					{{$list->body}}
                    </p>
                    
                       
                </div>
				@if($list->image_id)
                    <div class="mail-attachment">
                        <p>
                            <span><i class="fa fa-paperclip"></i> 1 fichier joint - </span>
                            <a href="#">Télécharger</a>
                        </p>

                        <div class="attachment">
                            <div class="file-box">
                                <div class="file">
                                    <a href="#">
                                        <span class="corner"></span>

                                        <div class="icon">
                                            <i class="fa fa-file"></i>
                                        </div>
                                        <div class="file-name">
                                            {{$list->image_id}}
                                        </div>
                                    </a>
                                </div>
                            </div>                           
                            <div class="clearfix"></div>
                        </div>
                    </div>
				@endif
				<div class="mail-body text-right tooltip-demo">
						<a class="btn btn-sm btn-success" href="{{route('mail', $list->id)}}"><i class="fa fa-reply"></i> Repondre</a>
						<a class="btn btn-sm btn-warning" href="#" id="important"><i class="fa fa-plus-circle"></i> marquer important</a>
						<button title="" data-placement="top" data-toggle="tooltip" data-original-title="Supprimer" class="btn btn-sm btn-danger" id="delete"><i class="fa fa-trash-o"></i> Supprimer</button>
				
				</div>
                        <div class="clearfix"></div>


                </div>
				@endforeach
            </div>
        </div>
        </div>
         @include('includes.ha_footer')

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('js/inspinia.js')}}"></script>
    <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
	
	<script type="text/javascript">
	  var form = document.getElementById("update");   
	  var del = document.getElementById("del");   
	  $(document).ready(function() {
		$('#important').click(function(){
			form.submit();
		 });

		$('#delete').click(function(){
			alert('Voulez vous supprimer ce message ?')
			del.submit();
		 });
	  });
	</script>

</body>

</html>
