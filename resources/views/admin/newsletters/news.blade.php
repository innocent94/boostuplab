<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Newsletter message</title>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
	
    <link href="{{asset('css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

     <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
		
		
        <div class="wrapper wrapper-content">
		@include('includes.form_error')
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb" style="padding:15px">
					<li>
						<a href="{{ url('/') }}">Accueil</a>
					</li>
					
					 <li>
						<a href="{{route('abonnes')}}">Newsletter</a>
					</li>
					
					<li>
						<a href="">write</a>
					</li>
					
				</ol>
			</div>
		</div>
		<br/>
        <div class="row">
            <div class="col-lg-12 animated fadeInRight">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>
                    <a href="mailbox.html" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                </div>
                <h2>
                    Compose mail
                </h2>
            </div>

                <div class="mail-box">

				{!! Form::model($messages, ['method'=>'PATCH', 'class'=>'form-horizontal', 'action'=> ['NewsletterController@sendmail'],'files'=>true]) !!}
        
                <div class="mail-body">
                   <span class="form-horizontal">
                        <div class="form-group"><label class="col-sm-2 control-label">Object:</label>

                            <div class="col-sm-10"><input type="text" class="form-control" name="title"></div>
                        </div>
                    </span>
					<div class="mail-text h-200">
				

						  {!! Form::textarea('message', null, ['class'=>'form-control summernote'])!!}
						  
                </div>

             
								
                    <div class="mail-body text-right tooltip-demo">
						{!! Form::submit('Envoyer', ['class'=>'btn btn-sm btn-primary']) !!}
                       
                    </div>
                    <div class="clearfix"></div>
	
				{!! Form::close() !!}
                </div>
            </div>


        </div>
        </div>
         @include('includes.ha_footer')

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('js/inspinia.js')}}"></script>
    <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
	
	<!-- SUMMERNOTE -->
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script>
        $(document).ready(function(){

            $('.summernote').summernote();

        });

    </script>


</body>

</html>
