<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Newsletter</title>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
	
	<!-- FooTable -->
    <link href="{{asset('css/plugins/footable/footable.core.css')}}" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

     <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
        <div class="wrapper wrapper-content">
		@include('includes.form_error')
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                            <a class="btn btn-block btn-primary compose-mail" href="{{route('news')}}">Ecrire un message</a>
                            <div class="space-25"></div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">

                <form method="get" action="index.html" class="pull-right mail-search">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Recherche dans le tableau">		
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary">
                                recherche
                            </button>
                        </div>
                    </div>
                </form>
                <h2>
                    Newsletter ({{$all_received}})
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <div class="btn-group pull-right">
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

                    </div>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox"><i class="fa fa-refresh"></i> Refresh</button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as read"><i class="fa fa-eye"></i> </button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as important"><i class="fa fa-exclamation"></i> </button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i> </button>

                </div>
            </div>
                <div class="mail-box">

                <div class="ibox float-e-margins">
					<div class="ibox-content">					
						<table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
									<th style="width:10%">Id</th>
									<th style="width:30%">Nom et prénoms</th>
									<th style="width:45%">email</th>
									<th style="width:20%">Date</th>
                                </tr>
                                </thead>
                                <tbody>
									@foreach($newsletters as $list)
									<tr>
									<td>{{$list->id}}</td>

									<td><a href=""><b>{{$list->name}} <br/> {{$list->prenom}}</b></a></td>
	
										<td><a href="" style="color:#555">{{$list->email}}</a></td>
										<td>{{date('d-m-Y', strtotime($list->created_at))}}</td>
									</tr>
									@endforeach
                                </tbody>
								<tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
					</div> 					
				</div>


                </div>
            </div>
        </div>
        </div>
         @include('includes.ha_footer')

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('js/inspinia.js')}}"></script>
    <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>

    <!-- FooTable -->
    <script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>

    <!-- Page-Level Scripts -->

    <script>
        $(document).ready(function() {
            $('.footable').footable();
            $('.footable2').footable();
        });
    </script>
	
	<script type="text/javascript">
	  $(document).ready(function() {
		$('#not_reads').click(function(){
			var vals = $(this).attr('value')
		  $('#filter').val(vals);	
		  $('#filter').focus();		  
		});

		$('#important').click(function(){
			var vals = $(this).attr('value')
		  $('#filter').val(vals);	
		  $('#filter').focus();		  
		});
	  });
	</script>
	
	
</body>

</html>
