<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz Questions options</title>
    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
    <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Liste des quiz</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Accueil</a>
                        </li>
						<li>
                            <a href="">Modifier les options : <b> </b></a>
                        </li>
						
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
					@include('includes.form_error')
				 </div>
			
			<div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Basic Data Tables example with responsive plugin</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
					<p>
						<a href="{{ route('questions', $topic->id) }}" class="btn btn-success">Ajouter une question</a>
					</p>
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th style="width:5%; vertical align:middlle">N°</th>
                        <th>Questions</th>
                        <th style="width:15%">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
					@if (count($questions_options) > 0)
                        @foreach ($questions_options as $key => $question)
                            <tr>
                                <td style="vertical-align:middle"> {{ $key + 1 }}</td>
                                <td style="vertical-align:middle"><b>{!! $question->question_text !!}</td>
                                <td class="center">
								
										
								
										<a href="" class="btn btn-danger btn-xs "><i class="fa fa-pencil"></i> | <i class="fa fa-trash fa-danger"></i> Question</a>
									 
									  
									
										<a href="{{route('admin.quiz.topics.questions.edit', $question->id)}}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Options</a>
									  
									 
								</td>																					 
						
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">Aucune question inseree</td>
                        </tr>
                    @endif
                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
			
			
        </div>
        @include('includes.ha_footer')

        </div>
        </div>
	
	
	@include('includes.ha_script')

 
	<!-- FooTable -->
	<script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>

    <!-- Page-Level Scripts -->
     <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
</body>

</html>
