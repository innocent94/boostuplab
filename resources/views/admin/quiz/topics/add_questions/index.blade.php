<!DOCTYPE html>
@include('includes.tinyeditor')
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter questions</title>
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
    <!-- Menu -->
	@include('includes.menu')
	<!-- Fin mneu -->

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        @include('includes.ha_top_nav_bar_with_notif')
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2> <b> Ajout de Questions au Quiz </b></h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">Accueil</a>
                        </li>
						<li>
                            <a href="{{ route('admin.quiz.topics.index') }}">Liste Quiz</a>
                        </li>
						<li>
                            <a href="">Ajout de qusetion au Quiz : <b class="text-navy"> {{$topic->title}}</b></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
					<div class="ibox-title">
                            <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
				</div>
			
				{!! Form::model($topic, ['method'=>'PATCH', 'class'=>'form-horizontal', 'action'=> ['AdminTopicsController@add', $topic->id],'files'=>true]) !!}
        
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">                       
                       <div class="ibox-content">
                                {!! Form::text('topic_id', + $topic->id, ['class'=>'form-control hide'])!!}
								<div class="form-group">
									<div class="col-sm-12">
									  {!! Form::label('question_text', 'Poser la Question', ['class'=>'text-navy']) !!}
									   {!! Form::textarea('question_text', old('question_text'), ['class' => 'form-control ', 'placeholder' => '']) !!}
									</div>
								</div>
								<div class="hr-line-dashed"></div>
								
								<div class="form-group">								
                                    <div class="col-sm-12">
										{!! Form::label('option1', 'Option #1', ['class' => 'control-label text-navy']) !!}
										{!! Form::text('option1', null, ['class' => 'form-control ', 'placeholder' => '']) !!}
									</div>     
								</div>
								<div class="hr-line-dashed"></div>
								
								<div class="form-group">								
									<div class="col-sm-12">	
										{!! Form::label('option2', 'Option #2', ['class' => 'control-label text-navy']) !!}
										{!! Form::text('option2',null, ['class' => 'form-control ', 'placeholder' => '']) !!}</div>
                                </div>
								
								<div class="form-group">								
                                    <div class="col-sm-12">
										{!! Form::label('option3', 'Option #3', ['class' => 'control-label text-navy']) !!}
										{!! Form::text('option3', null, ['class' => 'form-control ', 'placeholder' => '']) !!}
									</div>     
								</div>
								<div class="hr-line-dashed"></div>
								
								<div class="form-group">								
									<div class="col-sm-12">	
										{!! Form::label('option4', 'Option #4', ['class' => 'control-label text-navy']) !!}
										{!! Form::text('option4', null, ['class' => 'form-control ', 'placeholder' => '']) !!}</div>
									</div>
                                								
								<div class="hr-line-dashed"></div>
								
								<div class="form-group">	
									<div class="col-sm-12">	
										{!! Form::label('correct', 'Choisir la bonne reponse', ['class' => 'control-label text-navy']) !!}
										{!! Form::select('correct', [''=>''] + $correct_options, null, [''=>'', 'class' => 'form-control']) !!}
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-12">	
										{!! Form::label('answer_explanation', 'Explication de la reponse*', ['class' => 'control-label text-navy']) !!}
										{!! Form::textarea('answer_explanation', old('answer_explanation'), ['class' => 'form-control ', 'placeholder' => '']) !!}
										<p class="help-block"></p>
										@if($errors->has('answer_explanation'))
											<p class="help-block">
												{{ $errors->first('answer_explanation') }}
											</p>
										@endif
									</div>
								</div>
								
                        </div>
                    </div>
                </div>
				
				
				<div class="col-lg-12">
                   {!! Form::submit('Enregistrer', ['class'=>'btn btn-primary col-sm-6']) !!}
                </div>
				
				{!! Form::close() !!}
            </div>
        </div><br/><br/>
        @include('includes.ha_footer')

        </div>
        </div>
	
	
	@include('includes.ha_script')

</body>

</html>
