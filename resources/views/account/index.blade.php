<!DOCTYPE html>
<html  dir="ltr" lang="en" xml:lang="en">
<head>
	<title>Votre compte</title>
	<link rel="shortcut icon" href="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/favicon" />
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600|Raleway:400,700" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="moodle, New account" />
	<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/site/account.css')}}" rel="stylesheet" type="text/css"/>
	
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script type="text/javascript" src="{{asset('js/site/javascript.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/site/javascript_1.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/site/require.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/site/account.js')}}"></script>

	<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

	<meta name="robots" content="noindex" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
</head>
<body>		
	<div id="page">
		<!-- Start Top Header Section -->			
		<div class="top-header">			
			<div class="container-fluid">
				<div class="pull-left logocon">
					<a class="logo" href="http://learningzone.themescustom.com"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572//logo"/></a>
				</div>
				<div class="pull-right navigation-con">
					<header role="banner" class="navbar">
						<nav role="navigation" class="navbar-inner">
							<div class="container-fluid">
								<a class="brand" title="Home" href="http://learningzone.themescustom.com">Accueil</a>
								<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
								<div class="loginsection pull-right">
									<i class="fa fa-user" aria-hidden="true"></i>
									@if(Auth::guard('student')->user())
									<a class="signup common" href="{{route('account.index')}}">Mon Compte</a>
									@else
									<a class="signup common" href="{{route('student_login')}}">Espace Etudiant</a>
									@endif
								</div>
								<!-- end div .loginsection -->
								<div class="nav-collapse collapse">
									<ul class="nav">
										<li><a title="Components" href="{{route('courses.index')}}">Cours</a></li>											
										<li><a title="Components" href="#">Methodologie</a></li>
										<li><a title="Components" href="{{route('quiz.index')}}">Quiz</a></li>
										<li><a title="Components" href="#">Exercices</a></li>
										<li><a title="Components" href="#">News</a></li>
										<li class="dropdown">
										<a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Mon Emploi<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
											<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
										</ul>										
										<ul class="nav pull-right">
											<li></li>
										</ul>
								</div>
							</div>
						</nav>
					</header>
				</div>
				<div class="clearfix"></div>
			</div>
			
			<div class="bottom-header" style="positionn:relative">
				<div class="container-fluid">
					<div class="pull-left"></div>
					<div class="pull-right">
						<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
						<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
					</div>
					<div class="clearfix"></div>
				</div>
				
				@include('includes.message')
			</div>
			<!-- End Bottom Header Section -->					
		</div>
		<!-- End Top Header Section -->
			
		<!-- Start Page Content -->					
		<div id="page-content" class="row-fluid">
			<div class="container-fluid" style="position:relative">
			
				<section id="region-main" class="span12">
					<div role="main">					
						<div class="span3">
							
							<div menuItemName="Client Details" class="panel panel-sidebar panel-sidebar" style="border-radius:0px">
								<div class="panel-heading">
									<h3 class="panel-title">
										<i class="fa fa-user"></i>&nbsp; Vos informations
										<i class="fa fa-chevron-up panel-minimise pull-right"></i>
									</h3>
								</div>
								<div class="panel-body">
									<div style="width:30%; height:70px; float:left;"><img src="{{ asset('images/students/'.basename(Auth::guard('student')->user()->avatar ? Auth::guard('student')->user()->avatar : 'http://placehold.it/100x100')) }}" width="100%" class="img-circle"></div>
									<div style="width:65%; float:right;"> 
										<b>{{Auth::guard('student')->user()->name}}<br>{{Auth::guard('student')->user()->surname}}</b><br>
										ABIDJAN, KOUMASSI, 
										(+225) {{Auth::guard('student')->user()->phone_number}} <br/>
										@if(Auth::guard('student')->user()->active_subscription != 0)
										Compte : Actif <br/>
										Expire le : {{ Auth::guard('student')->user()->end_subscription }}
										@else
										<b> Compte : <span class="danger"> inactif </span></b>	
										@endif
									</div>
													
								</div>
                                    <div class="panel-footer clearfix">
									
										<a href="{{route('student_profile', Auth::guard('student')->user()->id)}}" class="btn btn-success btn-sm btn-block">
										<i class="fa fa-pencil"></i> Modifier mes informations
										</a>
									@if(Auth::guard('student')->user()->active_subscription == 0)
										<a href="{{route('account.subscription.index')}}" class="btn btn-danger btn-sm btn-block">
										<i class="fa fa-arrow-right"></i> Activer votre compte
										</a>
								    @endif
									</div>
							</div>
							
							<div class="panel panel-sidebar panel-sidebar">
								<div class="panel-heading">
									<h3 class="panel-title">
										<i class="fa fa-bookmark"></i>
										&nbsp;Raccourcis
										<i class="fa fa-chevron-up panel-minimise pull-right"></i>
									</h3>
								</div>
								<div class="list-group">
									<a menuItemName="Order New Services" href="/cart.php" class="list-group-item" id="Secondary_Sidebar-Client_Shortcuts-Order_New_Services"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;Cours</a>
									<a menuItemName="Register New Domain" href="/domainchecker.php" class="list-group-item" id="Secondary_Sidebar-Client_Shortcuts-Register_New_Domain"><i class="fa fa-globe fa-fw"></i>&nbsp;Exercies</a>
									<a menuItemName="Logout" href="/logout.php" class="list-group-item" id="Secondary_Sidebar-Client_Shortcuts-Logout"><i class="fa fa-arrow-left fa-fw"></i>&nbsp;Poser une question</a>
									<a menuItemName="Logout" href="/logout.php" class="list-group-item" id="Secondary_Sidebar-Client_Shortcuts-Logout"><i class="fa fa-arrow-left fa-fw"></i>&nbsp;Reponses</a>
									<a menuItemName="Logout" href="/logout.php" class="list-group-item" id="Secondary_Sidebar-Client_Shortcuts-Logout"><i class="fa fa-arrow-left fa-fw"></i>&nbsp;Message(s)</a>
								</div>
							</div>
							
            
						
						</div>
						<div class="span9">
						
							<div class="span12">
								<div class="alert alert-info">
								<div class="panel-body">
									<img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpa1/v/t1.0-1/c3.0.100.100/p100x100/1176410_641202149304196_134362683595865223_n.jpg?oh=2f08e0b97dcb230c1156edf125d10e31&oe=549C63E6&__gda__=1419657514_746df46625c05b91543a4208db9e06bc" class="pic_ina img-thumbnail"/>
									<div class="con_ina">
										Hello <b> {{ Auth::guard('student')->user()->name }} {{ Auth::guard('student')->user()->surname }}</b> Je suis <b> Daniel votre professeur de ****
										<br> Nous avons hate de commencer les cours avec vous. texte a definir plus tard blablabla bla
									</div>  
											<a href="{{route('account.premium.index')}}"> Premium </a>
								</div>
								</div>
							</div>
							@foreach($student as $students)
								
								{{ $students->name }} <br/> {{ $students->active_subscription }}
								
							@endforeach
						</div>
					
					
						
						
						
					</div>
				</section>
			</div>
		</div>
		<!-- End Page Content -->
						
						<!-- Start Top Footer -->
						@include('includes.site_footer')
					</div>
				</body>
			</html>


                            