<!DOCTYPE html>
<html  dir="ltr" lang="en" xml:lang="en">
<head>
	<title> Paiement </title>
	<link rel="shortcut icon" href="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/favicon" />
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600|Raleway:400,700" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="moodle, New account" />
	<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('css/site/account.css')}}" rel="stylesheet" type="text/css" />
	
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script type="text/javascript" src="{{asset('js/site/javascript.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/site/javascript_1.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/site/require.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/site/account.js')}}"></script>

	<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

	<meta name="robots" content="noindex" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script>
		</script>
	
</head>
<body>		
	<div id="page">
		<!-- Start Top Header Section -->			
		<div class="top-header">			
			<div class="container-fluid">
				<div class="pull-left logocon">
					<a class="logo" href="http://learningzone.themescustom.com"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572//logo"/></a>
				</div>
				<div class="pull-right navigation-con">
					<header role="banner" class="navbar">
						<nav role="navigation" class="navbar-inner">
							<div class="container-fluid">
								<a class="brand" title="Home" href="http://learningzone.themescustom.com">Accueil</a>
								<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
								<div class="loginsection pull-right">
									<i class="fa fa-user" aria-hidden="true"></i>
									<a class="signup common" href="{{route('student_login')}}">Espace Etudiant</a>
								</div>
								<!-- end div .loginsection -->
								<div class="nav-collapse collapse">
									<ul class="nav">
										<li><a title="Components" href="#">Cours</a></li>											
										<li><a title="Components" href="#">Methodologie</a></li>
										<li><a title="Components" href="#">Exercices</a></li>
										<li><a title="Components" href="#">News</a></li>
										<li class="dropdown">
										<a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Mon Emploi<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
											<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
										</ul>										
										<ul class="nav pull-right">
											<li></li>
										</ul>
								</div>
							</div>
						</nav>
					</header>
				</div>
				<div class="clearfix"></div>
			</div>
			
			<div class="bottom-header" style="positionn:relative">
				<div class="container-fluid">
						<div class="pull-left"></div>
						<div class="pull-right">
							<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
							<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- Message account status -->
				@include('includes.message')
				<!-- Message account status -->
			</div>
			<!-- End Bottom Header Section -->					
		</div>
		<!-- End Top Header Section -->
			
		<!-- Start Page Content -->					
		<div id="page-content" class="row-fluid">
			<div class="container-fluid" style="position:relative">
			
				<section id="region-main" class="span12">
					<div role="main">					
						
							<div class="span12">
								<div class="alert alert-info">
								<div class="panel-body">
									
									<div id="orangemoney">
										<form id="mobilepaie" class="form-horizontal" method="" autocomplete="off">
										<!--{{ csrf_field() }}-->
											<div class="" style="margin:20px;">
												<h3 style="text-align:center"> Paiement en ligne </h3>
												<br/>
												<div class="span3">
												</div>
												<div class="span6">
													<div class="span12">
														<label for="montant">Montant à Prélever (FCFA)</label>
														<input id="montant" value="{{Auth::guard('student')->user()->amount}}" style="height:35px;  margin-bottom: 15px" name="montant" class="form-control" type="number" readonly>
													</div>

													
													@php($val1 = rand(1,5))
													@php($val2 = rand(5,9))
													<span class="" style="font-weight: bold; margin-top: 15px"> {{$val1}} + {{$val2}} = </span>
													<input type="hidden" id="vlue1" value="{{$val1}}">
													<input type="hidden" id="vlue2" value="{{$val2}}">
													<input id="answer" name="answer" class="form-control" style="height:35px" type="number" required>
													
													<input type="hidden" id="_token" value="{{ csrf_token() }}"/>
													<input type="hidden" value="{{Auth::guard('student')->user()->amount}}">
													<input type="hidden" id="descriptionPaiement" name="descriptionPaiement" value="{{Auth::guard('student')->user()->type_subscription}}">
													<br/>
													<br/>
													<button type="button" class="btn btn-primary" onclick="SearchCarnet()"><i class="fa fa-money"></i> Payer</button>
												</div>
												<div class="span3">
												</div>
											</div>

											<!-- terms -->
											{{--<div class="form-group row">

												<div class="col-sm-offset-3 col-sm-8">

													<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
														<input type="checkbox" class="custom-control-input">
														<span class="custom-control-indicator"></span>
														<span class="custom-control-description"> En cliquant sur "Soumettre la commande" vous acceptez nos "Conditions d’utilisation"</span>
													</label>

												</div>
											</div>--}}

										<!-- Button  -->
											{{--<div class="form-group row">
												<label class="col-sm-3 col-form-label"></label>

												<div class="col-sm-8"><button type="submit" id="button1id" class="btn btn-danger"><i class="fa fa-check"></i> Soumettre la commande</button></div>
											</div>--}}
										</form>
										<div id="back_message"></div>
										<div id="fabloader" style="display: none;margin-left: 40%;margin-top: 20%;">
											<img src="{{asset('images/loder.gif')}}">
										</div>
								</div>
								</div>
								
								
								</div>
							</div>
						
					</div>
				</section>
			</div>
		</div>
		<!-- End Page Content -->
						
				<!-- Start Top Footer -->
				@include('includes.site_footer')
			</div>
		<script>
		function SearchCarnet(){

            var descriptionPaiement = $("#descriptionPaiement").val();
            var montantAPayer = $("#montant").val();
            var answer = $("#answer").val();
            var rp = parseInt($("#vlue1").val()) + parseInt($("#vlue2").val());
            // on vérifie que tous les champs sont remplis
            //alert(searchCarnet);
            if(answer!=rp) {
                //alert('Veuillez saisir la valeur exacte svp');
                swal("Désolé ", "Veuillez saisir la valeur exacte svp", "error")
                return false;
            }
            if(montantAPayer=="") {
                //alert('Veuillez saisir le montant svp');
                swal("Désolé ", "Veuillez saisir le montant svp !", "error")
                return false;
            }
            if(descriptionPaiement== "") {
                //alert('Veuillez saisir la description svp');
                swal("Désolé ", "Veuillez saisir la description svp !", "error")
                return false;

            }else {
                //fin test de vérification et envoi des données
                $.ajax
                ({
                    type: "post",
                    url: "",
                    timeout: 10000,
                    data: 'montantAPayer=' +montantAPayer+"&descriptionPaiement=" +descriptionPaiement+"&_token="+$("#_token").val(),
                    dataType: 'html',

                    beforeSend: function () {
                        $('#fabloader').show();
                        $('form#mobilepaie').hide();
                    },

                    success: function (response) {
                        console.log(response);
                        /*var rep = JSON.parse(response);
                         var nbdata = rep.data.length;
                         var nbdatatable = rep.datatable.length;
                         var table = "";*/

                        $("#back_message").html(response).hide();
                        $('#back_message form').submit();

                    },

                    error: function () {
                        console.log('Une erreur est survenue!');
                    }


                });

            }
        }

    </script>
		</body>
	</html>


                            