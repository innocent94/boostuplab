<!DOCTYPE html>
<html  dir="ltr" lang="en" xml:lang="en">
<head>
	<title> Type abonnement </title>
	<link rel="shortcut icon" href="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/favicon" />
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600|Raleway:400,700" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="moodle, New account" />
	<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('css/site/account.css')}}" rel="stylesheet" type="text/css" />
	
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script type="text/javascript" src="{{asset('js/site/javascript.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/site/javascript_1.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/site/require.js')}}"></script>
	

	<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

	<meta name="robots" content="noindex" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script>
		</script>
	
</head>
<body>		
	<div id="page">
		<!-- Start Top Header Section -->			
		<div class="top-header">			
			<div class="container-fluid">
				<div class="pull-left logocon">
					
				</div>
				<div class="pull-right navigation-con">
					<header role="banner" class="navbar">
						<nav role="navigation" class="navbar-inner">
							<div class="container-fluid">
								<a class="brand" title="Home" href="http://learningzone.themescustom.com">Accueil</a>
								<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
								<div class="loginsection pull-right">
									<i class="fa fa-user" aria-hidden="true"></i>
									<a class="signup common" href="{{route('student_login')}}">Espace Etudiant</a>
								</div>
								<!-- end div .loginsection -->
								<div class="nav-collapse collapse">
									<ul class="nav">
										<li><a title="Components" href="#">Cours</a></li>											
										<li><a title="Components" href="#">Methodologie</a></li>
										<li><a title="Components" href="#">Exercices</a></li>
										<li><a title="Components" href="#">News</a></li>
										<li class="dropdown">
										<a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Mon Emploi<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
											<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
										</ul>										
										<ul class="nav pull-right">
											<li></li>
										</ul>
								</div>
							</div>
						</nav>
					</header>
				</div>
				<div class="clearfix"></div>
			</div>
			
			<div class="bottom-header" style="positionn:relative">
				<div class="container-fluid">
						<div class="pull-left"></div>
						<div class="pull-right">
							<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
							<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- Message account status -->
				@include('includes.message')
				<!-- Message account status -->
			</div>
			<!-- End Bottom Header Section -->					
		</div>
		<!-- End Top Header Section -->
			
		<!-- Start Page Content -->					
		<div id="page-content" class="row-fluid">
			<div class="container-fluid" style="position:relative">
			
				<section id="region-main" class="span12">
					<div role="main">					
						<h3> Pourquoi devenir premium ? </h3>
						<p> Devenir un membre premium te permet de bénéficier encore de nombreuses fonctionnalités, services et avantages afin de t’aider à réussir à coup sûr ton année. </p>
						<p> Tu bénéficieras de notre réseau de professeurs, d’experts-métiers et de success students, qui a pour unique et commune mission de te soutenir pour exercices, devoirs, incompréhensions et autres préoccupations, et ce, via notre plateforme disponible 7jrs/7 et 24h/24 pour toi.</p>
						<p> Désormais tu ne seras plus seul, car toute une équipe travaille pour TOI, pour <strong> TA REUSSITE.</strong></p>
						<br/>
						<h3> Nos Offres Premium </h3>
						
						@foreach($subscription as $subscriptions)
						<div class="alert alert-info">
							
							<img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpa1/v/t1.0-1/c3.0.100.100/p100x100/1176410_641202149304196_134362683595865223_n.jpg?oh=2f08e0b97dcb230c1156edf125d10e31&oe=549C63E6&__gda__=1419657514_746df46625c05b91543a4208db9e06bc" class="pic_ina img-thumbnail"/>
							<div class="con_ina">
								{{ $subscriptions->name }} / {{ $subscriptions->amount }} / {{ $subscriptions->duration }}
							</div>  
							<a href="{{route('account.subscription.validate_subscription', $subscriptions->id)}}"> {{ $subscriptions->name }}  </a>
						
						</div>
						@endforeach
							
						
					</div>
				</section>
			</div>
		</div>
		<!-- End Page Content -->
						
						<!-- Start Top Footer -->
						@include('includes.site_footer')
					</div>
				</body>
			</html>


                            