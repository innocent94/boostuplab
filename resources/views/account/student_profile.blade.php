<!DOCTYPE html>
<html  dir="ltr" lang="en" xml:lang="en">
	<head>
		<title>Votre compte</title>
		<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('css/site/style.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
		
		<meta name="robots" content="noindex">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script type="text/javascript" src="{{asset('js/site/javascript.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/javascript_1.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/require.js')}}"></script>

		<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

		<meta name="robots" content="noindex" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">	
		
	</head>
	<body  id="page-course-view-topics" class="format-topics  path-course path-course-view safari dir-ltr lang-en yui-skin-sam yui3-skin-sam learningzone-themescustom-com pagelayout-course course-4 context-27 category-1 has-region-side-pre used-region-side-pre has-region-side-post used-region-side-post layout-option-langmenu">
	
		<div id="page">
			<!-- Start Top Header Section -->
			<div class="top-header">			
			<div class="container-fluid">
				<div class="pull-left logocon">
					<a class="logo" href="http://learningzone.themescustom.com"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572//logo"/></a>
				</div>
				<div class="pull-right navigation-con">
					<header role="banner" class="navbar">
						<nav role="navigation" class="navbar-inner">
							<div class="container-fluid">
								<a class="brand" title="Home" href="http://learningzone.themescustom.com">Accueil</a>
								<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
								<div class="loginsection pull-right">
									<i class="fa fa-user" aria-hidden="true"></i>
									<a class="signup common" href="{{route('student_login')}}">Espace Etudiant</a>
								</div>
								<!-- end div .loginsection -->
								<div class="nav-collapse collapse">
									<ul class="nav">
										<li><a title="Components" href="{{route('courses.index')}}">Cours</a></li>											
										<li><a title="Components" href="#">Methodologie</a></li>
										<li><a title="Components" href="{{route('quiz.index')}}">Quiz</a></li>
										<li><a title="Components" href="#">Exercices</a></li>
										<li><a title="Components" href="#">News</a></li>
										<li class="dropdown">
										<a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Mon Emploi<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
											<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
										</ul>										
										<ul class="nav pull-right">
											<li></li>
										</ul>
								</div>
							</div>
						</nav>
					</header>
				</div>
				<div class="clearfix"></div>
			</div>
			
			<div class="bottom-header" style="positionn:relative">
				<div class="container-fluid">
						<div class="pull-left"></div>
						<div class="pull-right">
							<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
							<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- Message account status -->
				@if(Auth::guard('student')->user()->active_subscription != 0)
				<div class="" style="height:60px; width:100%; background:green; line-height:60px;"> 
					<div class="container-fluid">
						 Felicitation votre compte est actif "lien"
					</div>
				</div>
				@else
				<div class="" style="height:60px; width:100%; background:red; line-height:60px;"> 
					<div class="container-fluid">
						<i class="fa fa-warning"></i>  Bienvenue sur la plateforme boostuplab 
					</div>
				</div>	
				@endif
				<!-- Message account status -->
			</div>
			<!-- End Bottom Header Section -->					
		</div>
						
						<!-- End Bottom Header Section -->
						<!-- Start Page Content -->
						<div id="page-content" class="row-fluid">
							<div class="container-fluid">
								<section id="region-main" class="span12">
									<span class="notifications" id="user-notifications"></span>
									<div role="main">
										<span id="maincontent"></span>
										<br/>
										<h2>Modifier les informations du compte</h2> 
										Les champs marqué d'un <img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" style="width:8px; height:12px"/>sont obligatoires.
										<!-- Custon error Register -->
										<br/>
										<br/>
										<b>@include('includes.form_error')</b>
										<!-- Custon error Register -->
										
										{!! Form::model($student, ['method'=>'PATCH', 'action'=> ['StudentAccountController@update_profile', $student->id], 'class'=>'mform', "id"=>"mform1", 'files'=>true]) !!}
                		
											<div class="collapsible-actions">
												<span class="collapseexpand">Expand all</span>
											</div>
											<fieldset class="clearfix collapsible"  id="id_createuserandpass">
												<legend class="ftoggler">
													Parametres de connection
													<img style="float:right; margin-top:-70px" src="{{ asset('images/students/'.basename($student->avatar ? $student->avatar : 'http://placehold.it/100x100')) }}" width="100px" height="100px">
													
												</legend>
												<div class="fcontainer clearfix">
													<div id="fitem_id_username" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_username">
																Email
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::email('email', null, ['class'=>'text-ltr', 'maxlength'=>"100", 'size'=>"50", "required"=>"required"])!!}
														</div>         
													</div>
													
													<div id="fitem_id_username" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_username">
																Ancien Mot de passe
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::password('password', null, ['class'=>'text-ltr', 'size'=>"50", "id"=>"First_password"])!!}
														</div>         
													</div>
													
													<div id="fitem_id_username" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_username">
																Nouveau mot de passe
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::password('newpassword', null, ['class'=>'text-ltr', 'size'=>"50", "id"=>"newpassword"])!!}
														</div>         
													</div>
												</div>
											</fieldset>
											<fieldset class="clearfix collapsible"  id="id_supplyinfo" style="margin-top:25px;">
												<legend class="ftoggler">
													Details sur votre identité
												</legend>
												<div class="fcontainer clearfix">
													<div id="fitem_id_firstname" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_email">
																Photo de profile
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::file('avatar', null)!!}
														</div>
													</div>
													
													<div id="fitem_id_firstname" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_email">
																Nom
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::text('name', null, ['class'=>'text-ltr', 'size'=>"25", "id"=>"id_firstname", "required"=>"required"])!!}
														</div>
													</div>
													
													
													<div id="fitem_id_lastname" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_lastname">
																Prenoms
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::text('surname', null, ['class'=>'text-ltr', 'size'=>"50", "id"=>"id_lastname", "required"=>"required"])!!}
														</div>
													</div>
													
													
													<div id="fitem_id_number" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_lastname">
																N° Telephone
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::text('phone_number', null, ['class'=>'text-ltr', 'size'=>"50"])!!}
														</div>
													</div>
													
													
													<div id="fitem_id_number" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_lastname">
																Etablissement
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::text('school_name', null, ['class'=>'text-ltr', 'size'=>"50"])!!}
														</div>
													</div>					
												</div>
											</fieldset>
											<fieldset class="hidden">
												<div>
													<div id="fgroup_id_buttonar" class="fitem fitem_actionbuttons fitem_fgroup ">
														<div class="felement fgroup" data-fieldtype="group">
															
															{!! Form::submit('Modifier', ['class'=>'btn btn-success col-sm-6']) !!}
														</div>
													</div>
													
												</div>
											</fieldset>
										{!! Form::close() !!}
										
										
									</div>
								</section>
							</div>
						</div>
						<!-- End Page Content -->
						
						<!-- Start Top Footer -->
						@include('includes.site_footer')
					</div>
				</body>
			</html>

