<!DOCTYPE html>
<html  dir="ltr" lang="en" xml:lang="en">
	<head>
		<title>New account</title>
		<link rel="shortcut icon" href="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/favicon" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="moodle, New account" />
		<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
		
		<meta name="robots" content="noindex">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script type="text/javascript" src="{{asset('js/site/javascript.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/javascript_1.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/require.js')}}"></script>
		<meta name="robots" content="noindex" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
	</head>
	<body  id="page-login-signup" class="format-site  path-login safari dir-ltr lang-en yui-skin-sam yui3-skin-sam learningzone-themescustom-com pagelayout-login course-1 context-1 notloggedin content-only layout-option-langmenu">
		
		<div id="page">
			<!-- Start Top Header Section -->
			<div class="top-header navbar-fixed-top">
				<div class="container-fluid">
					<div class="pull-left logocon">
						<a class="logo" href="http://learningzone.themescustom.com"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572//logo"/></a>
					</div>
					<div class="pull-right navigation-con">
						<header role="banner" class="navbar ">
							<nav role="navigation" class="navbar-inner">
								<div class="container-fluid">
									<a class="brand" title="Home" href="http://learningzone.themescustom.com">Home</a>
									<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</a>
									<div class="loginsection pull-right">
										<i class="fa fa-user" aria-hidden="true"></i>
										<a class="signup common" href="http://learningzone.themescustom.com/login/signup.php">Register</a>
										<i class="fa fa-sign-in" aria-hidden="true"></i>
										<a class="login common" href="http://learningzone.themescustom.com/login/index.php?sesskey=qVIoGQdmA9Log in " >Log in</a>
									</div>
									<!-- end div .loginsection -->
									<div class="nav-collapse collapse">
										<ul class="nav">
											<li class="dropdown">
												<a href="http://learningzone.themescustom.com/" class="dropdown-toggle" data-toggle="dropdown" title="Features">Features<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li><a title="Fully Responsive" href="#">Fully Responsive</a></li>
													<li><a title="Front Page Banner" href="#">Front Page Banner</a></li>
													<li><a title="Theme Settings" href="#">Theme Settings</a></li>
													<li><a title="Quick links" href="#">Quick links</a></li>
													<li><a title="Social Media" href="#">Social Media</a></li>
													<li><a title="Custom Footer" href="#">Custom Footer</a></li>
													<li><a title="Custom Section" href="#">Custom Section</a></li>
												</ul>
												<li class="dropdown">
													<a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Courses<b class="caret"></b></a>
													<ul class="dropdown-menu">
														<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
														<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
														<li><a title="Advanced Topics in Literature I" href="http://learningzone.themescustom.com/course/view.php?id=5">Advanced Topics in Literature I</a></li>
														<li><a title="Fundamentals of Literary Analysis" href="http://learningzone.themescustom.com/course/view.php?id=6">Fundamentals of Literary Analysis</a></li>
														<li><a title="Human Nature and Society" href="http://learningzone.themescustom.com/course/view.php?id=2">Human Nature and Society</a></li>
													</ul>
													<li><a title="Typography" href="#">Typography</a></li>
													<li><a title="Components" href="#">Components</a></li>
													<li class="dropdown langmenu">
														<a href="" class="dropdown-toggle" data-toggle="dropdown" title="Language">English ‎(en)‎<b class="caret"></b></a>
														<ul class="dropdown-menu">
															<li><a title="Deutsch ‎(de)‎" href="http://learningzone.themescustom.com/login/signup.php?lang=de">Deutsch ‎(de)‎</a></li>
															<li><a title="English ‎(en)‎" href="http://learningzone.themescustom.com/login/signup.php?lang=en">English ‎(en)‎</a></li>
															<li><a title="Español - Internacional ‎(es)‎" href="http://learningzone.themescustom.com/login/signup.php?lang=es">Español - Internacional ‎(es)‎</a></li>
															<li><a title="Español Colombiano ‎(es_co)‎" href="http://learningzone.themescustom.com/login/signup.php?lang=es_co">Español Colombiano ‎(es_co)‎</a></li>
															<li><a title="简体中文 ‎(zh_cn)‎" href="http://learningzone.themescustom.com/login/signup.php?lang=zh_cn">简体中文 ‎(zh_cn)‎</a></li>
														</ul>
													</ul>
													<ul class="nav pull-right">
														<li></li>
													</ul>
												</div>
											</div>
										</nav>
									</header>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<!-- End Top Header Section -->
						<!-- Start Bottom Header Section -->
						<div class="bottom-header navbar-fixed-top">
							<div class="container-fluid">
								<div class="pull-left"></div>
								<div class="pull-right">
									<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
									<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<!-- End Bottom Header Section -->
						<!-- Start Page Content -->
						<div id="page-content" class="row-fluid">
							<div class="container-fluid">
								<section id="region-main" class="span12">
									<span class="notifications" id="user-notifications"></span>
									<div role="main">
										<span id="maincontent"></span>
										<br/>
										<h2>Nouveau compte</h2> 
										Les champs marqué d'un <img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" style="width:8px; height:12px"/>sont obligatoires.
										<!-- Custon error Register -->
										<br/>
										<br/>
										@if(count($errors) > 0 )
											<div class="alert alert-danger">
												@foreach($errors->all() as $error)			
													@if($errors->has('email'))
														<b> Erreur : Cette adresse email est deja utilisee, veuillez utiliser une autre adresse mail  </b>
													@endif
												@endforeach
											</div>
										@endif
										@if(session()->has('success') > 0 )
											<div class="alert alert-success">
												<b> Félicitation votre compte etudiant a bien ete cree connecter vous pour l'activiter </b> ==> <a href="{{route('student_login')}}"> Connexion </a>  
											</div>
										@endif
										<!-- Custon error Register -->
										
										{!! Form::open(['method'=>'POST', 'action'=> 'CreateAccountController@store', "accept-charset"=>"utf-8", "id"=>"mform1", "class"=>"mform", "autocomplete"=>"off"]) !!}
											<div class="collapsible-actions">
												<span class="collapseexpand">Expand all</span>
											</div>
											<fieldset class="clearfix collapsible"  id="id_createuserandpass">
												<legend class="ftoggler">
													Parametres de connection
												</legend>
												<div class="fcontainer clearfix">
													<div id="fitem_id_username" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_username">
																Email
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::email('email', null, ['class'=>'text-ltr', 'maxlength'=>"100", 'size'=>"50", "required"=>"required"])!!}
														</div>         
													</div>
													
													<div id="fitem_id_username" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_username">
																Mot de passe
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::password('password', null, ['class'=>'text-ltr', 'size'=>"50", "id"=>"First_password", "required"=>"required"])!!}
														</div>         
													</div>
													
													<div id="fitem_id_username" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_username">
																Confirmer le mot de passe
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::password('confirmpassword', null, ['class'=>'text-ltr', 'size'=>"50", "id"=>"Password_confirm", "required"=>"required"])!!}
														</div>         
													</div>
												</div>
											</fieldset>
											<fieldset class="clearfix collapsible"  id="id_supplyinfo" style="margin-top:25px;">
												<legend class="ftoggler">
													Details sur votre identité
												</legend>
												<div class="fcontainer clearfix">
													<div id="fitem_id_firstname" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_email">
																Nom
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::text('name', null, ['class'=>'text-ltr', 'size'=>"25", "id"=>"id_firstname", "required"=>"required"])!!}
														</div>
													</div>
													
													
													<div id="fitem_id_lastname" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_lastname">
																Prenoms
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::text('surname', null, ['class'=>'text-ltr', 'size'=>"50", "id"=>"id_lastname", "required"=>"required"])!!}
														</div>
													</div>
													
													
													<div id="fitem_id_number" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_lastname">
																N° Telephone
																<span class="req"><img class="icon " alt="Required field" title="Required field" src="{{asset('images/others/req.gif')}}" /></span>
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::text('phone_number', null, ['class'=>'text-ltr', 'size'=>"50", "required"=>"required"])!!}
														</div>
													</div>
													
													
													<div id="fitem_id_number" class="fitem required fitem_ftext  ">
														<div class="fitemtitle">
															<label for="id_lastname">
																Etablissement
															</label>
														</div>
														<div class="felement ftext" data-fieldtype="text">
															{!! Form::text('school_name', null, ['class'=>'text-ltr', 'size'=>"50"])!!}
														</div>
													</div>
													
												</div>
											</fieldset>
											<fieldset class="hidden">
												<div>
													<div id="fgroup_id_buttonar" class="fitem fitem_actionbuttons fitem_fgroup ">
														<div class="felement fgroup" data-fieldtype="group">
															{!! Form::submit('Creer', ['class'=>'btn btn-primary', "id"=>"id_submitbutton"]) !!}
															{!! Form::reset('Annuler', ['class'=>'btn-cancel', "id"=>"id_cancel"]) !!}
														</div>
													</div>
													
												</div>
											</fieldset>
										{!! Form::close() !!}
										
										
									</div>
								</section>
							</div>
						</div>
						<!-- End Page Content -->
						
						<!-- Start Top Footer -->
						@include('includes.site_footer')
					</div>
				</body>
			</html>

