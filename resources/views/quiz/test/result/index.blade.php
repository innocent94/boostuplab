<!DOCTYPE html>
<html>
	<head>
		<title>Quiz</title>
		<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('css/site/style.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
		
		<meta name="robots" content="noindex">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script type="text/javascript" src="{{asset('js/site/javascript.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/javascript_1.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/require.js')}}"></script>
		
		
       <div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.2"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<style>
		 
		</style>
	</head>
	<body  id="page-course-view-topics" class="format-topics  path-course path-course-view safari dir-ltr lang-en yui-skin-sam yui3-skin-sam learningzone-themescustom-com pagelayout-course course-4 context-27 category-1 has-region-side-pre used-region-side-pre has-region-side-post used-region-side-post layout-option-langmenu">

		
		<div id="page">
			<!-- Start Top Header Section -->
			<!-- Start Top Header Section -->			
		<div class="top-header">			
			<div class="container-fluid">
				<div class="pull-left logocon">
					<a class="logo" href="http://learningzone.themescustom.com"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572//logo"/></a>
				</div>
				<div class="pull-right navigation-con">
					<header role="banner" class="navbar">
						<nav role="navigation" class="navbar-inner">
							<div class="container-fluid">
								<a class="brand" title="Home" href="http://learningzone.themescustom.com">Accueil</a>
								<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
								<div class="loginsection pull-right">
									<i class="fa fa-user" aria-hidden="true"></i>
									<a class="signup common" href="{{route('student_login')}}">Espace Etudiant</a>
								</div>
								<!-- end div .loginsection -->
								<div class="nav-collapse collapse">
									<ul class="nav">
										<li><a title="Components" href="#">Cours</a></li>											
										<li><a title="Components" href="#">Methodologie</a></li>
										<li><a title="Components" href="#">Exercices</a></li>
										<li><a title="Components" href="#">News</a></li>
										<li class="dropdown">
										<a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Mon Emploi<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
											<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
										</ul>										
										<ul class="nav pull-right">
											<li></li>
										</ul>
								</div>
							</div>
						</nav>
					</header>
				</div>
				<div class="clearfix"></div>
			</div>
			
			<div class="bottom-header" style="positionn:relative">
				<div class="container-fluid">
						<div class="pull-left"></div>
						<div class="pull-right">
							<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
							<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- Message account status -->
				@if(Auth::guard('student')->user()->active_subscription != 0)
				<div class="" style="height:60px; width:100%; background:green; line-height:60px;"> 
					<div class="container-fluid">
						 Felicitation votre compte est actif "lien"
					</div>
				</div>
				@else
				<div class="" style="height:60px; width:100%; background:red; line-height:60px;"> 
					<div class="container-fluid">
						<i class="fa fa-warning"></i>  Votre compte est inactif veuiillez l'activer maintenant pour beneficier de tous les avantages "lien"
					</div>
				</div>	
				@endif
				<!-- Message account status -->
			</div>
			<!-- End Bottom Header Section -->					
		</div>
		<!-- End Top Header Section -->
		
					
						<!-- End Bottom Header Section -->
						
						<!-- End Block Section -->
						<!-- Start Internal Banner Section -->
						<div class="banner-inner">
							<div class="container-fluid">
								<div class="pageheadingcon">
									<h1>Quiz Résultats</h1>
								</div>
							</div>
						</div>
						<!-- End Internal Banner -->
						<!-- Start Breadcrumb -->
						<div id="page-navbar" class="clearfix">
							<div class="container-fluid">
								<nav class="breadcrumb-nav">
									<nav aria-labelledby="navbar-label">
										<ul class="breadcrumb">
											<li>
												<span itemscope="">
													<a itemprop="url" href="#"><span itemprop="title">Accueil</span></a>
												</span>
												<span class="divider">
													<span class="accesshide " ><span class="arrow_text">/</span>&nbsp;</span>
													<span class="arrow sep">&#x25BA;</span>
												</span>
											</li>
											<li>
												<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
													<a itemprop="url" href=""><span itemprop="title">Quiz</span></a>
												</span>
												<span class="divider">
													<span class="accesshide " ><span class="arrow_text">/</span>&nbsp;</span>
													<span class="arrow sep">&#x25BA;</span>
												</span>
											</li>
											<li>
												<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
													<a itemprop="url" href=""><span itemprop="title">{{$topics->title}}</span></a>
												</span>
												<span class="divider">
													<span class="accesshide " ><span class="arrow_text">/</span>&nbsp;</span>
													<span class="arrow sep">&#x25BA;</span>
												</span>
											</li>
											<li>
												<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
													<a itemprop="url" href=""><span itemprop="title">résultat</span></a>
												</span>
												<span class="divider">
													<span class="accesshide " ><span class="arrow_text">/</span>&nbsp;</span>
													<span class="arrow sep">&#x25BA;</span>
												</span>
											</li>
										</ul>
									</nav>
								</nav>
								<div class="breadcrumb-button"></div>
							</div>
						</div>
						<!-- End Breadcrumb -->
						<!-- Start Page Content -->
						<div id="page-content" class="row-fluid">
							<div class="container-fluid">
								<div id="region-main-box" class="span12">
									<div class="row-fluid">
										<section id="region-main" style="margin:auto; max-width:900px;">
											<span class="notifications" id="user-notifications"></span>
											<div role="main">
												<span id="maincontent"></span>
												    
												<div class="course-content">
													<h2>Résultats</h2>
													
													<table class="table table-bordered table-striped">
														<tr>
															<th >Sujet</th>
															<td>{{$topics->title}}</td>
														</tr>
														<tr>
															<th >Matière</th>
															<td>{{$matiere->name}}</td>
														</tr>
														<tr>
															<th >Nombre de question</th>
															<td>10</td>
														</tr>
														<tr>
															<th style="width:30%">Bonne(s) reponse(s)</th>
															<td>{{$ok}}</td>
														</tr>
														<tr>
															<th>Mauvaise(s) reponse(s)</th>
															<td>{{$notok}}</td>
														</tr>
														<tr>
															<th>Note</th>
															<td>{{ $result }}/20</td>
														</tr>
													</table>
													
													
													<br/>
													
													
													
													
													<h2>Correction</h2>
		
													<?php 
														foreach($QuestionId as $key => $list){ 
														$question = DB::table('questions')->where('id', $list)->get()->all();														
														$questions_options = DB::table('questions_options')->where('question_id', $list)->get()->all();														
													  ?>
													
													@foreach($question as $questions)
													<table class="table table-bordered table-striped">
														
														
														
														<tr class="test-option{{ $result->correct ? '-true' : '-false' }}">
															
															@if($Answers[$key] == 1)
																<th style="width: 30%; color:#fff; background:#26B99A;">Question #{{ $key+1 }}</th>
															@else
																<th style="width: 30%; color:#fff; background:#e20000;">Question #{{ $key+1 }}</th>
															@endif
															
															
															@if($Answers[$key] == 1)
																<th style="color:#fff; background:#26B99A;">{!! $questions->question_text !!}</th>
															@else
																<th style="color:#fff; background:#e20000;">{!! $questions->question_text !!}</th>
															@endif
															
														</tr>
														
														<tr>
															<td>Options</td>
															<td>
																<ul>
																@foreach($questions_options as $option)
																	@if($option->correct == 1)
																		@if($option->id == $OptionSelected[$key])
																			<b style="color:#26B99A; text-decoration:underline"><li> {{ $option->option }} : Bonne reponse Felicitation</li></b>
																		@else	
																			<b style="color:#26B99A"><li> {{ $option->option }} => La bonne reponse</li></b>
																		@endif
																	@else
																		@if($option->id == $OptionSelected[$key])
																			<li style="color:#e20000; text-decoration:underline"> {{ $option->option }} => Votre reponse</li>
																		@else	
																			<li> {{ $option->option }} </li>
																		@endif
																	@endif
																@endforeach
																</ul>
															</td>
														</tr>
														<tr>
															<td>Explications</td>
															<td>
																{!! $questions->answer_explanation  !!}											
															</td>
														</tr>
													</table>
													@endforeach
																										
														<?php } ?>
											
													
												
												</div>
												<a href="" class="btn btn-danger"> Reessayer le Quiz </a>
												<hr/>
											</div>
											
											
											
											<!-- comments container -->
											<br/>
											<h3 class="headingtag"># Commentaires</h3>
											<div class="comment_block">											
												<div class="fb-comments" data-href="{{route('test', $topics->id)}}" data-width="" data-numposts="5"></div>											
											</div>


										</section>
									</div>
								</div>
							</div>
						</div>

						<!-- Start Top Footer -->
						<div class="top-footer">
							<div class="container-fluid">
								<ul class="social">
									<li>
										<a href="http://www.facebook.com/mycollege" target="_blank" >
											<i class="facebook"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/facebook.jpg" alt="Facebook"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.twitter.com/mycollege" target="_blank" >
											<i class="twitter"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/twitter.jpg" alt="twitter"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.googlepluse.com/mycollege" target="_blank" >
											<i class="googlepluse"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/googlepluse.jpg" alt="googlepluse"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.pinterest.com/mycollege" target="_blank" >
											<i class="pinterest"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/pinterest.jpg" alt="pinterest"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.vimeo.com/mycollege" target="_blank" >
											<i class="vimeo"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/vimeo.jpg" alt="vimeo"/></i>
										</a>
									</li>
									<li>
										<a href="https://git-scm.com/mycollege" target="_blank" >
											<i class="git"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/git.jpg" alt="git"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.yahoo.com/mycollege" target="_blank" >
											<i class="yahoo"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/yahoo.jpg" alt="yahoo"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.linkdin.com/mycollege" target="_blank" >
											<i class="linkdin"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/linkdin.jpg" alt="linkdin"/></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- End Top Footer -->
						<!-- Start Footer section -->
						<footer id="page-footer">
							<div class="container-fluid">
								<div class="row-fluid">
									<div class="span3">
										<h5>
											ABOUT learningzone
										</h5>
										<ul>
											<li>Duis autem vel eum iriure dolor inhendrerit in vulputate velit esse molestieconsequat, vel illum dolore eu feugiatnulla facilisis at vero eros.</li>
											<li><a href="mailto:cmsbrand93@gmail.com">cmsbrand93@gmail.com</a></li>
											<li>+00 123-456-789</li>
											<li>123 6th St.Melbourne, FL 32904</li>
										</ul>
									</div>
									<div class="span3">
										<h5>
											INFORMATION
										</h5>
										<ul class="common">
											<li><a href="#">About Us</a></li>
											<li><a href="#">Our Stories</a></li>
											<li><a href="#">My Account</a></li>
											<li><a href="#">Our History</a></li>
											<li><a href="#">Sprcialist Info</a></li>
										</ul>
									</div>
									<div class="span3">
										<h5>
											STUDENT HELP
										</h5>
										<ul class="common">
											<li><a href="#">My Info</a></li>
											<li><a href="#">My Questions</a></li>
											<li><a href="#">F.A.Q</a></li>
											<li><a href="#">Serch Courses</a></li>
											<li><a href="#">Latest Informations</a></li>
										</ul>
									</div>
									<div class="span3">
										<h5>
											INSTAGRAM
										</h5>
										<ul class="instagram-pics">
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-1-image" alt="instagram-1-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-2-image" alt="instagram-2-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-3-image" alt="instagram-3-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-4-image" alt="instagram-4-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-5-image" alt="instagram-5-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-6-image" alt="instagram-6-image" /></a>
											</li>
										</ul>
										<span class="followus"><a href="#">Follow Us</a></span>
									</div>
								</div>
								<div class="clearfix"></div>
								<ul class="copyright">
									<li><p class="copy">Copyright &copy; 2019 CmsBrand</p></li>
								</ul>
							</div>
						</footer>
						<!-- End Footer section -->
						<!-- Start Bottom Footer section -->
						<div class="bottom-footer">
							<div id="course-footer"></div>
							<p class="helplink"></p>
							<div class="logininfo">
								You are currently using guest access (
								<a href="http://learningzone.themescustom.com/login/index.php">Log in</a>
								)
							</div>
						</div>
						<!-- End Bottom Footer section -->
						
						<!-- Start Back To Top -->
						<div id="backtotop" style="display: none;">
							<a class="scrollup" href="javascript:void(0);" title="Back to top"></a>
						</div>
						<!-- End Back To Top -->
						
					</div>
					
					<style> 
						#correction {			 
						  display: none;
						}
					</style>

					<script> 
					$(document).ready(function(){
					  $("#vew_answers").click(function(){
						$("#correction").slideToggle("slow");
					  });
					});
					</script>
					
	</body>
</html>
