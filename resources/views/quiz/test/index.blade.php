<!DOCTYPE html>
<html  dir="ltr" lang="en" xml:lang="en">
	<head>
		<title>Quiz</title>
		<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('css/site/style.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
		
		<meta name="robots" content="noindex">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script type="text/javascript" src="{{asset('js/site/javascript.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/javascript_1.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/require.js')}}"></script>
		
		
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.2"></script>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	</head>
	<body  id="page-course-view-topics" class="format-topics  path-course path-course-view safari dir-ltr lang-en yui-skin-sam yui3-skin-sam learningzone-themescustom-com pagelayout-course course-4 context-27 category-1 has-region-side-pre used-region-side-pre has-region-side-post used-region-side-post layout-option-langmenu">

		
		<div id="page">
			<!-- Start Top Header Section -->
			<!-- Start Top Header Section -->			
		<div class="top-header">			
			<div class="container-fluid">
				<div class="pull-left logocon">
					<a class="logo" href="http://learningzone.themescustom.com"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572//logo"/></a>
				</div>
				<div class="pull-right navigation-con">
					<header role="banner" class="navbar">
						<nav role="navigation" class="navbar-inner">
							<div class="container-fluid">
								<a class="brand" title="Home" href="http://learningzone.themescustom.com">Accueil</a>
								<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
								<div class="loginsection pull-right">
									<i class="fa fa-user" aria-hidden="true"></i>
									<a class="signup common" href="{{route('student_login')}}">Espace Etudiant</a>
								</div>
								<!-- end div .loginsection -->
								<div class="nav-collapse collapse">
									<ul class="nav">
										<li><a title="Components" href="#">Cours</a></li>											
										<li><a title="Components" href="#">Methodologie</a></li>
										<li><a title="Components" href="#">Exercices</a></li>
										<li><a title="Components" href="#">News</a></li>
										<li class="dropdown">
										<a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Mon Emploi<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
											<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
										</ul>										
										<ul class="nav pull-right">
											<li></li>
										</ul>
								</div>
							</div>
						</nav>
					</header>
				</div>
				<div class="clearfix"></div>
			</div>
			
			<div class="bottom-header" style="positionn:relative">
				<div class="container-fluid">
						<div class="pull-left"></div>
						<div class="pull-right">
							<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
							<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- Message account status -->
				@if(Auth::guard('student')->user()->active_subscription != 0)
				<div class="" style="height:60px; width:100%; background:green; line-height:60px;"> 
					<div class="container-fluid">
						 Felicitation votre compte est actif "lien"
					</div>
				</div>
				@else
				<div class="" style="height:60px; width:100%; background:red; line-height:60px;"> 
					<div class="container-fluid">
						<i class="fa fa-warning"></i>  Votre compte est inactif veuiillez l'activer maintenant pour beneficier de tous les avantages "lien"
					</div>
				</div>	
				@endif
				<!-- Message account status -->
			</div>
			<!-- End Bottom Header Section -->					
		</div>
		<!-- End Top Header Section -->
		
					
						<!-- End Bottom Header Section -->
						
						<!-- End Block Section -->
						<!-- Start Internal Banner Section -->
						<div class="banner-inner">
							<div class="container-fluid">
								<div class="pageheadingcon">
									<h1>Quiz</h1>
								</div>
							</div>
						</div>
						<!-- End Internal Banner -->
						<!-- Start Breadcrumb -->
						<div id="page-navbar" class="clearfix">
							<div class="container-fluid">
								<nav class="breadcrumb-nav">
									<nav aria-labelledby="navbar-label">
										<ul class="breadcrumb">
											<li>
												<span itemscope="">
													<a itemprop="url" href="#"><span itemprop="title">Accueil</span></a>
												</span>
												<span class="divider">
													<span class="accesshide " ><span class="arrow_text">/</span>&nbsp;</span>
													<span class="arrow sep">&#x25BA;</span>
												</span>
											</li>
											<li>
												<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
													<a itemprop="url" href="http://learningzone.themescustom.com/course/index.php"><span itemprop="title">Quiz</span></a>
												</span>
												<span class="divider">
													<span class="accesshide " ><span class="arrow_text">/</span>&nbsp;</span>
													<span class="arrow sep">&#x25BA;</span>
												</span>
											</li>
											<li>
												<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
													<a itemprop="url" href="http://learningzone.themescustom.com/course/index.php"><span itemprop="title">{{$topics->title}}</span></a>
												</span>
												<span class="divider">
													<span class="accesshide " ><span class="arrow_text">/</span>&nbsp;</span>
													<span class="arrow sep">&#x25BA;</span>
												</span>
											</li>
										</ul>
									</nav>
								</nav>
								<div class="breadcrumb-button"></div>
							</div>
						</div>
						<!-- End Breadcrumb -->
						<!-- Start Page Content -->
						<div id="page-content" class="row-fluid">
							<div class="container-fluid">
								<div id="region-main-box" class="span12">
									<div class="row-fluid">																				
										<section id="region-main"  style="margin:auto; max-width:900px;">
											<div role="main">
												<span id="maincontent"></span>
												
												<div class="course-content">
													<h2>Quiz : {{$topics->title}}</h2>
													<hr />
													<ul class="topics">
													
													{!! Form::model($topics, ['method'=>'PATCH', 'class'=>'form-horizontal', 'action'=> ['PremiumQuizController@store_quiz_test', $topics->id],'files'=>true]) !!}												
																									
														<?php //dd($questions) ?>
														@if(count($questions) > 0)
															<div class="panel-body">
															<?php $i = 1; ?>
															@foreach($questions as $question)
																@if ($i > 1) <hr /> @endif															
																	<strong> {{ $i }}/ {!! strip_tags($question->question_text) !!}</strong>
																	<input type="hidden" name="questions[{{ $i }}]" value="{{ $question->id }}">
																	<div style="margin-top:5px;">
																	@foreach($question->options as $option)								
																		<label>
																			<input type="radio" name="answers[{{ $question->id }}]" value="{{ $option->id }}" required>
																			{{ $option->option }}
																		</label>
																	@endforeach
																	<div>
															<?php $i++; ?>
															@endforeach	
															<br/>
															<hr/>
															{!! Form::submit(trans('Valider vos reponses'), ['class' => 'btn-danger']) !!}
															</div>
														@endif
																											
													{!! Form::close() !!}   	
													
													</ul>													
												</div>
											</div>
										</section>
									</div>
								</div>
							</div>
						</div>
						<!-- Start Page Content -->
						<script type="text/javascript">
							var activeurl = window.location;
							$('.panel-heading a[href="' + activeurl + '"]').addClass('active');
						</script>
						<!-- Start Top Footer -->
						<div class="top-footer">
							<div class="container-fluid">
								<ul class="social">
									<li>
										<a href="http://www.facebook.com/mycollege" target="_blank" >
											<i class="facebook"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/facebook.jpg" alt="Facebook"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.twitter.com/mycollege" target="_blank" >
											<i class="twitter"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/twitter.jpg" alt="twitter"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.googlepluse.com/mycollege" target="_blank" >
											<i class="googlepluse"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/googlepluse.jpg" alt="googlepluse"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.pinterest.com/mycollege" target="_blank" >
											<i class="pinterest"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/pinterest.jpg" alt="pinterest"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.vimeo.com/mycollege" target="_blank" >
											<i class="vimeo"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/vimeo.jpg" alt="vimeo"/></i>
										</a>
									</li>
									<li>
										<a href="https://git-scm.com/mycollege" target="_blank" >
											<i class="git"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/git.jpg" alt="git"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.yahoo.com/mycollege" target="_blank" >
											<i class="yahoo"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/yahoo.jpg" alt="yahoo"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.linkdin.com/mycollege" target="_blank" >
											<i class="linkdin"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/linkdin.jpg" alt="linkdin"/></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- End Top Footer -->
						<!-- Start Footer section -->
						<footer id="page-footer">
							<div class="container-fluid">
								<div class="row-fluid">
									<div class="span3">
										<h5>
											ABOUT learningzone
										</h5>
										<ul>
											<li>Duis autem vel eum iriure dolor inhendrerit in vulputate velit esse molestieconsequat, vel illum dolore eu feugiatnulla facilisis at vero eros.</li>
											<li><a href="mailto:cmsbrand93@gmail.com">cmsbrand93@gmail.com</a></li>
											<li>+00 123-456-789</li>
											<li>123 6th St.Melbourne, FL 32904</li>
										</ul>
									</div>
									<div class="span3">
										<h5>
											INFORMATION
										</h5>
										<ul class="common">
											<li><a href="#">About Us</a></li>
											<li><a href="#">Our Stories</a></li>
											<li><a href="#">My Account</a></li>
											<li><a href="#">Our History</a></li>
											<li><a href="#">Sprcialist Info</a></li>
										</ul>
									</div>
									<div class="span3">
										<h5>
											STUDENT HELP
										</h5>
										<ul class="common">
											<li><a href="#">My Info</a></li>
											<li><a href="#">My Questions</a></li>
											<li><a href="#">F.A.Q</a></li>
											<li><a href="#">Serch Courses</a></li>
											<li><a href="#">Latest Informations</a></li>
										</ul>
									</div>
									<div class="span3">
										<h5>
											INSTAGRAM
										</h5>
										<ul class="instagram-pics">
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-1-image" alt="instagram-1-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-2-image" alt="instagram-2-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-3-image" alt="instagram-3-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-4-image" alt="instagram-4-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-5-image" alt="instagram-5-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-6-image" alt="instagram-6-image" /></a>
											</li>
										</ul>
										<span class="followus"><a href="#">Follow Us</a></span>
									</div>
								</div>
								<div class="clearfix"></div>
								<ul class="copyright">
									<li><p class="copy">Copyright &copy; 2019 CmsBrand</p></li>
								</ul>
							</div>
						</footer>
						<!-- End Footer section -->
						<!-- Start Bottom Footer section -->
						<div class="bottom-footer">
							<div id="course-footer"></div>
							<p class="helplink"></p>
							<div class="logininfo">
								You are currently using guest access (
								<a href="http://learningzone.themescustom.com/login/index.php">Log in</a>
								)
							</div>
						</div>
						<!-- End Bottom Footer section -->
						
						<!-- Start Back To Top -->
						<div id="backtotop" style="display: none;">
							<a class="scrollup" href="javascript:void(0);" title="Back to top"></a>
						</div>
						<!-- End Back To Top -->
						
					</div>
					
					
					<script type="text/javascript">
					  // JavaScript code
					  var form = document.getElementById("filter");  		  
					  $(document).ready(function(){
						  $("input:checkbox").change(function(){
							if($("input:checkbox").is(':checked')){
							  $("input:checkbox[name='"+$(this).attr("name")+"']").not(this).prop("checked",false);							  
								  if(this.value == 0){
									$('#filter_status').val('');  
								  }else{
									$('#filter_status').val(this.value);  
								  }							 
							}else{
							  $('#filter_status').val('');
							}
							form.submit();
						  });
						  
						  $(".matiere a").click(function(){
							var id = $(this).attr('value');
							$('#filter_matiere').val(id);
							form.submit();
						  });
					  });
					</script>
					
					
					
					<!-- Current link active -->
					<script type="text/javascript">
						// JavaScript code
						$(document).ready(function(){
							var current = $('#filter_status').val();
							if(current == 1){
								$('#checkbox_3').prop("checked",true);
							}else if(current == 2){
								$('#checkbox_2').prop("checked",true);
							}else{
								$('#checkbox_1').prop("checked",true);
							}
						});
					</script>
					
	</body>
</html>
