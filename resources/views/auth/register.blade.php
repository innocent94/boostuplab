@extends('layouts.app2')

@section('content')
<div>
   @include('includes/landing_page_background')
    <div class="login_wrapper">
        <div id="register" class="animate">
          <section class="login_content">
            <form role="form" method="POST" action="{{ url('/register') }}">
              <h1>Créer un compte</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" name="name" value="{{ old('name') }}" />
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required="" />
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="password" required="" />
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password confirm" name="password_confirmation" required="" />
                 @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
              </div>
              <div>
                <button type="submit"  class="btn_">Valider</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Vous avez dejà un compte ?
                  <a href="{{ url('/login') }}" class="to_register"> Connectez vous </a>
                </p>

                <div class="clearfix"></div>
               
              </div>
            </form>
          </section>
        </div>
    </div>
</div>
<div class="footer_fixed">
      <div class="container">
        @include('includes/ha_footer')
      </div>
</div>
@endsection
