<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Connexion</title>
		<link rel="shortcut icon" href="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/favicon">
		
		<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
		
		<meta name="robots" content="noindex">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script type="text/javascript" src="{{asset('js/site/javascript.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/javascript_1.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/require.js')}}"></script>

	</head>
	<body id="page-login-index" class="format-site  path-login safari dir-ltr lang-en yui-skin-sam yui3-skin-sam learningzone-themescustom-com pagelayout-login course-1 context-1 notloggedin content-only layout-option-langmenu jsenabled">
		
		<script type="text/javascript" src="{{asset('js/site/javascript_2.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/javascript_3.js')}}"></script>
		<div id="page">
			<!-- Start Top Header Section -->
			<div class="top-header navbar-fixed-top original" style="visibility: hidden;">
				<div class="container-fluid">
					<div class="pull-left logocon">
						<a class="logo" href="http://learningzone.themescustom.com/"><img src="./LearningZone_ Log in to the site_files/logo.png"></a>
					</div>
					<div class="pull-right navigation-con">
						<header role="banner" class="navbar ">
							<nav role="navigation" class="navbar-inner">
								<div class="container-fluid">
									<a class="brand" title="Home" href="http://learningzone.themescustom.com/">Home</a>
									<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</a>
									<div class="loginsection pull-right">
										<i class="fa fa-user" aria-hidden="true"></i>
										<a class="signup common" href="http://learningzone.themescustom.com/login/signup.php">Register</a>
										<i class="fa fa-sign-in" aria-hidden="true"></i>
										<a class="login common" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in">Log in</a>
									</div>
									<!-- end div .loginsection -->
									<div class="nav-collapse collapse">
										<ul class="nav">
											<li class="dropdown">
												<a href="http://learningzone.themescustom.com/" class="dropdown-toggle" data-toggle="dropdown" title="Features">Features<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li><a title="Fully Responsive" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Fully Responsive</a></li>
													<li><a title="Front Page Banner" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Front Page Banner</a></li>
													<li><a title="Theme Settings" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Theme Settings</a></li>
													<li><a title="Quick links" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Quick links</a></li>
													<li><a title="Social Media" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Social Media</a></li>
													<li><a title="Custom Footer" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Custom Footer</a></li>
													<li><a title="Custom Section" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Custom Section</a></li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Courses<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
													<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
													<li><a title="Advanced Topics in Literature I" href="http://learningzone.themescustom.com/course/view.php?id=5">Advanced Topics in Literature I</a></li>
													<li><a title="Fundamentals of Literary Analysis" href="http://learningzone.themescustom.com/course/view.php?id=6">Fundamentals of Literary Analysis</a></li>
													<li><a title="Human Nature and Society" href="http://learningzone.themescustom.com/course/view.php?id=2">Human Nature and Society</a></li>
												</ul>
											</li>
											<li><a title="Typography" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Typography</a></li>
											<li><a title="Components" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Components</a></li>
											<li class="dropdown langmenu">
												<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in" class="dropdown-toggle" data-toggle="dropdown" title="Language">English ‎(en)‎<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li><a title="Deutsch ‎(de)‎" href="http://learningzone.themescustom.com/login/index.php?lang=de">Deutsch ‎(de)‎</a></li>
													<li><a title="English ‎(en)‎" href="http://learningzone.themescustom.com/login/index.php?lang=en">English ‎(en)‎</a></li>
													<li><a title="Español - Internacional ‎(es)‎" href="http://learningzone.themescustom.com/login/index.php?lang=es">Español - Internacional ‎(es)‎</a></li>
													<li><a title="Español Colombiano ‎(es_co)‎" href="http://learningzone.themescustom.com/login/index.php?lang=es_co">Español Colombiano ‎(es_co)‎</a></li>
													<li><a title="简体中文 ‎(zh_cn)‎" href="http://learningzone.themescustom.com/login/index.php?lang=zh_cn">简体中文 ‎(zh_cn)‎</a></li>
												</ul>
											</li>
										</ul>
										<ul class="nav pull-right">
											<li></li>
										</ul>
									</div>
								</div>
							</nav>
						</header>
					</div>
					<div class="clearfix"></div>
				</div>
			</div> 
			<div class="top-header navbar-fixed-top duplicate" style="position: fixed; top: 0px; margin-top: 0px; z-index: 999999; left: 0px; width: 0px; display: block;">
				<div class="container-fluid">
					<div class="pull-left logocon">
						<a class="logo" href="http://learningzone.themescustom.com/"><img src="./LearningZone_ Log in to the site_files/logo.png"></a>
					</div>
					<div class="pull-right navigation-con">
						<header role="banner" class="navbar ">
							<nav role="navigation" class="navbar-inner">
								<div class="container-fluid">
									<a class="brand" title="Home" href="http://learningzone.themescustom.com/">Home</a>
									<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</a>
									<div class="loginsection pull-right">
										<i class="fa fa-user" aria-hidden="true"></i>
										<a class="signup common" href="http://learningzone.themescustom.com/login/signup.php">Register</a>
										<i class="fa fa-sign-in" aria-hidden="true"></i>
										<a class="login common" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in">Log in</a>
									</div>
									<!-- end div .loginsection -->
									<div class="nav-collapse collapse">
										<ul class="nav">
											<li class="dropdown">
												<a href="http://learningzone.themescustom.com/" class="dropdown-toggle" data-toggle="dropdown" title="Features">Features<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li><a title="Fully Responsive" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Fully Responsive</a></li>
													<li><a title="Front Page Banner" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Front Page Banner</a></li>
													<li><a title="Theme Settings" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Theme Settings</a></li>
													<li><a title="Quick links" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Quick links</a></li>
													<li><a title="Social Media" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Social Media</a></li>
													<li><a title="Custom Footer" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Custom Footer</a></li>
													<li><a title="Custom Section" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Custom Section</a></li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Courses<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
													<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
													<li><a title="Advanced Topics in Literature I" href="http://learningzone.themescustom.com/course/view.php?id=5">Advanced Topics in Literature I</a></li>
													<li><a title="Fundamentals of Literary Analysis" href="http://learningzone.themescustom.com/course/view.php?id=6">Fundamentals of Literary Analysis</a></li>
													<li><a title="Human Nature and Society" href="http://learningzone.themescustom.com/course/view.php?id=2">Human Nature and Society</a></li>
												</ul>
											</li>
											<li><a title="Typography" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Typography</a></li>
											<li><a title="Components" href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Components</a></li>
											<li class="dropdown langmenu">
												<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in" class="dropdown-toggle" data-toggle="dropdown" title="Language">English ‎(en)‎<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li><a title="Deutsch ‎(de)‎" href="http://learningzone.themescustom.com/login/index.php?lang=de">Deutsch ‎(de)‎</a></li>
													<li><a title="English ‎(en)‎" href="http://learningzone.themescustom.com/login/index.php?lang=en">English ‎(en)‎</a></li>
													<li><a title="Español - Internacional ‎(es)‎" href="http://learningzone.themescustom.com/login/index.php?lang=es">Español - Internacional ‎(es)‎</a></li>
													<li><a title="Español Colombiano ‎(es_co)‎" href="http://learningzone.themescustom.com/login/index.php?lang=es_co">Español Colombiano ‎(es_co)‎</a></li>
													<li><a title="简体中文 ‎(zh_cn)‎" href="http://learningzone.themescustom.com/login/index.php?lang=zh_cn">简体中文 ‎(zh_cn)‎</a></li>
												</ul>
											</li>
										</ul>
										<ul class="nav pull-right">
											<li></li>
										</ul>
									</div>
								</div>
							</nav>
						</header>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- End Top Header Section -->
			<!-- Start Bottom Header Section -->
			<div class="bottom-header navbar-fixed-top">
				<div class="container-fluid">
					<div class="pull-left"></div>
					<div class="pull-right">
						<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
						<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- End Bottom Header Section -->
			<!-- Start Page Content -->
			<div id="page-content" class="row-fluid">
				<div class="container-fluid">
					<section id="region-main" class="span12">
						<span class="notifications" id="user-notifications"></span>
						<div role="main">
							<span id="maincontent"></span>
							<div class="loginbox clearfix twocolumns">
								<div class="loginpanel">
									<div class="loginpanel-inner">
										<b>J'ai deja un compte je me connecte</b>
										<h2>Log in</h2>
										<div class="subcontent loginsub">
										
											
										@if(count($errors) > 0 )
											<div class="loginerrors">
												<span class="error"><img class="icon icon icon-pre" alt="Error" src="http://learningzone.themescustom.com/theme/image.php/learningzone/core/1536410572/i/warning" /><b>Erreur de connexion ..! Vos parametres de connexion sont incorrects. Veuillez recommencer avec les bons parametres</b></span>
											</div>
										@endif	
											<form method="POST" action="{{ route('student.login.submit') }}" autocomplete="off" id="login">
												{!! csrf_field() !!}
												<div class="loginform">
													<div class="form-label">
														<label for="username">Username</label>
													</div>
													<div class="form-input">
														<input type="text" name="email" id="username" value="{{ old('email') }}" placeholder="Email" required="">
													</div>
											
													<div class="clearer">
														<!-- -->
													</div>
													<div class="form-label">
														<label for="password">Password</label>
													</div>
													<div class="form-input">
														<input type="password"  placeholder="Mot de passe" name="password" required="">
													</div>
												</div>
												<div class="clearer">
													<!-- -->
												</div>
												<div class="rememberpass">
													<input type="checkbox" name="rememberusername" id="rememberusername" value="1">
													<label for="rememberusername">Rester connecté</label>
												</div>
												<div class="clearer">
													<!-- -->
												</div>
												<input id="anchor" type="hidden" name="anchor" value="">
												
												<input type="submit" id="loginbtn" value="Connexion">
												<div class="forgetpass">
													<a href="{{url('/password/reset') }}" style="color:#fff" aria-haspopup="true" target="_blank">Mot de passe oublié <img src="{{asset('images/others/help.svg')}}" alt="First Slider"></a>
												</div>
											</form>

										</div>

										
									</div>
								</div>
								<div class="signuppanel">
									<h2></h2>
									<div class="subcontent">
										<div style="margin:20px 0;text-align:center;color:#fff;">
											Vous etes un nouveau  
											<span style="color:#f98012">etudiant</span>
											 de la filiere 
											<span style="color:#f98012">RHCOM ?</span>
										</div>
										Pour beneficier de nos cours, devoirs, quiz, Etude de cas etc..., Veuillez creer votre compte
										<div class="signupform">
										<a href="{{ route('create.index') }}"> Create new account </a>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
			<!-- End Page Content -->
			
			<!-- Start Top Footer -->
			@include('includes.site_footer')
			
			
			
		</div>
		
	</body>
</html>
