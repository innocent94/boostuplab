<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Admin | Connexion</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h3>Admin Page</h3>
            
            <p>Login in. To see it in action.</p>
            <form class="m-t" role="form" method="POST" action="{{ url('/login') }}" autocomplete="off">
				{!! csrf_field() !!}
                <div class="form-group">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required="">
					@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Mot de passe" name="password" required="">
					@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Connexion</button>

                <a href="{{url('/password/reset') }}"><small>Forgot password?</small></a>               
            </form>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>

</body>
</html>
