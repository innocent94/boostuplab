<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mot de passe oublié</title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/vendor_backend/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/vendor_backend/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('build/build_backend/css/custom.css')}}" rel="stylesheet">
        
  </head>

<body class="login">
    @include('includes/landing_page_background')
    <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {!! csrf_field() !!}
                        <h1>Mot de passe oublié</h1>
                        <p> Veuillez saisir votre email associé à votre compte pour recevoir votre mot de passe. </p>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email de récupération" required="" />
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                
                        </div>
                        <div>
                        <a href="{{ url('/') }}" title="Retour"><i class="fa fa-long-arrow-left"></i></a>
                        <button type="submit"  class="btn_">
                            <i class="fa fa-btn fa-envelope"></i>
                            Envoyer
                        </button>
                        </div>   
                    </form>
            </section> 
    </div>

    
</div>
<!-- BEGIN HERO SHADOW -->
    <div class="hero__shadow"></div>
<!-- END HERO SHADOW -->

<div class="footer_fixed">
    <p>@include('includes/ha_footer')</p>
</div>

</body>
</html>
