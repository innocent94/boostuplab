<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Initialiser votre mot de passe. </title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/vendor_backend/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/vendor_backend/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('build/build_backend/css/custom.css')}}" rel="stylesheet">
        
  </head>

<body class="login">
    @include('includes/landing_page_background')
    <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <h1>Intialisation</h1>
                        <p> Initialiser un nouveau mot de passe. </p>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          
                            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ $email or old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            
                                <input type="password" class="form-control" placeholder="Mot de passe" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            
                                <input type="password" class="form-control" placeholder="Confirmation du mot de passe" name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                           
                        </div>
                        <div>
                        <button type="submit"  class="btn_">
                            <i class="fa fa-btn fa-refresh"></i>
                            Initialiser
                        </button>
                        </div>   
                    </form>
            </section> 
    </div>

    
</div>
<!-- BEGIN HERO SHADOW -->
    <div class="hero__shadow"></div>
<!-- END HERO SHADOW -->

<div class="footer_fixed">
    <p>@include('includes/ha_footer')</p>
</div>

</body>
</html>
