<html>
<head>
<title>Nsia email</title>
<style type="text/css">
@media only screen and (max-width: 600px) {
 table[class="contenttable"] {
 width: 320px !important;
 border-width: 3px!important;
}
 table[class="tablefull"] {
 width: 100% !important;
}
 table[class="tablefull"] + table[class="tablefull"] td {
 padding-top: 0px !important;
}
 table td[class="tablepadding"] {
 padding: 15px !important;
}
}
</style>
</head>
<body style="margin:0; border: none; background:#f5f5f5">
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
  <tbody><tr>
    <td align="center" valign="top"><table class="contenttable" style="border-width: 8px; border-style: solid; border-collapse: separate; border-color:#ececec; margin-top:40px; font-family:Arial, Helvetica, sans-serif" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="600">
        <tbody><tr>
          <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                <tr>
                  <td height="40" width="100%">&nbsp;</td>
                </tr>
                <tr>
                  <td align="center" valign="top"><a href="#"><img alt="" src="{{URL::asset('images/logo_Groupe_NSIA_145x125.jpg')}}" style="padding-bottom: 0; display: inline !important;" width="217"></a></td>
                </tr>
                <tr>
                  <td height="40" width="100%">&nbsp;</td>
                </tr>
                <tr>
              </tr></tbody>
            </table></td>
        </tr>
        <tr>
          <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                <tr>
                  <td style="padding:16px 10px; line-height:24px; color:#ffffff; font-weight:bold" align="center" bgcolor="#cb9c2a"> Bonjour {{ $name }} <br>
                    Thank you for your order! </td>
                </tr>
                <tr>
              </tr></tbody>
            </table></td>
        </tr>
        <tr>
          <td class="tablepadding" style="border-top:1px solid #eaeaea;border-bottom:1px solid #eaeaea;padding:13px 20px;"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                
                <tr>
                  <td style="font-size:13px; font-family:Arial, Helvetica, sans-serif; color:#676767"><span style="color:#707070">Identifiant réclamation: </span><a style="outline:none; color:#3e7cb4; text-decoration:none;" href="#">13841888912</a> <br/><br/> Object : {{ $title }} </td>
                 
                </tr>
              </tbody>
            </table></td>
        </tr>

        <tr>
          <td class="tablepadding" style="padding:20px; font-size:14px; line-height:20px;"> {{ $content }} </td>
        </tr>
        
       
        <tr>
          <td class="tablepadding" style="padding:20px 0; border-top-width:1px;border-top-style:solid;border-top-color:#ececec;border-collapse:collapse" bgcolor="#fcfcfc"><table style="font-size:13px;color:#999999; font-family:Arial, Helvetica, sans-serif" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                <tr>
                  <td class="tablepadding" style="line-height:20px; padding:20px;" align="center"> 
                    Copyright All Rights Reserved.
                  </td>
                </tr>
                <tr> </tr>
              </tbody>
            </table>
            </td>
        </tr>
      </tbody></table></td>
  </tr>
  
</tbody></table>

</body>