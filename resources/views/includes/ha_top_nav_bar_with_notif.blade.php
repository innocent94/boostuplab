<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to  your website Admin page.</span>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
					<?php 
					$not_read = DB::table('messages')->where('is_read', '=', 0)->get()->all();
				    $TotalNotRead = count($not_read);
					?>
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">{{$TotalNotRead}}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
					@foreach($not_read as $list)
                        <li>
                            <div class="dropdown-messages-box">
                                <div class="media-body">
								<a href="{{route('admin.messages.edit', $list->id)}}" style="color:#555; font-size:12px; padding:0;">
                                    <b>{{$list->first_name}} {{$list->last_name}}</b> <br/>
									{{substr($list->body,0,40)}} ...<br/>
                                    <small class="text-muted">Envoy� le : {{date('d-m-Y', strtotime($list->created_at))}}</small>
                                </a>
								</div>
                            </div>
                        </li>
                        <li class="divider"></li>
					@endforeach
                        <li>
                            <div class="text-center link-block">
                                <a href="{{route('admin.messages.index')}}">
                                    <i class="fa fa-envelope"></i> <strong>Lire tous</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.html">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="{{url('./logout')}}">
                        <i class="fa fa-sign-out"></i> Déconnexion
                    </a>
                </li>
            </ul>

        </nav>