@if(count($errors) > 0 )


    <div class="alert alert-danger">

        <ul>

            @foreach($errors->all() as $error)				
                <b>{{$error}}</b>				
            @endforeach



        </ul>



    </div>

@endif


@if(session()->has('success') > 0 )


    <div class="alert alert-success">

        {{ session()->get('success') }}

    </div>

@endif
