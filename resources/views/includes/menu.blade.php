<nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> 	<span>
                            <img alt="image" class="img-circle" src="{{Auth::user()->photo->file}}" style="width:55px" width="100%"/>
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->name  }} </strong>
                             </span> <span class="text-muted text-xs block"> {{ Auth::user()->roles->pluck('name')->first()}} <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{route('editprofile', Auth::user()->id)}}">Profil</a></li>
                            <li><a href="{{url('./logout')}}">Déconnexion</a></li>
                        </ul>
                    </div>
                </li>
				<li>
                    <a href="{{url('/admin')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Tableau de bord</span></a>
                </li> 
				
				<li>
                    <a href="{{route('admin.slider.index')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Slider</span></a>
				</li>
				
				@can('course_create')
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Cours</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('admin.cours.create')}}">Nouveau cours</a></li>
                        <li><a href="{{route('admin.cours.index')}}">Liste des cours</a></li>
                    </ul>
                </li>
				
				<li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Exercice</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('admin.exercises.create')}}">Nouvel exercice</a></li>
                        <li><a href="{{route('admin.exercises.index')}}">Liste des exercices</a></li>
                    </ul>
                </li>
				
				<li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">E-book</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('admin.ebooks.create')}}">Ajouter</a></li>
                        <li><a href="{{route('admin.ebooks.index')}}">Liste des documents</a></li>
                    </ul>
                </li>
				
				<li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Methodologoie</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('admin.methodologies.create')}}">Ajouter</a></li>
                        <li><a href="{{route('admin.methodologies.index')}}">Liste</a></li>
                    </ul>
                </li>
				
				<li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Lexique</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('admin.lexiques.create')}}">Ajouter un mot</a></li>
                        <li><a href="{{route('admin.lexiques.index')}}">Liste des mots</a></li>
                    </ul>
                </li>
				
				@endcan
				
				@can('news_create')
				<li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Actualite</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('admin.news.create')}}">Nouvelle actualite</a></li>
                        <li><a href="{{route('admin.news.index')}}">Liste des actualite</a></li>
                    </ul>
                </li>
				@endcan
				


				@can('user_create')
				<li>
                    <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('admin.users.create')}}">Nouvel utilisateur</a></li>
                        <li><a href="{{route('admin.users.index')}}">Liste des utilisateurs</a></li>
                    </ul>
                </li>
				@endcan
				
				<li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Quiz</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('admin.quiz.topics.index')}}">Sujets</a></li>
                        <li><a href="">Questions & reponses</a></li>
                    </ul>
                </li>
				
            </ul>

        </div>
    </nav>