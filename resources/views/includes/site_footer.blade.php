<div class="top-footer">
				<div class="container-fluid">
					<ul class="social">
						<li>
							<a href="" target="_blank">
								<i class="facebook"><img src="{{asset('images/icons/facebook.jpg')}}" alt="Facebook"></i>
							</a>
						</li>
						<li>
							<a href="" target="_blank">
								<i class="twitter"><img src="{{asset('images/icons/twitter.jpg')}}" alt="twitter"></i>
							</a>
						</li>
						<li>
							<a href="" target="_blank">
								<i class="googlepluse"><img src="{{asset('images/icons/googlepluse.jpg')}}" alt="googlepluse"></i>
							</a>
						</li>
						<li>
							<a href="" target="_blank">
								<i class="pinterest"><img src="{{asset('images/icons/pinterest.jpg')}}" alt="pinterest"></i>
							</a>
						</li>
						<li>
							<a href="" target="_blank">
								<i class="vimeo"><img src="{{asset('images/icons/vimeo.jpg')}}" alt="vimeo"></i>
							</a>
						</li>
						<li>
							<a href="" target="_blank">
								<i class="git"><img src="{{asset('images/icons/git.jpg')}}" alt="git"></i>
							</a>
						</li>
						<li>
							<a href="" target="_blank">
								<i class="yahoo"><img src="{{asset('images/icons/yahoo.jpg')}}" alt="yahoo"></i>
							</a>
						</li>
						<li>
							<a href="" target="_blank">
								<i class="linkdin"><img src="{{asset('images/icons/linkdin.jpg')}}" alt="linkdin"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!-- End Top Footer -->
			<!-- Start Footer section -->
			<footer id="page-footer">
				<div class="container-fluid">
					<div class="row-fluid">
						<div class="span3">
							<h5>
								ABOUT learningzone
							</h5>
							<ul>
								<li>Duis autem vel eum iriure dolor inhendrerit in vulputate velit esse molestieconsequat, vel illum dolore eu feugiatnulla facilisis at vero eros.</li>
								<li><a href="mailto:cmsbrand93@gmail.com">cmsbrand93@gmail.com</a></li>
								<li>+00 123-456-789</li>
								<li>123 6th St.Melbourne, FL 32904</li>
							</ul>
						</div>
						<div class="span3">
							<h5>
								INFORMATION
							</h5>
							<ul class="common">
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">About Us</a></li>
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Our Stories</a></li>
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">My Account</a></li>
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Our History</a></li>
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Sprcialist Info</a></li>
							</ul>
						</div>
						<div class="span3">
							<h5>
								STUDENT HELP
							</h5>
							<ul class="common">
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">My Info</a></li>
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">My Questions</a></li>
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">F.A.Q</a></li>
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Serch Courses</a></li>
								<li><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Latest Informations</a></li>
							</ul>
						</div>
						<div class="span3">
							<h5>
								INSTAGRAM
							</h5>
							<ul class="instagram-pics">
								<li>
									<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#"><img src="./LearningZone_ Log in to the site_files/instagram-1-image.jpg" alt="instagram-1-image"></a>
								</li>
								<li>
									<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#"><img src="./LearningZone_ Log in to the site_files/instagram-2-image.jpg" alt="instagram-2-image"></a>
								</li>
								<li>
									<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#"><img src="./LearningZone_ Log in to the site_files/instagram-3-image.jpg" alt="instagram-3-image"></a>
								</li>
								<li>
									<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#"><img src="./LearningZone_ Log in to the site_files/instagram-4-image.jpg" alt="instagram-4-image"></a>
								</li>
								<li>
									<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#"><img src="./LearningZone_ Log in to the site_files/instagram-5-image.jpg" alt="instagram-5-image"></a>
								</li>
								<li>
									<a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#"><img src="./LearningZone_ Log in to the site_files/instagram-6-image.jpg" alt="instagram-6-image"></a>
								</li>
							</ul>
							<span class="followus"><a href="http://learningzone.themescustom.com/login/index.php?sesskey=trPGNtkk2TLog%20in#">Follow Us</a></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<ul class="copyright">
						<li><p class="copy">Copyright © 2019 CmsBrand</p></li>
					</ul>
				</div>
			</footer>
			<!-- End Footer section -->
			<!-- Start Bottom Footer section -->
			<div class="bottom-footer">
				<div id="course-footer"></div>
				<p class="helplink"></p>
				<div class="logininfo">
					You are not logged in.
				</div>
			</div>
			<!-- End Bottom Footer section -->
			
			<script type="text/javascript" src="{{asset('js/site/javascript_4.js')}}"></script>
	
			<!-- Start Back To Top -->
			<div id="backtotop" style="display: none;">
				<a class="scrollup" href="javascript:void(0);" title="Back to top"></a>
			</div>
			<!-- End Back To Top -->