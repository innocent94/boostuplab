@if(Auth::guard('student')->user())
	<!-- Message account status -->
	@if(Auth::guard('student')->user()->active_subscription != 0)
	<div class="alert-danger" style="padding-top:25px; padding-bottom:25px; width:100%; border-radius:0px;"> 
		<div class="container-fluid">
			<i class="fa fa-warning"></i>  Bienvenue <b> {{Auth::guard('student')->user()->name}} </b>, vous naviguez en mode <b>Premium</b> vous beneficier de tous les avantages de la plateforme..! Cliquez <a href="{{route('account.subscription.index')}}"> ici </a>
		</div>
	</div>	
	@else
	<div class="alert-danger" style="padding-top:25px; padding-bottom:25px; width:100%; border-radius:0px;"> 
		<div class="container-fluid">
			<i class="fa fa-warning"></i>  Bienvenue <b> {{Auth::guard('student')->user()->name}} </b>, votre navigiguez en mode gratuit (limité), passer au mode <b>Premium</b> pour beneficier de tous les avantages de la plateforme..! Cliquez <a href="{{route('account.subscription.index')}}"> ici </a>
		</div>
	</div>	
	@endif
	<!-- Message account status -->
@else
	<div class="alert-info" style="padding-top:25px; padding-bottom:25px; width:100%; border-radius:0px;"> 
		<div class="container-fluid">
			<b> Bienvenue sur la Plateforme BoostupLab  Pour beneficier des avantages du site veuillez créer votre compte étudiant si vous etes nouveau ou vous connecter si vous posseder deja un compte..! Cliquez<a href="{{route('create.index')}}"><em> ici </em></a> </b>
		</div>
	</div>
@endif