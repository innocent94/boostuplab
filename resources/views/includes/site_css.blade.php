<!-- Title and Meta Tags Ends -->
<!-- Google Font Begins -->
<link href='http://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
<!-- Google Font Ends -->
<!-- CSS Begins -->
<link href="{{asset('css/site/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/site/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/site/style.css')}}" rel="stylesheet" type="text/css" />
