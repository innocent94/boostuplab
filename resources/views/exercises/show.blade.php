<!DOCTYPE html>
<html  dir="ltr" lang="en" xml:lang="en">
	<head>
		<title>Exercice</title>
		<link href="{{asset('css/site/styles.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('css/site/style.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
		
		<meta name="robots" content="noindex">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script type="text/javascript" src="{{asset('js/site/javascript.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/javascript_1.js')}}"></script>
		<script type="text/javascript" src="{{asset('js/site/require.js')}}"></script>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.2"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	</head>
	<body  id="page-course-view-topics" class="format-topics  path-course path-course-view safari dir-ltr lang-en yui-skin-sam yui3-skin-sam learningzone-themescustom-com pagelayout-course course-4 context-27 category-1 has-region-side-pre used-region-side-pre has-region-side-post used-region-side-post layout-option-langmenu">

		
		<div id="page">
			<!-- Start Top Header Section -->
			<!-- Start Top Header Section -->			
		<div class="top-header">			
			<div class="container-fluid">
				<div class="pull-left logocon">
					<a class="logo" href="http://learningzone.themescustom.com"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572//logo"/></a>
				</div>
				<div class="pull-right navigation-con">
					<header role="banner" class="navbar">
						<nav role="navigation" class="navbar-inner">
							<div class="container-fluid">
								<a class="brand" href="http://learningzone.themescustom.com">Accueil</a>
								<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
								<div class="loginsection pull-right">
									<i class="fa fa-user" aria-hidden="true"></i>
									<a class="signup common" href="{{route('student_login')}}">Espace Etudiant</a>
								</div>
								<!-- end div .loginsection -->
								<div class="nav-collapse collapse">
									<ul class="nav">
										<li><a href="#">Cours</a></li>											
										<li><a href="#">Methodologie</a></li>
										<li><a href="#">Exercices</a></li>
										<li><a href="#">News</a></li>
										<li class="dropdown">
										<a href="#cm_submenu_2" class="dropdown-toggle" data-toggle="dropdown" title="Courses">Mon Emploi<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a title="History and Philosophy of Science" href="http://learningzone.themescustom.com/course/view.php?id=4">History and Philosophy of Science</a></li>
											<li><a title="Methodology of Science – Biology" href="http://learningzone.themescustom.com/course/view.php?id=3">Methodology of Science – Biology</a></li>
										</ul>										
										<ul class="nav pull-right">
											<li></li>
										</ul>
								</div>
							</div>
						</nav>
					</header>
				</div>
				<div class="clearfix"></div>
			</div>
			
			<div class="bottom-header" style="positionn:relative">
				<div class="container-fluid">
						<div class="pull-left"></div>
						<div class="pull-right">
							<a class="cal" href="http://learningzone.themescustom.com/course/"><i class="fa fa-folder-open-o"></i> Courses</a>
							<a class="cal" href="http://learningzone.themescustom.com/calendar/view.php"><i class="fa fa-calendar"></i> Calendar</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- Message account status -->
				@include('includes.message')
				<!-- Message account status -->
			</div>
			<!-- End Bottom Header Section -->					
		</div>
		<!-- End Top Header Section -->
		
					
						<!-- End Bottom Header Section -->
						
						<!-- End Block Section -->
						<!-- Start Internal Banner Section -->
						<div class="banner-inner">
							<div class="container-fluid">
								<div class="pageheadingcon">
									<h1>Détails du Cours</h1>
								</div>
							</div>
						</div>
						<!-- End Internal Banner -->
						<!-- Start Breadcrumb -->
						<div id="page-navbar" class="clearfix">
							<div class="container-fluid">
								<nav class="breadcrumb-nav">
									<nav aria-labelledby="navbar-label">
										<ul class="breadcrumb">
											<li>
												<span itemscope="">
													<a itemprop="url" href="#"><span itemprop="title">Accueil</span></a>
												</span>
												<span class="divider">
													<span class="accesshide " ><span class="arrow_text">/</span>&nbsp;</span>
													<span class="arrow sep">&#x25BA;</span>
												</span>
											</li>
											<li>
												<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
													<a itemprop="url" href="http://learningzone.themescustom.com/course/index.php"><span itemprop="title">Cours</span></a>
												</span>
												<span class="divider">
													<span class="accesshide " ><span class="arrow_text">/</span>&nbsp;</span>
													<span class="arrow sep">&#x25BA;</span>
												</span>
											</li>
										</ul>
									</nav>
								</nav>
								<div class="breadcrumb-button"></div>
							</div>
						</div>
						<!-- End Breadcrumb -->
						<!-- Start Page Content -->
						<div id="page-content" class="row-fluid">
							<div class="container-fluid">
								<div id="region-main-box" class="span12">
									<div class="row-fluid">
										<aside class="span3 desktop-first-column">
											<div class="teacherlist">
												<h2 class="headingtag">A propos de l'exercice</h2>
												<br/>
												<h3>L'auteur</h3>
												<ul class="teachers">
													<li>
														<div class="teacher-container clearfix">
															<div class="face pull-left">
															<a href="#">
															  <img src="{{ asset('images/admin/'.basename($user_picture->name)) }}" alt="{{$user->name}}" title="{{$user->name}}" class="userpicture" width="50" height="50"></a></div> 
															  <div class="username"><a href="">{{$user->name}}</a><br>
															  <span class="email">{{$user->email}}</span>
															  </div>
														</div>
													</li>
												
												<ul class="teachers">								
													<a href="">
														<i class="fa fa-sticky-note" style="font-size:15px; color:#333"></i>
														&nbsp;<span class="instancename"> Matière : {{$matiere->name}}</span>
													</a><br/>
													<a href="">
														<i class="fa fa-calendar" style="font-size:15px; color:#333"></i>
														&nbsp;<span class="instancename"> Publié le : {{date('d-m-Y', strtotime($exercises->created_at))}}</span>
													</a><br/>
													<a href="">
														<i class="fa fa-eye" style="font-size:15px; color:#333"></i>
														&nbsp;<span class="instancename"> Vues : {{$exercises->vue}}</span>
													</a><br/>													
													<a href="">
														<i class="fa fa-comments" style="font-size:15px; color:#333"></i>
														&nbsp;<span class="instancename"> Commentaires : <span class="fb-comments-count" data-href="{{route('courses.show', $cours->id)}}"></span> </span>
													</a>														
												</ul>
											</div>
											<div class="teacherlist">
												<h4 class="headingtag">Vous avez une préoccupation en rapport avec le cours ?</h4>			
												<div class="teacherlist" style="background:#26B99A; text-align:center; padding:20px">
													<h5 style="color:#fff"> Poser votre question au professeur </h5>
													<img src="{{ asset('images/admin/'.basename($user_picture->name)) }}" alt="{{$user->name}}" title="{{$user->name}}" style="border-radius:50px" width="70" height="70"></a><br/><br/>
													<a href="" class="btn button" style="background:#f9f9f9"> <i class="fa fa-question-circle"></i> Poser la question </a>	  
												</div>
											</div>
											<br/>
											<br/>
											<a href="{{ asset('images/cours/'.basename($cours->file_id)) }}" target="_blank" class="btn btn-warning btn-xs">  <i class="fa fa-download"></i> Téléchager le cour </a>	  
											
										</aside>
										
										<section id="region-main" class="span9">
											<span class="notifications" id="user-notifications"></span>
											<div role="main">
												<span id="maincontent"></span>
												{!! Form::open(['id'=>'filter', 'method'=>'POST', 'action'=> 'PremiumCourseController@index', 'style'=>'display:none'])!!}
													<div>
														<input type="text" id="filter_status" name="filter_status" value="{{$filter_status}}"/>
														<input type="text" id="filter_matiere" name="filter_matiere" value="{{$filter_matiere}}"/>
													</div>
												{!! Form::close() !!}     
												<div class="course-content">
													<h2>Cours : {{$cours->title }}</h2>
													<hr/>
													
												
														
														<div class="alert alert-info">
														 <h4> <i class="fa fa-info-circle" style="font-size:30px; position:relative;"> </i> Objectifs </h4><br/>
															<div class="panel-body">
																{!!$cours->objectifs !!}
															</div>
														</div>
													
													
												
														
														<div class="alert alert-danger">
														<h4> <i class="fa fa-info-circle" style="font-size:30px; position:relative;"> </i> Prérequis </h4><br/>
															<div class="panel-body">
																{!!$cours->prerequis !!}
															</div>
														</div>
												
														<br/>
														<h4> <i class="fa fa-flag" style="font-size:30px; position:relative;"> </i> &nbsp; Résumé du cours </h4>
														<hr/>
														{!!$cours->content !!}
														
														<a href=""> 
													<!-- comments container -->
											<br/>
											<h3 class="headingtag"># Commentaires</h3>
											<div class="comment_block">											
												<div class="fb-comments" data-href="{{route('courses.show', $cours->id)}}" data-width="" data-numposts="5"></div>											
											</div>
												
												</div>
											</div>
										</section>
									</div>
								</div>
							</div>
						</div>
						<!-- Start Page Content -->
						<script type="text/javascript">
							var activeurl = window.location;
							$('.panel-heading a[href="' + activeurl + '"]').addClass('active');
						</script>
						<!-- Start Top Footer -->
						<div class="top-footer">
							<div class="container-fluid">
								<ul class="social">
									<li>
										<a href="http://www.facebook.com/mycollege" target="_blank" >
											<i class="facebook"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/facebook.jpg" alt="Facebook"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.twitter.com/mycollege" target="_blank" >
											<i class="twitter"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/twitter.jpg" alt="twitter"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.googlepluse.com/mycollege" target="_blank" >
											<i class="googlepluse"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/googlepluse.jpg" alt="googlepluse"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.pinterest.com/mycollege" target="_blank" >
											<i class="pinterest"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/pinterest.jpg" alt="pinterest"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.vimeo.com/mycollege" target="_blank" >
											<i class="vimeo"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/vimeo.jpg" alt="vimeo"/></i>
										</a>
									</li>
									<li>
										<a href="https://git-scm.com/mycollege" target="_blank" >
											<i class="git"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/git.jpg" alt="git"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.yahoo.com/mycollege" target="_blank" >
											<i class="yahoo"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/yahoo.jpg" alt="yahoo"/></i>
										</a>
									</li>
									<li>
										<a href="http://www.linkdin.com/mycollege" target="_blank" >
											<i class="linkdin"><img src="http://learningzone.themescustom.com/theme/learningzone/pix/social/linkdin.jpg" alt="linkdin"/></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- End Top Footer -->
						<!-- Start Footer section -->
						<footer id="page-footer">
							<div class="container-fluid">
								<div class="row-fluid">
									<div class="span3">
										<h5>
											ABOUT learningzone
										</h5>
										<ul>
											<li>Duis autem vel eum iriure dolor inhendrerit in vulputate velit esse molestieconsequat, vel illum dolore eu feugiatnulla facilisis at vero eros.</li>
											<li><a href="mailto:cmsbrand93@gmail.com">cmsbrand93@gmail.com</a></li>
											<li>+00 123-456-789</li>
											<li>123 6th St.Melbourne, FL 32904</li>
										</ul>
									</div>
									<div class="span3">
										<h5>
											INFORMATION
										</h5>
										<ul class="common">
											<li><a href="#">About Us</a></li>
											<li><a href="#">Our Stories</a></li>
											<li><a href="#">My Account</a></li>
											<li><a href="#">Our History</a></li>
											<li><a href="#">Sprcialist Info</a></li>
										</ul>
									</div>
									<div class="span3">
										<h5>
											STUDENT HELP
										</h5>
										<ul class="common">
											<li><a href="#">My Info</a></li>
											<li><a href="#">My Questions</a></li>
											<li><a href="#">F.A.Q</a></li>
											<li><a href="#">Serch Courses</a></li>
											<li><a href="#">Latest Informations</a></li>
										</ul>
									</div>
									<div class="span3">
										<h5>
											INSTAGRAM
										</h5>
										<ul class="instagram-pics">
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-1-image" alt="instagram-1-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-2-image" alt="instagram-2-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-3-image" alt="instagram-3-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-4-image" alt="instagram-4-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-5-image" alt="instagram-5-image" /></a>
											</li>
											<li>
												<a href="#"><img src="http://learningzone.themescustom.com/theme/image.php/learningzone/theme/1536410572/instagram-6-image" alt="instagram-6-image" /></a>
											</li>
										</ul>
										<span class="followus"><a href="#">Follow Us</a></span>
									</div>
								</div>
								<div class="clearfix"></div>
								<ul class="copyright">
									<li><p class="copy">Copyright &copy; 2019 CmsBrand</p></li>
								</ul>
							</div>
						</footer>
						<!-- End Footer section -->
						<!-- Start Bottom Footer section -->
						<div class="bottom-footer">
							<div id="course-footer"></div>
							<p class="helplink"></p>
							<div class="logininfo">
								You are currently using guest access (
								<a href="http://learningzone.themescustom.com/login/index.php">Log in</a>
								)
							</div>
						</div>
						<!-- End Bottom Footer section -->
						
						<!-- Start Back To Top -->
						<div id="backtotop" style="display: none;">
							<a class="scrollup" href="javascript:void(0);" title="Back to top"></a>
						</div>
						<!-- End Back To Top -->
						
					</div>
					
					
					<script type="text/javascript">
					  // JavaScript code
					  var form = document.getElementById("filter");  		  
					  $(document).ready(function(){
						  $("input:checkbox").change(function(){
							if($("input:checkbox").is(':checked')){
							  $("input:checkbox[name='"+$(this).attr("name")+"']").not(this).prop("checked",false);							  
								  if(this.value == 0){
									$('#filter_status').val('');  
								  }else{
									$('#filter_status').val(this.value);  
								  }							 
							}else{
							  $('#filter_status').val('');
							}
							form.submit();
						  });
						  
						  $(".matiere a").click(function(){
							var id = $(this).attr('value');
							$('#filter_matiere').val(id);
							form.submit();
						  });
					  });
					</script>
					
					
					
					<!-- Current link active -->
					<script type="text/javascript">
						// JavaScript code
						$(document).ready(function(){
							var current = $('#filter_status').val();
							if(current == 1){
								$('#checkbox_3').prop("checked",true);
							}else if(current == 2){
								$('#checkbox_2').prop("checked",true);
							}else{
								$('#checkbox_1').prop("checked",true);
							}
						});
					</script>
					
	</body>
</html>
