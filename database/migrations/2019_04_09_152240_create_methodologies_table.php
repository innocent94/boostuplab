<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMethodologiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('methodologies', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id');
            $table->integer('matiere_id');
            $table->string('illustration_img_id');
            $table->text('objectifs');
            $table->string('title');
            $table->text('content');
            $table->string('level');
            $table->string('file_id');
            $table->integer('status_id');
            $table->integer('category_id');
            $table->integer('vue')->default(0);
            $table->integer('comment')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('methodologies');
    }
}
