<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->increments('id');         
			$table->string('title');
			$table->text('body');
			$table->string('topic');
			$table->string('correction');
			$table->integer('status_id');
            $table->integer('category_id');
            $table->integer('vue')->default(0);
            $table->integer('comment')->default(0);
			$table->integer('user_id');
            $table->integer('matiere_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
    }
}
