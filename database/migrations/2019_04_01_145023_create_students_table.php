<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id'); 
			$table->string('type_subscription');
			$table->integer('amount');
            $table->date('date_subscription')->nullable()->default(null);
            $table->date('end_subscription')->nullable()->default(null);
			$table->integer('active_subscription')->default(0);
			
			$table->string('name');
			$table->string('surname');
			$table->string('school_name');
			$table->string('phone_number');
            $table->string('email')->unique();
			$table->string('password');
			$table->string('avatar');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
