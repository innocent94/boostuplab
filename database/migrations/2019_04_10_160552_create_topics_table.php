<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('description');
            $table->string('user_id')->nullable();
            $table->string('status_id')->nullable();
            $table->integer('matiere_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('vews')->default(0);
            $table->foreign('matiere_id')->references('id')->on('matieres')->onDelete('cascade');
            
            $table->timestamps();
            $table->softDeletes();

            $table->index(['deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}
