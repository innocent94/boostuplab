<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cours', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->unsigned();
            $table->integer('matiere_id')->unsigned();
			$table->foreign('matiere_id', 'fk_256_matiere_id')->references('id')->on('matieres');
			$table->foreign('user_id', 'fk_256_user_id')->references('id')->on('users');
            $table->string('illustration_img_id');
            $table->text('objectifs');
            $table->text('prerequis');
            $table->string('title');
            $table->text('content');
            $table->string('time');
            $table->string('level');
            $table->string('file_id');
            $table->integer('status_id');
            $table->integer('category_id');
            $table->integer('vue')->default(0);
            $table->integer('comment')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cours');
    }
}
