<?php

use Illuminate\Database\Seeder;

class PostDependSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		
		//For table maitere
    	$matieres = [
    		['name'=>"Matiere 1"],
    		['name'=> "matiere 2"],
    		['name'=> "Matiere 3"],
    		['name'=> "Matiere 4"]
    	];
    	DB::table('matieres')->insert($matieres);
		
		
		//For table subscription
		$subscriptions = [
			['name'=>"Premium Mois", 'amount'=>'3600', 'duration'=>'1 mois'],
			['name'=>"Premium Année", 'amount'=>'9600', 'duration'=>'1 année'],
			['name'=>"Premium Vip", 'amount'=>'10600', 'duration'=>'1 mois']
    	];
    	DB::table('subscriptions')->insert($subscriptions);

	}

}
