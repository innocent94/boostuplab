<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Student;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//SUPER ADMIN
    	$employee = new User();
    	$employee->name = 'Admin';
    	$employee->email = 'admin@gmail.com';
    	$employee->password = bcrypt('12345');
        $employee->is_active = 1;
        $employee->file_id=1;
        $employee->save();
        $employee->assignRole('Super Administrateur');
        $employee->save();

		//PROFESSOR
    	// $employee = new User();
    	// $employee->name = 'Admin prof';
    	// $employee->email = 'prof@gmail.com';
    	// $employee->password = bcrypt('12345');
        // $employee->is_active = 1;
        // $employee->file_id=1;
        // $employee->save();
        // $employee->assignRole('Administrateur (Professeur)');
        // $employee->save();

		//Student
		// $employee = new Student();
    	// $employee->name = 'Student';
    	// $employee->email = 'student@gmail.com';
    	// $employee->password = bcrypt('12345');
        // $employee->avatar=1;
        // $employee->save();

    }
}
