<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {




        // DB::statement('SET FOREIGN_KEY_CHECKS=0');
        // DB::table('users')->truncate();

       //factory(App\Role::class, 3)->create();

        //$this->call(RoleTableSeeder::class);

        //$this->call(PermissionTableSeeder::class);
        // Role comes before User seeder here.



        $this->call(UserTableSeeder::class);

		//$this->call(PostDependSeeders::class);




    }

}
