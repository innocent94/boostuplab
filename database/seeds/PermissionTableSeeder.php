<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     $permissions = [
	   'admin_access',
	   'admin_state',
	   'state_view',
	   'dashboard_view',
       'user_create',
	   'news_create',
	   'quiz_create',	   
	   'set_ebook',	   
       'course_create',
       'exercise_create',
	   'add_advice' 
     ];

     foreach ($permissions as $permission) {
       Permission::create(['name' => $permission]);
     }

        // Reset cached roles and permissions
     app()['cache']->forget('spatie.permission.cache');

        // create roles and assign created permissions
     $role = Role::create(['name' => 'Super Administrateur']);
     $role->givePermissionTo(Permission::all());


         // create roles and assign created permissions
     $role = Role::create(['name' => 'Administrateur (Professeur)']);
     $role->givePermissionTo([
	   'admin_access',
	   'quiz_create',	   
	   'set_ebook',	   
       'course_create',
       'exercise_create',  
       'add_advice'  
    ]);

       

   }
 }
