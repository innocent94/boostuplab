<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
  error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

Route::get('/', function () {
  return view('welcome');
});


Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/account/login', 'Auth\StudentLoginController@showLoginForm')->name('student_login');
Route::post('/account/login', 'Auth\StudentLoginController@login')->name('student.login.submit');



/* Free access Links */
/* Free access Links */
/* Free access Links */
/* Free access Links */

 // Get quiz route
 Route::get('/quiz', array('as' => 'quiz.index', 'uses' => 'PremiumQuizController@index'));
 Route::post('/quiz', array('as' => 'quiz.index', 'uses' => 'PremiumQuizController@index'));

 Route::get('/quiz/{id}/test', 'PremiumQuizController@test')->name('test');
 Route::match(['put', 'patch'],'/quiz/{id}/test','PremiumQuizController@store_quiz_test');


 // Get courses route
 Route::get('/courses', array('as' => 'courses.index', 'uses' => 'PremiumCourseController@index'));
 Route::post('/courses', array('as' => 'courses.index', 'uses' => 'PremiumCourseController@index'));
 Route::get('/courses/{id}/show', ['as'=>'courses.show', 'uses'=>'PremiumCourseController@show']);


 // Get Exercises route
 Route::get('/exercises', array('as' => 'exercises.index', 'uses' => 'PremiumExercisesController@index'));
 Route::post('/exercises', array('as' => 'exercises.index', 'uses' => 'PremiumExercisesController@index'));
 Route::get('/exercises/{id}/show', ['as'=>'exercises.show', 'uses'=>'PremiumExercisesController@show']);




/*Route::get('/admin/users/{id}/editprofile', 'CreateAccountController@editprofile')->name('editprofile');
  Route::match(['put', 'patch'],'/admin/users/{id}/editprofile','CreateAccountController@update_profile'); */

  Route::resource('create/', 'CreateAccountController',['names'=>[
    'index'=>'create.index',
    'create'=>'create.create',
    'store'=>'create.store',
    'edit'=>'create.edit'
  ]]);



/* Student Route group inside middleware */
/* Student Route group inside middleware */
/* Student Route group inside middleware */
/* Student Route group inside middleware */

Route::group(['middleware'=>['isAdmin','forceSSL']], function(){

	Route::get('/account', function () {
		return 'You can view this because you are logged in';
	})->middleware('auth.student');

	// Get account route
	Route::get('/account', array('as' => 'account.index', 'uses' => 'StudentAccountController@index'));

	Route::get('/account/{id}/student_profile', 'StudentAccountController@edit')->name('student_profile');
	Route::match(['put', 'patch'],'/account/{id}/student_profile','StudentAccountController@update_profile');

	Route::get('/account/subscription', array('as' => 'account.subscription.index', 'uses' => 'StudentAccountController@subscription'));
	Route::get('/account/subscription/{id}/validate_subscription', ['as'=>'account.subscription.validate_subscription', 'uses'=>'StudentAccountController@validate']);

});



/* Student Premium Route group inside middleware */
/* Student Premium Route group inside middleware */
/* Student Premium Route group inside middleware */
/* Student Premium Route group inside middleware */
/* Student Premium Route group inside middleware */

Route::get('/errors/premium', array('as' => 'errors.premium.index', 'uses' => 'CustomErrorController@premium'));

Route::group(['middleware'=>['isAdmin','forceSSL', 'isPremium']], function(){

	Route::get('/account/premium', function () {
		return 'You can view this because you are logged in and account premium is active';
	})->middleware('auth.student');

	// Get account route
	Route::get('/account/premium', array('as' => 'account.premium.index', 'uses' => 'PremiumAccessController@index'));

	//Get Quiz Route for premium
	Route::get('/quiz/test_premium', array('as' => 'quiz.test_premium.index', 'uses' => 'PremiumQuizController@test_premium'));
	Route::post('/quiz/test_premium', array('as' => 'quiz.test_premium.index', 'uses' => 'PremiumQuizController@store_quiz_premium'));

});





Route::group(['middleware'=>['isAdmin','forceSSL']], function(){


  Route::get('/admin', function () {
    return 'You can view this because you are logged in';
  })->middleware('auth');

  //Route::get('/admin', 'HomeController@index');
  Route::get('/admin', array('as' => 'admin', 'uses' => 'HomeController@index'));
  Route::post('/admin', array('as' => 'admin', 'uses' => 'HomeController@index'));


  Route::get('/admin/users/{id}/editprofile', 'AdminUsersController@editprofile')->name('editprofile');
  Route::match(['put', 'patch'],'/admin/users/{id}/editprofile','AdminUsersController@update_profile');
  Route::resource('admin/users', 'AdminUsersController',['names'=>[
    'index'=>'admin.users.index',
    'create'=>'admin.users.create',
    'store'=>'admin.users.store',
    'edit'=>'admin.users.edit'
  ]]);


  //Slider
   Route::resource('admin/slider', 'AdminSlideController',['names'=>[
    'index'=>'admin.slider.index',
    'create'=>'admin.slider.create',
    'store'=>'admin.slider.store',
    'edit'=>'admin.slider.edit'
  ]]);


  //Cours
  Route::get('/admin/cours/view/{id}', ['as'=>'admin.cours.view', 'uses'=>'AdminCoursController@view']);
  Route::resource('admin/cours', 'AdminCoursController',['names'=>[
    'index'=>'admin.cours.index',
    'create'=>'admin.cours.create',
    'store'=>'admin.cours.store',
    'edit'=>'admin.cours.edit'
  ]]);


  //Exercises
  Route::get('/admin/exercises/view/{id}', ['as'=>'admin.exercises.view', 'uses'=>'AdminExercisesController@view']);
  Route::resource('admin/exercises', 'AdminExercisesController',['names'=>[
    'index'=>'admin.exercises.index',
    'create'=>'admin.exercises.create',
    'store'=>'admin.exercises.store',
    'edit'=>'admin.exercises.edit'
  ]]);


  //Ebook
  Route::get('/admin/ebooks/view/{id}', ['as'=>'admin.ebooks.view', 'uses'=>'AdminEbooksController@view']);
  Route::resource('admin/ebooks', 'AdminEbooksController',['names'=>[
    'index'=>'admin.ebooks.index',
    'create'=>'admin.ebooks.create',
    'store'=>'admin.ebooks.store',
    'edit'=>'admin.ebooks.edit'
  ]]);

  //Methodology
  Route::get('/admin/methodologies/view/{id}', ['as'=>'admin.methodologies.view', 'uses'=>'AdminMethodologiesController@view']);
  Route::resource('admin/methodologies', 'AdminMethodologiesController',['names'=>[
    'index'=>'admin.methodologies.index',
    'create'=>'admin.methodologies.create',
    'store'=>'admin.methodologies.store',
    'edit'=>'admin.methodologies.edit'
  ]]);


  //Lexique
  Route::get('/admin/lexiques/view/{id}', ['as'=>'admin.lexiques.view', 'uses'=>'AdminLexiquesController@view']);
  Route::resource('admin/lexiques', 'AdminLexiquesController',['names'=>[
    'index'=>'admin.lexiques.index',
    'create'=>'admin.lexiques.create',
    'store'=>'admin.lexiques.store',
    'edit'=>'admin.lexiques.edit'
  ]]);


  // news
  Route::get('/admin/news/view/{id}', ['as'=>'admin.news.view', 'uses'=>'AdminNewsController@view']);
  Route::resource('admin/news', 'AdminNewsController',['names'=>[
    'index'=>'admin.news.index',
    'create'=>'admin.news.create',
    'store'=>'admin.news.store',
    'edit'=>'admin.news.edit'
  ]]);


  //Quiz
  Route::resource('admin/quiz/topics', 'AdminTopicsController',['names'=>[
    'index'=>'admin.quiz.topics.index',
    'create'=>'admin.quiz.topics.create',
    'store'=>'admin.quiz.topics.store',
    'edit'=>'admin.quiz.topics.edit'
  ]]);

  //Add question to Quiz
  Route::get('/admin/quiz/topics/{id}/add_questions', 'AdminTopicsController@addquestion')->name('questions');
  Route::match(['put', 'patch'],'/admin/quiz/topics/{id}/questions','AdminTopicsController@add');
  Route::get('/admin/quiz/topics/{id}/questions', 'AdminTopicsController@show_questions')->name('index');

  Route::resource('admin/quiz/topics/questions', 'AdminQuestionsController',['names'=>[
    'index'=>'admin.quiz.topics.questions.index',
    'create'=>'admin.quiz.topics.questions.create',
    'store'=>'admin.quiz.topics.questions.store',
    'edit'=>'admin.quiz.topics.questions.edit'
  ]]);

  Route::get('/admin/quiz/topics/questions/{id}/options', array('as' => 'admin.quiz.topics.questions.options.index', 'uses' => 'AdminQuestionsOptionsController@show_options'));
  Route::resource('admin/quiz/topics/questions/options', 'AdminQuestionsOptionsController',['names'=>[

    'create'=>'admin.quiz.topics.questions.options.create',
    'store'=>'admin.quiz.topics.questions.options.store',
    'edit'=>'admin.quiz.topics.questions.options.edit'
  ]]);





  // Messages
  Route::get('/admin/messages/mail/{id}', 'MessageController@composemail')->name('mail');
  Route::match(['put', 'patch'],'/admin/messages/mail/{id}','MessageController@sendmail');

  Route::resource('admin/messages', 'MessageController',['names'=>[
    'index'=>'admin.messages.index',
    'create'=>'admin.messages.create',
    'store'=>'admin.messages.store',
    'edit'=>'admin.messages.edit'
  ]]);



});





Route::get('/home/login', 'StudentAuth\TeacherLoginController@showLoginForm')->name('login');
Route::post('/home/login', 'StudentAuth\TeacherLoginController@login')->name('login');
Route::post('/home/logout', 'StudentAuth\TeacherLoginController@logout')->name('logout');


Route::group(['middleware'=>'student'], function() {
    Route::get('/home/home', 'Teacher\HomeController@index');
});


