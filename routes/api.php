<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

// For cinetPay API
Route::post('/abonnement-notif', [
    'as' => 'abonnementNotif',
    'uses' => 'AbonnementController@notifpay',
]);

Route::post('/abonnement-return', [
    'as' => 'abonnementReturn',
    'uses' => 'AbonnementController@returnpay',
]);